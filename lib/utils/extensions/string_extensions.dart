
import 'package:validators/validators.dart';

extension StringExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();
  bool get isNullOrEmpty => _textIsNullOrEmpty(this);
  String get capitalize => _capitalize(this);
  bool get isUrl => _validateUrl(this);
  String get titleCase => _titleCase(this);
  String get normalizeList => _listNormalize(this);
  String get normalizeUsername => _normalizeUsername(this);
}

String _normalizeUsername(String value) {
  return value.trim().replaceFirst(" ", "");
}

String _listNormalize(String value) {
  return value.replaceFirst("[", "").substring(0, value.length - 2);
}

bool _validateUrl(String url) {
  /// Return false immediately if the url is null or empty
  if (url.isNullOrEmpty) return false;
  String urlPattern = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)";
  var match = RegExp(urlPattern, caseSensitive: false);
  bool result = match.hasMatch(url);
  /// Ignore reg exp
  /// Take care of both https and http
  bool realResult = (url.startsWith("https://") || url.startsWith("http://") || url.startsWith("www.")) && isURL(url);
  return realResult;
}

String _titleCase(String value) {
  if (value.isNullOrEmpty) return value;
  if (value.length == 1) return value.toUpperCase();
  /// Some value is found to have two spaces when one is needed
  value = value.replaceAll("  ", "").trim();
  return value
      .split(' ')
      .map((word) => word[0].toUpperCase() + word.substring(1))
      .join(' ');
}

String _capitalize(String value) {
  if (value.isNullOrEmpty) return value;
  return value[0].toUpperCase() + value.substring(1).toLowerCase();
}

bool _textIsNullOrEmpty(String value, {bool allowSpaceBar = false}) {
  if (value == null) {
    return true;
  }
  if (value.trim().isEmpty && !allowSpaceBar) {
    return true;
  }
  return false;
}
