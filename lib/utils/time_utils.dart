import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

class TimeUtils {

  /// Used to get current time for a new message sent from this device
  String getCurrentUTCDateTime({bool isWithTrailing = true}) {
    String _utcTime = DateTime.now().toUtc().toString();
    if (isWithTrailing) {
      return _utcTime;
    }
    return _utcTime.split(".").first;
  }

  DateTime getCurrentUTCDateTimeWithoutTrailing(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    return dateFormat.parse(time);
  }

  String getLocalDateTimeFromUTC(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var offsetTime = dateFormat.parse(time);
    return offsetTime.add(offsetTime.timeZoneOffset).toString().replaceAll(".000", "");
  }

  DateTime parseBdayString(String value) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    return dateFormat.parse(value);
  }

  String convertDateTimeDisplay(DateTime date) {
    final DateFormat serverFormatter = DateFormat('yyyy-MM-dd');
    final String formatted = serverFormatter.format(date);
    return formatted;
  }

  String parseDateTimeInt(int value) {
    if (value < 10) {
      return "0" + value.toString();
    }
    return value.toString();
  }

  String getYearMonthDay(String time, {bool fromLocalTime = false, String nullValue = "Never"}) {
    if (time.isNullOrEmpty) {
      return nullValue;
    }
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var offsetTime = dateFormat.parse(time);
    if (!fromLocalTime) {
      offsetTime = offsetTime.add(offsetTime.timeZoneOffset);
    }
    int year = offsetTime.year;
    int month = offsetTime.month;
    int day = offsetTime.day;
    String monthString = parseDateTimeInt(month);
    String dayString = parseDateTimeInt(day);
    return year.toString() + "-" + monthString + "-" + dayString;
  }

  String getDateTimeFromTimestamp(Timestamp timestamp) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000, isUtc: true);
    return date.toString().split(".").first;
  }

  String getHourMinuteFromUTCDateTime(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var offsetTime = dateFormat.parse(time);
    offsetTime = offsetTime.add(offsetTime.timeZoneOffset);
    int hour = offsetTime.hour;
    int minute = offsetTime.minute;
    String hourStr;
    String minuteStr;
    if (hour < 10) {
      hourStr = "0" + hour.toString();
    } else {
      hourStr = hour.toString();
    }
    if (minute < 10) {
      minuteStr = "0" + minute.toString();
    } else {
      minuteStr = minute.toString();
    }
    return hourStr + ":" + minuteStr;
  }

  String convertTimestampToEngFormat(String time) {
    DateFormat dateFormat = DateFormat.yMMMd("en_US");
    DateTime createdAt = DateTime.parse(time);
    var currentUtcTime = dateFormat.format(createdAt);
    return currentUtcTime.toString();
  }

  Duration getTimeDifferenceFromNow(DateTime receivedDateTime) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var currentUtcTime = dateFormat.parse(dateFormat.format(DateTime.now().toUtc()));
    return currentUtcTime.difference(receivedDateTime);
  }

  int calculateDifference(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime date = dateFormat.parse(time);
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
  }

  bool getIsSameDay(String time, String secondTime) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime date = dateFormat.parse(time);
    DateTime dateTwo = dateFormat.parse(secondTime);
    return date.year == dateTwo.year && date.month == dateTwo.month && date.day == dateTwo.day;
  }

  bool getIsYesterday(String time, {bool isFromUtc = true}) {
    int result = calculateDifference(isFromUtc ? getLocalDateTimeFromUTC(time) : time);
    if (result == -1) {
      return true;
    }
    return false;
  }

  bool getIsToday(String time, {bool isFromUtc = true}) {
    int result = calculateDifference(isFromUtc ? getLocalDateTimeFromUTC(time) : time);
    if (result == 0) {
      return true;
    }
    return false;
  }

  String getTimeDiffString(String createdAt) {
    var timeDifferenceString;
    var timeDifference = getTimeDifferenceFromNow(DateTime.parse(createdAt));
    if (timeDifference.inSeconds < 0) {
      timeDifference = Duration(seconds: 1);
    }
    if (timeDifference.inMinutes < 1) {
      timeDifferenceString = timeDifference.inSeconds.toString() + "s";
    } else if (timeDifference.inMinutes > 59) {
      if (timeDifference.inHours > 23) {
        if (timeDifference.inDays > 6) {
          timeDifferenceString = (timeDifference.inDays ~/ 7).toString() + "w";
        } else {
          timeDifferenceString = timeDifference.inDays.toString() + "d";
        }
      } else {
        timeDifferenceString = timeDifference.inHours.toString() + "h";
      }
    } else {
      timeDifferenceString = timeDifference.inMinutes.toString() + "m";
    }
    return timeDifferenceString;
  }
}