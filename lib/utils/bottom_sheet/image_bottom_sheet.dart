import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';

import 'generic_bottom_sheet.dart';

void getImageBottomSheet(Function onTap, Function onRemove, {String title}) {
  getGenericBottomSheet([
    SafeArea(
      child: Stack(
        children: [
          Container(
            height: deviceHeight,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 120,
                ),
                GestureDetector(
                  onTap: () => onTap(ImageSource.camera),
                  child: Container(
                    width: deviceWidth * 0.9,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(commonRadius),
                      child: Image.asset(
                        "assets/from_camera.jpg",
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () => onTap(ImageSource.gallery),
                  child: Container(
                    width: deviceWidth * 0.9,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(commonRadius),
                      child: Image.asset(
                        "assets/from_gallery.jpg",
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                LongElevatedButton(
                  title: TextData.removeImage,
                  color: warning,
                  onPressed: onRemove,
                ),
              ],
            ),
          ),
          Positioned.fill(
            top: 40,
            child: Align(
              alignment: Alignment.topLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                    onPressed: () => getRoute.pop(),
                    icon: Icon(
                      Icons.close,
                      color: mainColor,
                      size: 32,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      title ?? TextData.insertImage,
                      style: bigTextStyle.merge(
                          TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30
                          )
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      TextData.pickASource,
                      style: commonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  ], needTopIndicator: false, fullScreen: true);
}