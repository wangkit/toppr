import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/view/view.dart';

void getGenericBottomSheet(List<Widget> listOfWidget, {bool needTopIndicator = true, Function onCompleted, bool needOverlay = true, bool fullScreen = false}) {

  List<Widget> columnChildren;
  if (needTopIndicator) {
    columnChildren = [bottomSheetPullIndicator()] + listOfWidget;
  } else {
    columnChildren = listOfWidget;
  }

  showModalBottomSheet(
    barrierColor: needOverlay ? null : Colors.white.withOpacity(0),
    backgroundColor: appBgColor,
    isScrollControlled: fullScreen,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(commonRadius),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child:  SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: columnChildren,
          ),
        ),
      );
    },
  ).whenComplete(() {
    if (onCompleted != null) {
      onCompleted();
    }
  });
}