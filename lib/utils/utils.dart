import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:encrypt/encrypt.dart' as en;
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:toppr/app/landing/landing_page.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/config/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:toppr/resources/color/hex.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/action.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:uuid/uuid.dart';

class Utils {
  /// Quit focus on text field and close keyboard
  void quitFocus({bool createNewFocusToRemoveOld = false}) {
    FocusScopeNode currentFocus = FocusScope.of(getRoute.getContext());
    if (!currentFocus.hasPrimaryFocus) {
      if (createNewFocusToRemoveOld) {
        currentFocus.requestFocus(FocusNode());
      } else {
        currentFocus.unfocus();
      }
    }
    closeKeyboard();
  }

  fieldFocusChange(FocusNode currentFocus,FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(getRoute.getContext()).requestFocus(nextFocus);
  }

  String getHexCodeByColor(Color color) {
    return color.value.toRadixString(16);
  }

  void closeApp() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  Color getVisibleColor(String colorCode) {
    return getColorByHex(colorCode).withRed(getColorByHex(colorCode).red ~/ 2).withBlue(getColorByHex(colorCode).blue ~/ 2).withGreen(getColorByHex(colorCode).green ~/ 2);
  }

  /// Close the keyboard.
  void closeKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /// Open the keyboard.
  void openKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.show');
  }

  Color getDefaultWhiteOrBlackColor(bool isWhite) {
    return isWhite ? defaultWhiteColor : defaultBlackColor;
  }

  Color getColorByHex(String colorCode, {Color nullColor}) {
    if (colorCode.isNullOrEmpty) {
      return nullColor == null ? unfocusedColor : nullColor;
    } else {
      return HexColor(colorCode) ?? nullColor ?? unfocusedColor;
    }
  }

  String encodeString(String target) {
    return encrypter.encrypt(target, iv: iv).base64;
  }

  String decodeString(String encodedString) {
    return encrypter.decrypt(en.Encrypted.from64(encodedString), iv: iv);
  }

  Future<void> launchURL(String url, {String fallbackUrl}) async {
    if (fallbackUrl == null) fallbackUrl = url;
    if (!url.contains("http") && !url.contains("mailto")) {
      url = "https://" + url;
    }
    try {
      await canLaunch(url) ? launch(url, forceSafariVC: false) : launch(fallbackUrl, forceSafariVC: false);
    } on Exception catch(e) {
//      toast(TextData.cantOpenUrl, bgColor: warning);
    }
  }

  String generatePassword({bool isWithLetters = true, bool isWithUppercase = true, bool isWithNumbers = true, bool isWithSpecial = false, double numberCharPassword = 8}) {

    //Define the allowed chars to use in the password
    String _lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    String _upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String _numbers = "0123456789";
    String _special = "@#=+!£\$%&?[](){}";

    //Create the empty string that will contain the allowed chars
    String _allowedChars = "";

    //Put chars on the allowed ones based on the input values
    _allowedChars += (isWithLetters ? _lowerCaseLetters : '');
    _allowedChars += (isWithUppercase ? _upperCaseLetters : '');
    _allowedChars += (isWithNumbers ? _numbers : '');
    _allowedChars += (isWithSpecial ? _special : '');

    int i = 0;
    String _result = "";

    //Create password
    while (i < numberCharPassword.round()) {
      //Get random int
      int randomInt = Random.secure().nextInt(_allowedChars.length);
      //Get random char and append it to the password
      _result += _allowedChars[randomInt];
      i++;
    }

    return _result;
  }

  void addEmoji(TextEditingController controller, String emoji) {
    int offsetStart;
    int originalOffsetStart;
    String currentText;
    Runes sRunes;
    int emojiCount;
    var beforeInsertionText;
    var beforeInsertionRunes;
    int emojiLength = emoji.length;
    try {
      /// Sometimes, some weird emoji that can't be understood is passed into this function as an argument
      /// To filter these unwanted emojis, make sure the length of the emoji string is 2
      if (emojiLength == 2) {
        if (controller.selection.start < 0) {
          controller.text = controller.text + emoji;
        } else {
          /// Need to turn string into runes since emoji will crash app
          /// Calculate number of emoji in text before insertion point first
          /// then, since each emoji's length is 2 instead of 1 of normal character
          /// Deduct the offset by the number of emoji to get the correct offset of runes
          /// Emoji utf16 code are all larger than 125000, but i am not sure if there are any normal characters beyond 125000, so this might be a problem
          emojiCount = 0;
          offsetStart = controller.selection.start;
          originalOffsetStart = offsetStart;
          currentText = controller.text;
          sRunes = currentText.runes;
          beforeInsertionText = currentText.substring(0, offsetStart);
          beforeInsertionRunes = beforeInsertionText.runes;
          beforeInsertionRunes.forEach((int rune) {
            if (rune > 125000 && rune < 130000) {
              emojiCount++;
            }
          });
          offsetStart = offsetStart - emojiCount;
          controller.text = String.fromCharCodes(sRunes, 0, offsetStart)
              + emoji + String.fromCharCodes(sRunes, offsetStart, sRunes.length);
          if (Platform.isAndroid) {
            controller.value = controller.value.copyWith(
              text: controller.text,
              selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
              composing: TextRange.empty,
            );
          } else {
            /// This delay is needed for ios to allow jumping of cursor in text field
            /// Any milliseconds smaller than 30 will cause unstable performance
            Future.delayed(Duration(milliseconds: 30), () {
              controller.value = controller.value.copyWith(
                text: controller.text,
                selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
                composing: TextRange.empty,
              );
            });
          }
        }
      }
    } on Exception catch (e) {
      print("Add emoji error: $e");
    }
  }

  void deleteChat(bool isOnlyMe, String targetId) {
    firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: myId).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
      if (result.docs.isNotEmpty) {
        firestore.collection(chatDbName).doc(result.docs.first.id).delete();
      }
    });
    if (!isOnlyMe) {
      /// Delete chat for other party as well
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: myId).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).delete();
        }
      });
    }
  }

  void clearSingleMessage(String messageId) {
    firestore.collection(messageDbName).where(MessageItem.messageId, isEqualTo: messageId).get().then((result) {
      if (result.docs.isNotEmpty) {
        result.docs.forEach((messageToBeDeleted) {
          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
        });
      }
    });
  }

  void clearMessageInChat(String chatId, String targetId) {
    firestore.collection(messageDbName).where(MessageItem.chatId, isEqualTo: chatId).get().then((result) {
      if (result.docs.isNotEmpty) {
        result.docs.forEach((messageToBeDeleted) {
          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
        });
      }
      /// Update my chat item
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: myId).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).update({
            ChatTable.hasUnread: false,
            ChatTable.lastMessage: "",
            ChatTable.isLastMessageMyself: false,
            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
          });
        }
      });
      /// Update target chat item
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: myId).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).update({
            ChatTable.hasUnread: false,
            ChatTable.lastMessage: "",
            ChatTable.isLastMessageMyself: false,
            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
          });
        }
      });
    });
  }

  String getChatId(List<String> listOfIds) {
    /// Chat id must always remain the same when:
    /// sender is A, receiver is B
    /// and
    /// receiver is A, sender is B
    /// Using integer parsed from the uuid as id is a way to make sure the id will always remain the same
    /// whenever the conversation involves A and B

    assert(listOfIds.length > 1, "List of ids must have at least two ids in order to form a chat and get the chat id");
    var uuid = Uuid();
    String chatId = "";
    SplayTreeMap<int, String> mapOfIdAndSum = SplayTreeMap();
    listOfIds.forEach((id) {
      int sum = 0;
      uuid.parse(id).forEach((value) {
        if (value.isEven) {
          sum += value;
        } else {
          sum -= value;
        }
      });
      mapOfIdAndSum[sum] = id;
    });
    var newMap = Map.fromEntries(mapOfIdAndSum.entries.toList()..sort((e1, e2) =>
        e1.key.compareTo(e2.key)));
    newMap.values.toList().forEach((id) {
      chatId += id;
    });
    return chatId;
  }

  void emailToSpinnerOfYarns({String subject = "Contacting Spinner of Yarns", String body}) async {
    if (Platform.isAndroid) {
      final emailUrl = body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body";
      await launchURL(emailUrl);
    } else if (Platform.isIOS) {
      final emailUrl = Uri.encodeFull(body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body");
      await launchURL(emailUrl);
    } else {
//      toast(TextData.notSupportedOs, bgColor: warning, iconData: warningIcon);
    }
  }

  Future<void> openFacebook(String fbId) async {
    var fallbackUrl = "https://www.facebook.com/$fbId";
    var fbProtocolUrl = Platform.isAndroid ? "fb.me/$fbId" : Platform.isIOS ? fallbackUrl : fallbackUrl;
    await launchURL(fbProtocolUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openTwitter(String twitterId) async {
    var fallbackUrl = "https://twitter.com/$twitterId";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> launchCaller(BuildContext context, String phoneNumber) async {
    String url = "tel:$phoneNumber";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
//      toast(TextData.phoneDialFail, bgColor: warning, iconData: warningIcon);
    }
  }

  Future<void> loadPref() async {
    hasCalledFCMRegistration = prefs.getBool(PrefKey.hasCalledFCMRegistration) ?? false;
    myId = prefs.getString(PrefKey.myId) ?? "";
    secret = prefs.getString(PrefKey.secret) ?? "";
    uniqueId = prefs.getString(PrefKey.uniqueId) ?? "";
    recentEmojis = prefs.getStringList(PrefKey.recentEmoji) ?? [];
  }

  logout() {
    apiManager.actionLog(ActionLog.logout);
    myId = null;
    firebaseUser = null;
    myProfile = null;
    prefs.remove(PrefKey.myId);
    prefs.remove(PrefKey.accountColor);
    getRoute.navigateToAndPopAll(LandingPage());
  }

  Color getCorrectContrastColor(String color) {
    return getDefaultWhiteOrBlackColor(isDarkColor(color));
  }

  bool isDarkColor(String color) {
    return ThemeData.estimateBrightnessForColor(utils.getColorByHex(color)) == Brightness.dark;
  }

  String shortStringForLongInt(int value) {
    if (value == null) {
      return "0";
    }
    if (value < 0) {
      return "0";
    }
    if (value > 999) {
      var _formattedNumber = NumberFormat.compactCurrency(
        symbol: "",
        decimalDigits: 1,
      ).format(value);
      return _formattedNumber.toLowerCase();
    } else {
      return value.toString();
    }
  }

  int calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).round();
  }

  String getSubscriptionDetails() {
    if (Platform.isIOS) {
      return "Recurring billing, cancel anytime. Subscriptions will be applied to your iTunes account on confirmation and will automatically renew unless canceled before the end of the current period. For more information, see our license agreement at $eulaLink and privacy policy at $privacyPolicyUrl.";
    } else {
      return "Recurring billing, cancel anytime. Subscriptions will be applied to your Google account on confirmation and will automatically renew unless canceled before the end of the current period. For more information, see our license agreement at $eulaLink and privacy policy at $privacyPolicyUrl.";
    }
  }

  String determineNeedS(int number, String text, {bool showNumber = true}) {
    if (number > 1) {
      if (showNumber) {
        if (text == "reply") {
          return shortStringForLongInt(number) + " " + text.replaceAll("y", "") + "ies";
        } else {
          return shortStringForLongInt(number) + " " + text + "s";
        }
      } else {
        return text + "s";
      }
    } else {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text;
      } else {
        return text;
      }
    }
  }
//
//  void deleteChat(bool isOnlyMe, String targetId) {
//    firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
//      if (result.docs.isNotEmpty) {
//        firestore.collection(chatDbName).doc(result.docs.first.id).delete();
//      }
//    });
//    if (!isOnlyMe) {
//      /// Delete chat for other party as well
//      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: qp.id).get().then((result) {
//        if (result.docs.isNotEmpty) {
//          firestore.collection(chatDbName).doc(result.docs.first.id).delete();
//        }
//      });
//    }
//  }
//
//  void clearSingleMessage(String messageId) {
//    firestore.collection(messageDbName).where(MessageItem.messageId, isEqualTo: messageId).get().then((result) {
//      if (result.docs.isNotEmpty) {
//        result.docs.forEach((messageToBeDeleted) {
//          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
//        });
//      }
//    });
//  }
//
//  void clearMessageInChat(String chatId, String targetId) {
//    firestore.collection(messageDbName).where(MessageItem.chatId, isEqualTo: chatId).get().then((result) {
//      if (result.docs.isNotEmpty) {
//        result.docs.forEach((messageToBeDeleted) {
//          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
//        });
//      }
//      /// Update my chat item
//      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
//        if (result.docs.isNotEmpty) {
//          firestore.collection(chatDbName).doc(result.docs.first.id).update({
//            ChatTable.hasUnread: false,
//            ChatTable.lastMessage: "",
//            ChatTable.isLastMessageMyself: false,
//            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
//          });
//        }
//      });
//      /// Update target chat item
//      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: qp.id).get().then((result) {
//        if (result.docs.isNotEmpty) {
//          firestore.collection(chatDbName).doc(result.docs.first.id).update({
//            ChatTable.hasUnread: false,
//            ChatTable.lastMessage: "",
//            ChatTable.isLastMessageMyself: false,
//            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
//          });
//        }
//      });
//    });
//  }

  Future<File> uintToFile(Uint8List data) async {
    final tempDir = await getTemporaryDirectory();
    final _tempFile = await File('${tempDir.path}/${Uuid().v4()}.jpg').create();
    _tempFile.writeAsBytesSync(data);
    return _tempFile;
  }

  toast(String msg, {bool isSuccess = false, bool isWarning = false, Color bgColor, IconData iconData, int seconds = 5, String title, Function onPressed, FlushbarPosition flushBarPosition = FlushbarPosition.BOTTOM}) {
    if (isSuccess) {
      iconData = successIcon;
      bgColor = success;
    }
    if (isWarning) {
      iconData = warningIcon;
      bgColor = warning;
    }
    Flushbar(
      messageText: Text(
        msg,
        style: commonTextStyle.merge(
          TextStyle(
            color: defaultWhiteColor
          )
        ),
      ),
      flushbarPosition: flushBarPosition,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.easeInOut,
      backgroundColor: expandableBlackColor,
      isDismissible: true,
      duration: Duration(seconds: seconds),
      icon: Icon(
        iconData,
        color: bgColor,
      ),
      leftBarIndicatorColor: bgColor,
    )..show(getRoute.getContext());
  }

  saveStr(String key, String value) {
    prefs.setString(key, value);
  }

  saveStrList(String key, List<String> value) {
    prefs.setStringList(key, value);
  }

  saveInt(String key, int value) {
    prefs.setInt(key, value);
  }

  saveBoo(String key, bool value) {
    prefs.setBool(key, value);
  }

  saveDouble(String key, double value) {
    prefs.setDouble(key, value);
  }

  void addToUserDataMap(UserProfile profile) {
    if (!usernameVsProfileMap.containsKey(profile.username)) {
      usernameVsProfileMap[profile.username] = profile;
    }
  }

  void addPostToUserData(List<Post> posts) async {
    List<String> _usernames = [];
    List<UserProfile> _userData = [];
    for (int i = 0; i < posts.length; i++) {
      _usernames.add(posts[i].profileUsername);
    }
    print("_usernames: $_usernames");
    if (_usernames.isNotEmpty) {
      await apiManager.getUserProfileByUsername(_usernames).then((jsonResponse) {
        List<dynamic> listOfData = jsonResponse[DataKey.profiles];
        var _newData = UserProfile.generateUserProfile(listOfData);
        _userData..addAll(List<UserProfile>.from(_newData));
        _userData.forEach((eachUserData) {
          utils.addToUserDataMap(eachUserData);
        });
      });
    }
  }

  String extractTaggedUsername(String value) {
    List<String> taggedUsername = [];
    int index;
    String splitFirst = value;
    while (splitFirst.contains(userTag)) {
      index = splitFirst.indexOf(userTag);
      splitFirst = splitFirst.substring(index + userTag.length, splitFirst.length);
      taggedUsername.add("'" + splitFirst.split(" ").first + "'");
    }
    return taggedUsername.toString().replaceAll("\n", " ");
  }

  Future<String> getAddress(Post post) async {
    List<Placemark> _placemarks = await placemarkFromCoordinates(post.latitude, post.longitude);
    if (_placemarks.isNotEmpty) {
        String country = _placemarks.first.country;
        String city = _placemarks.first.locality.isNullOrEmpty ?
        _placemarks.first.administrativeArea.isNullOrEmpty ? _placemarks.first.thoroughfare : _placemarks.first.administrativeArea : _placemarks.first.locality;
        return "$country, $city";
    } else {
      return "";
    }
  }

  Flushbar getLoadingToast(String msg) {
    return Flushbar(
      messageText: Text(
        msg,
        style: commonTextStyle
      ),
      isDismissible: false,
      showProgressIndicator: true,
      flushbarPosition: FlushbarPosition.BOTTOM,
      flushbarStyle: FlushbarStyle.FLOATING,
      borderRadius: 0,
      progressIndicatorBackgroundColor: getVisibleColor(myProfile.color),
      margin: EdgeInsets.only(bottom: 0.0),
      backgroundColor: appBgColor,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    );
  }

  Future<Position> getLocation() async {
    bool isLocationEnabled  = await Geolocator.isLocationServiceEnabled();
    if (!isLocationEnabled) {
      if (Platform.isAndroid) {
        await Geolocator.openLocationSettings();
      } else {
        await Geolocator.openAppSettings();
      }
      return null;
    } else {
      try {
        Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        return position;
      } on Exception catch (e) {
        return null;
      }
    }
  }

  TextStyle getNameTextStyle(Color textColor, bool bold) {
    if (bold) {
      return TextStyle(
        color: textColor,
        fontWeight: FontWeight.w500,
        fontSize: bigFontSize,
      );
    } else {
      return commonTextStyle.merge(
        TextStyle(
          fontSize: smallFontSize,
          color: textColor,
        )
      );
    }
  }

  String getUserTemplate(String nickname) {
    return userTag + nickname;
  }

  String getTextAfterUserTag(String value) {
    return value.split(userTag).last;
  }
}
