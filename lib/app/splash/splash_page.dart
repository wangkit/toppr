import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:toppr/app/landing/landing_page.dart';
import 'package:toppr/app/main/main_page.dart';
import 'package:toppr/network/firebase.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/env.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/app/registration/registration_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/network/api_services.dart';
import 'package:toppr/network/uploader.dart';
import 'package:toppr/view/dialogs/force_update_dialog.dart';
import 'package:toppr/widgets/emoji/custom_emoji_lists.dart' as emojiList;
import 'package:toppr/page_route/get_route.dart';
import 'package:toppr/utils/time_utils.dart';
import 'package:toppr/utils/utils.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';

class SplashPage extends StatefulWidget {

  SplashPage({
    Key key,
  }) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  static const platform = const MethodChannel("emoji_picker");
  bool isCorrectVersion = true;

  Future<bool> _isEmojiAvailable(String emoji) async {
    if (Platform.isAndroid) {
      bool isAvailable;
      try {
        isAvailable = await platform.invokeMethod("isAvailable", {"emoji": emoji});
      } on PlatformException catch (_) {
        isAvailable = false;
      }
      return isAvailable;
    } else {
      return true;
    }
  }

  Future<Map<String, String>> getAvailableEmojis(Map<String, String> map) async {
    Map<String, String> newMap = Map<String, String>();

    for (int index = 0; index < map.length; index++) {
      String key = map.keys.toList()[index];
      /// map[key] get the emoji value, and only emoji with length equals to two can be normally displayed, thus the checking
      if (await _isEmojiAvailable(map[key]) && map[key].length == 2) {
        newMap[key] = map[key];
      }
    }
    return newMap;
  }

  Future<void> initSharedPreference() async {
    smileyMap = await getAvailableEmojis(emojiList.smileys);
    animalMap = await getAvailableEmojis(emojiList.animals);
    foodMap = await getAvailableEmojis(emojiList.foods);
    travelMap = await getAvailableEmojis(emojiList.travel);
    activityEmojiMap = await getAvailableEmojis(emojiList.activities);
    objectMap = await getAvailableEmojis(emojiList.objects);
    symbolMap = await getAvailableEmojis(emojiList.symbols);
    flagMap = await getAvailableEmojis(emojiList.flags);
    await utils.loadPref();
  }

  bool _isNewUser() {
    return myId.isNullOrEmpty || uniqueId.isNullOrEmpty || secret.isNullOrEmpty;
  }

  Future<void> initCalls() async {
    if (!_isNewUser()) {
      debugPrint("Has account");
      /// Can enter directly
      await Future.wait([
        apiManager.initClientOrRetrieveData(),
        apiManager.getMyProfile(),
      ]);
    } else {
      debugPrint("No account");
      /// New comer or need login
      await apiManager.initClientOrRetrieveData();
    }
  }

  @override
  void initState() {
    utils = Utils();
    timeUtils = TimeUtils();
    uploader = Uploader();
    getRoute = GetRoute();
    apiManager = ApiManager();
    firebaseClient = FirebaseClient();
    apiBaseUrl = env.baseUrl;
    super.initState();
    WidgetsFlutterBinding.ensureInitialized();
    initSharedPreference().then((_) {
      Future.wait([
        initCalls(),
        PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {
          versionCode = int.parse(packageInfo.buildNumber);
          versionName = packageInfo.version;
//          await apiManager.getIsCurrentVersion().then((getIsCorrectVersion) {
//            isCorrectVersion = getIsCorrectVersion;
//          });
        }),
      ]).then((_) {
        if (isCorrectVersion) {
          /// Step nine: Navigate to forum app.main page if myProfile is got and this app does not need to update
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) async {
            /// Init firebase messaging instance
            firebaseMessaging = FirebaseMessaging();
            /// Register firebase messaging
            firebaseMessaging.requestNotificationPermissions(
                const IosNotificationSettings(sound: true, badge: true, alert: true));
            firebaseMessaging.onIosSettingsRegistered
                .listen((IosNotificationSettings settings) {
            });
            Future.delayed(Duration(seconds: 2), () {
              firebaseMessaging.configure(
                onMessage: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is on foreground
                  myBackgroundMessageHandler(message);
                },
                /// For unknown reason, adding below line will cause an exception
                onBackgroundMessage: onBackgroundMessage,
                onLaunch: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is terminated
                  myBackgroundMessageHandler(message);
                },
                onResume: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is in background
                  myBackgroundMessageHandler(message);
                },
              );
            });
            /// Not necessarily qp.email, use any object inside qp can achieve the same result
            if (_isNewUser()) {
              /// No account
              getRoute.navigateToAndPopAll(LandingPage());
            } else {
              getRoute.navigateToAndPopAll(MainPage());
            }
          });
        } else if (myProfile == null) {
          utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
        } else {
          showForceUpdateDialog();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    deviceWidth = MediaQuery.of(context).size.width;
    deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: appBgColor,
      body: Center(
        child: LoadingWidget(),
      ),
    );
  }
}

Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) async {
  debugPrint("Background message: $message");
  return Future<void>.value();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  debugPrint("Background handler message: $message");
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    String title;
    String body;
    title = notification['title'];
    body = notification['body'];
    utils.toast(body, isSuccess: true);
  }

}
