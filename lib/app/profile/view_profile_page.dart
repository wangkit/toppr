import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/chat/chat_page.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/profile/edit_profile_page.dart';
import 'package:toppr/app/relationship/view_relationship_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/physics/avoid_implicit_scroll.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/wrong_token_dialog.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/button/bordered_flat_button.dart';
import 'package:toppr/widgets/button/follow_button.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';
import 'package:toppr/widgets/button/popup_menu.dart';
import 'package:toppr/widgets/display/tag_name_display.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/widgets/media/image/profile_background.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/profile/count.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class ViewProfilePage extends StatefulWidget {

  final UserProfile profile;

  ViewProfilePage({
    Key key,
    @required this.profile,
  }) : super(key: key);

  @override
  _ViewProfilePageState createState() => _ViewProfilePageState();
}

class _ViewProfilePageState extends State<ViewProfilePage> {

  bool _isMyself;
  RefreshController _refreshController;
  bool showLoading;
  List<Post> _posts;
  bool noPosts;
  Future<void> _loading;
  GlobalKey<CountState> _followerKey = GlobalKey();

  @override
  void initState() {
    _refreshController = RefreshController();
    showLoading = false;
    noPosts = false;
    _posts = [];
    _isMyself = widget.profile.id == myProfile.id;
    _loading = handleRefresh();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  Future<void> handleRefresh({bool onScrollDown = false, bool onPullDown = false}) async {

    final Completer<void> completer = Completer<void>();
    int startFrom = onScrollDown ? _posts.length : 0;
    await getPosts(onScrollDown, startFrom);
    showLoading = false;
    refreshThisPage();
    completer.complete();
    return completer.future.then<void>((_) {
      if (onPullDown) {
        _refreshController.refreshCompleted();
      }
      if (onScrollDown) {
        _refreshController.loadComplete();
      }
    });
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> getPosts(bool onScrollDown, int startFrom) async {
    await apiManager.getPosts(startFrom, PostType.profile, widget.profile.id).then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      if (!onScrollDown) _posts.clear();
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfPosts = jsonResponse[DataKey.posts];
          var _newPosts = Post.generatePost(listOfPosts);
          _posts..addAll(List<Post>.from(_newPosts));
          noPosts = _posts.isEmpty;
          if (!noPosts) await Post.setLocation(_posts);
          break;
        case StatusCode.wrongToken:
          getWrongTokenDialog();
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  @override
  Widget build(BuildContext context) {

    UserProfile _refProfile = _isMyself ? myProfile : widget.profile;

    return Scaffold(
      backgroundColor: appBgColor,
      body: NestedScrollView(
        physics: NoImplicitScrollPhysics(parent: ScrollPhysics()),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          // These are the slivers that show up in the "outer" scroll view.
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverSafeArea(
                top: false,
                bottom: false,
                sliver: SliverAppBar(
                  backgroundColor: appBgColor,
                  centerTitle: false,
                  title: Text(
                      _refProfile.username,
                      overflow: TextOverflow.ellipsis,
                      style: commonTextStyle.merge(
                        TextStyle(
                          color: utils.getColorByHex(_refProfile.color),
                        )
                      ),
                  ),
                  actions: _isMyself ? [] : [
                    PopupMenu(
                      profile: _refProfile,
                    ),
                  ],
                  flexibleSpace: FlexibleSpaceBar(
                    background: SafeArea(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ProfileBackground(
                            profile: _refProfile,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                                  child: Thumbnail(
                                    key: Key(_refProfile.image),
                                    radius: 100,
                                    width: 100,
                                    height: 100,
                                    imageUrl: _refProfile.image,
                                    text: _refProfile.username,
                                    color: utils.getColorByHex(_refProfile.color),
                                  ),
                                ),
                                Count(
                                  count: _refProfile.postCount,
                                  text: TextData.post,
                                ),
                                Count(
                                  key: _followerKey,
                                  count: _refProfile.followerCount,
                                  text: TextData.follower,
                                  openedWidget: ViewRelationshipPage(profile: _refProfile, isFollower: true),
                                ),
                                Count(
                                  count: _refProfile.followingCount,
                                  text: TextData.following,
                                  openedWidget: ViewRelationshipPage(profile: _refProfile, isFollower: false),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                              child: SingleChildScrollView(
                                child: TagNameDisplay(
                                  key: Key(_refProfile.bio),
                                  text: _refProfile.bio,
                                  style: smallTextStyle,
                                ),
                              ),
                            ),
                          ),
                          _isMyself ? BorderedFlatButton(
                            onPressed: () {
                              getRoute.navigateTo(EditProfilePage());
                            },
                            borderColorCode: myProfile.color,
                            text: TextData.editProfile,
                            width: deviceWidth,
                            height: 45,
                            edgeInsets: null,
                            textColor: reverseColor,
                            flatButtonPadding: EdgeInsets.all(0.0),
                            bgColor: utils.getColorByHex(myProfile.color,),
                          ) : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FollowButton(
                                profile: widget.profile,
                                width: deviceWidth * 0.45,
                                onChange: (bool isFollowing) {
                                  _followerKey.currentState.changeFollowerCount(isFollowing);
                                },
                              ),
                              OpenContainerWrapper(
                                openedWidget: ChatPage(
                                  targetProfile: _refProfile,
                                ),
                                closedElevation: 0,
                                closedColor: transparent,
                                closedWidget: LongElevatedButton(
                                  width: deviceWidth * 0.45,
                                  borderRadius: commonRadius,
                                  height: 45,
                                  onPressed: null,
                                  title: TextData.message,
                                  textStyle: smallTextStyle.merge(
                                    TextStyle(
                                      color: utils.getCorrectContrastColor(_refProfile.color)
                                    )
                                  ),
                                  color: utils.getColorByHex(_refProfile.color),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  expandedHeight: deviceHeight * 0.65,
                  floating: !_isMyself,
                  pinned: !_isMyself,
                  snap: false,
                  primary: true,
                  automaticallyImplyLeading: true,
                  forceElevated: true,
                ),
              ),
            ),
          ];
        },
        body: FutureBuilder(
          future: _loading,
          builder: (ctx, snapshot) {
            if (showLoading || snapshot.connectionState != ConnectionState.done) {
              return LoadingWidget();
            }
            return SmartRefresher(
              controller: _refreshController,
              enablePullUp: true,
              onRefresh: () => handleRefresh(onPullDown: true),
              onLoading: () => handleRefresh(onScrollDown: true),
              child: GridView.builder(
                padding: EdgeInsets.zero,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1 / 1,
                ),
                itemCount: _posts.length,
                itemBuilder: (BuildContext ctx, int index) {

                  String _key = UniqueKey().toString();

                  return ImageHolder(
                    key: Key(_posts[index].images.toString()),
                    showLoading: false,
                    width: deviceWidth / 3,
                    height: deviceWidth / 3,
                    imageList: _posts[index].images,
                    index: 0,
                    heroKey: _key,
                    onPressed: () {

                      List<Post> _tempSublist = List.from(_posts);
                      _tempSublist = _tempSublist.sublist(index);

                      getRoute.navigateTo(ViewPostPage(
                        posts: _tempSublist,
                        title: _posts.first.profileUsername,
                        postType: PostType.profile,
                        targetProfileId: _refProfile.id,
                        totalCount: _posts.length - _tempSublist.length
                      ));
                    },
                  );
                }
              ),
            );
          },
        ),
      ),
    );
  }
}
