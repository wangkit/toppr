import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/relationship/view_relationship_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/bottom_sheet/image_bottom_sheet.dart';
import 'package:toppr/view/dialogs/loading_dialog.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/button/bordered_flat_button.dart';
import 'package:toppr/widgets/button/follow_button.dart';
import 'package:toppr/widgets/color_picker/color_picker.dart';
import 'package:toppr/widgets/display/tag_name_display.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/widgets/media/image/profile_background.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/textfield/bio_textfield.dart';
import 'package:toppr/widgets/textfield/username_textfield.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

class EditProfilePage extends StatefulWidget {

  EditProfilePage({
    Key key,
  }) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {

  File _backgroundImage;
  File _image;
  ImagePicker _picker;
  TextEditingController _bioController;
  TextEditingController _usernameController;
  FocusNode _bioNode;
  FocusNode _usernameNode;
  String _currentColorCode;
  GlobalKey<FormState> _formKey = GlobalKey();
  String _imageLink = myProfile.image;
  String _backgroundImageLink = myProfile.backgroundImage;

  @override
  void initState() {
    _currentColorCode = myProfile.color;
    _picker = ImagePicker();
    _bioNode = FocusNode();
    _usernameNode = FocusNode();
    _bioController = TextEditingController(text: myProfile.bio);
    _usernameController = TextEditingController(text: myProfile.username);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> _pickImage(ImageSource source) async {
    getRoute.pop();
    PickedFile _pickedFile = await _picker.getImage(
      source: source,
    );
    if (_pickedFile != null) {
      _image = File(_pickedFile.path);
    }
    refreshThisPage();
  }

  void onSelectColor(Color pickerColor, StateSetter customState) {
    _currentColorCode = utils.getHexCodeByColor(pickerColor);
    customState(() {});
  }

  void _remove() {
    getRoute.pop();
    _image = null;
    refreshThisPage();
  }

  Future<void> _pickBgImage(ImageSource source) async {
    getRoute.pop();
    PickedFile _pickedFile = await _picker.getImage(
      source: source,
    );
    if (_pickedFile != null) {
      _backgroundImage = File(_pickedFile.path);
    }
    refreshThisPage();
  }

  void _removeBg() {
    getRoute.pop();
    _backgroundImage = null;
    refreshThisPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Dialogs.showLoadingDialog(title: TextData.editing);

          bool isUsernameAvailable = true;

          if (myProfile.username != _usernameController.text.normalizeUsername) {
            isUsernameAvailable = await apiManager.checkUsername(_usernameController.text.normalizeUsername);
            if (!isUsernameAvailable) {
              getRoute.pop();
              utils.toast(TextData.usernameTaken, isWarning: true);
              return;
            }
          }

          if (_image != null) {
            _imageLink = await uploader.uploadFileToFirebase(_image, false, ImageType.profile);
          }

          if (_backgroundImage != null) {
            _backgroundImageLink = await uploader.uploadFileToFirebase(_backgroundImage, false, ImageType.profile);
          }

          await apiManager.editProfile(
            _bioController.text.trim(),
            _currentColorCode,
            _usernameController.text.trim(),
            _imageLink,
            _backgroundImageLink,
          );
          await apiManager.getMyProfile();
          /// Pop loading
          getRoute.pop();
          /// Pop page
          getRoute.pop();
          /// Show toast after pop page
          utils.toast(TextData.editProfileSuccessfully, isSuccess: true);
        },
        child: Icon(
          Icons.done,
          color: mainColor,
        ),
      ),
      appBar: MyAppBar(
        title: TextData.editProfile,
      ),
      backgroundColor: appBgColor,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  getImageBottomSheet(_pickBgImage, _removeBg, title: TextData.insertBgImage);
                },
                child: ProfileBackground(
                  profile: myProfile,
                  disableOnPress: true,
                  image: _backgroundImage,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                onTap: () {
                  getImageBottomSheet(_pickImage, _remove,);
                },
                child: Thumbnail(
                  radius: 100,
                  width: 100,
                  height: 100,
                  imageUrl: _imageLink,
                  imageFile: _image,
                  text: myProfile.username,
                  color: utils.getColorByHex(myProfile.color),
                  disableOnTap: true,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              ColorPicker(
                onPressed: (String newColor) {
                  _currentColorCode = newColor;
                },
                currentColorCode: _currentColorCode,
              ),
              SizedBox(
                height: 15,
              ),
              BioTextField(
                controller: _bioController,
                focusNode: _bioNode,
              ),
//              SizedBox(
//                height: 15,
//              ),
//              UsernameTextField(
//                controller: _usernameController,
//                focusNode: _usernameNode,
//              ),
              SizedBox(
                height: kToolbarHeight * 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
