import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toppr/app/map/google_map.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/firestore_tables.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/view/dialogs/wrong_token_dialog.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/animation/fade_indexed_stack.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/button/like_icon_button.dart';
import 'package:toppr/widgets/button/like_reply_icon_button.dart';
import 'package:toppr/widgets/button/popup_menu.dart';
import 'package:toppr/widgets/display/tag_name_display.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/media/media_swipper.dart';
import 'package:toppr/widgets/no_data/no_data.dart';
import 'package:toppr/widgets/panel/custom_panel.dart';
import 'package:toppr/widgets/textfield/reply_bar.dart';
import 'package:visibility_detector/visibility_detector.dart';

class PostPage extends StatefulWidget {

  final List<Post> posts;
  final PostType postType;
  final int skippedCount;
  final String targetProfileId;

  PostPage({
    Key key,
    this.posts,
    this.postType = PostType.following,
    this.targetProfileId,
    this.skippedCount = 0,
  }) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> with AutomaticKeepAliveClientMixin<PostPage> {

  CardController _cardController;
  double _bottomMinHeight = 70;
  double _cardMinWidth = 20;
  bool showLoading = false;
  double _opacity;
  List<Post> _posts;
  bool noPosts;
  Future<void> _loading;
  ValueNotifier<int> _currentIndexNotifier;
  PanelController _panelController;
  ExpandableController _replyBarExpandableController;
  TextEditingController _replyController;
  DocumentSnapshot _lastDocument;
  double _iconSize = 20;
  double _panelHeight = deviceHeight - kToolbarHeight;
  GlobalKey<ReplyBarState> _collapsedKey = GlobalKey();
  GlobalKey<ReplyBarState> _expandedKey = GlobalKey();
  bool _isOtherPosts;
  int _startFrom;

  @override
  void dispose() {
    _replyBarExpandableController.dispose();
    _replyController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _startFrom = 0;
    _opacity = 1;
    _replyBarExpandableController = ExpandableController();
    _replyController = TextEditingController();
    _panelController = PanelController();
    _currentIndexNotifier = ValueNotifier(0);
    noPosts = false;
    _posts = [];
    _cardController = CardController();
    _isOtherPosts = widget.postType != PostType.following;
    if (_isOtherPosts) {
      _posts = widget.posts;
      _loading = _posts.isEmpty ? handleRefresh() : _emptyCall();
    } else {
      _loading = handleRefresh();
    }
    super.initState();
  }

  Widget _replySeparator() {
    return   Divider(
      color: unfocusedColor,
      height: 1,
    );
  }

  Future<void> _emptyCall() async {
    await Future.delayed(Duration(milliseconds: 10), () {});
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> handleRefresh({bool onScrollDown = false, bool onPullDown = false}) async {
    _startFrom += onScrollDown ? _posts.length : 0;
    await getPosts(onScrollDown);
    showLoading = false;
    refreshThisPage();
  }
  
  Future<void> getPosts(bool onScrollDown) async {
    await apiManager.getPosts(widget.skippedCount + _startFrom, widget.postType, widget.targetProfileId).then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      if (onScrollDown) _posts.clear();
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfPosts = jsonResponse[DataKey.posts];
          var _newPosts = Post.generatePost(listOfPosts);
          _posts..addAll(List<Post>.from(_newPosts));
          utils.addPostToUserData(_posts);
          noPosts = _posts.isEmpty;
          if (!noPosts) await Post.setLocation(_posts);
          break;
        case StatusCode.wrongToken:
          getWrongTokenDialog();
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  Widget _loadingWidget() {

    /// Imitate the look of loaded posts

    return CustomSlidingUpPanel(
      minHeight: _bottomMinHeight,
      maxHeight: _panelHeight,
      backdropColor: unfocusedColor,
      backdropEnabled: true,
      borderRadius: BorderRadius.circular(commonRadius),
      collapsed: Material(
        color: appBgColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(commonRadius), topLeft: Radius.circular(commonRadius)),
        ),
      ),
      panel: Container(
        decoration: BoxDecoration(
            color: appBgColor,
            borderRadius: BorderRadius.only(topRight: Radius.circular(commonRadius), topLeft: Radius.circular(commonRadius))
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(bottom: _bottomMinHeight),
        child: LoadingWidget(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return noPosts ? NoData(
      text: TextData.noPost,
    ) : FutureBuilder(
      future: _loading,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.done || showLoading) {
          return _loadingWidget();
        }
        return WillPopScope(
          onWillPop: () async {
            if (_panelController.isPanelOpen) {
              if (_replyBarExpandableController.expanded) {
                _replyBarExpandableController.toggle();
              } else {
                utils.closeKeyboard();
                _panelController.close();
              }
              return false;
            }
            return true;
          },
          child: Scaffold(
            key: UniqueKey(),
            body: CustomSlidingUpPanel(
              minHeight: _bottomMinHeight,
              maxHeight: _panelHeight,
              backdropColor: unfocusedColor,
              backdropEnabled: true,
              controller: _panelController,
              borderRadius: BorderRadius.circular(commonRadius),
              collapsed: ValueListenableBuilder(
                valueListenable: _currentIndexNotifier,
                builder: (ctx, int value, Widget child) {

                  Post _thisPost = _posts[value];

                  return Material(
                    color: appBgColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(topRight: Radius.circular(commonRadius), topLeft: Radius.circular(commonRadius)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 15,
                              ),
                              Tooltip(
                                message: TextData.likeCount,
                                child: LikeIconRow(
                                  post: _thisPost,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              IconButton(
                                tooltip: TextData.replyCount,
                                onPressed: () {
                                  if (_panelController.isPanelClosed) {
                                    _collapsedKey.currentState.unfocus();
                                    _expandedKey.currentState.unfocus();
                                    _panelController.open();
                                  }
                                },
                                splashRadius: kToolbarHeight / 3,
                                icon: Icon(
                                  MdiIcons.commentOutline,
                                  color: utils.getColorByHex(_thisPost.profileColor),
                                ),
                              ),
                              StreamBuilder<QuerySnapshot>(
                                key: UniqueKey(),
                                stream: firestore.collection(outerRepliesDbName)
                                    .doc(_thisPost.postId)
                                    .collection(innerReplyDbName)
                                    .snapshots(),
                                builder: (ctx, snapshot) {
                                  return Text(
                                    utils.shortStringForLongInt(snapshot.data != null ? snapshot.data.size : 0),
                                    style: commonTextStyle,
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: TagNameDisplay(
                                    key: Key(_thisPost.profileUsername),
                                    text: utils.getUserTemplate(_thisPost.profileUsername),
                                    maxLines: 2,
                                    isOverflow: true,
                                    isBold: false,
                                    showImage: false,
                                  ),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: PopupMenu(
                                      profile: UserProfile(
                                        id: _thisPost.id,
                                        username: _thisPost.profileUsername,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              panel: ValueListenableBuilder<int>(
                valueListenable: _currentIndexNotifier,
                builder: (ctx, int value, Widget child) {

                  Post _thisPost = _posts[value];

                  return Container(
                    width: deviceWidth,
                    decoration: BoxDecoration(
                      color: appBgColor,
                      borderRadius: BorderRadius.only(topRight: Radius.circular(commonRadius), topLeft: Radius.circular(commonRadius))
                    ),
                    child: Column(
                      children: [
                        bottomSheetPullIndicator(
                          padding: 12.0,
                        ),
                        Expanded(
                          child: StreamBuilder<QuerySnapshot>(
                            key: Key(_thisPost.postId),
                            stream: firestore
                                .collection(outerRepliesDbName)
                                .doc(_thisPost.postId)
                                .collection(innerReplyDbName)
                                .orderBy(ReplyTable.createdAt, descending: true)
                                .limit(50)
                                .snapshots(),
                            builder: (ctx, snapshot) {

                              List<Widget> repliesWidget = [];

                              void _handleReplies(List replies) {
                                if (replies == null) {
                                  return;
                                }
                                if (replies.isNotEmpty) {
                                  _lastDocument = replies.last;
                                  for (DocumentSnapshot reply in replies) {

                                    Timestamp createdAt = reply.data()[ReplyTable.createdAt];
                                    String createdAtString = timeUtils.getTimeDiffString(timeUtils.getDateTimeFromTimestamp(createdAt));
                                    String username = reply.data()[ReplyTable.username];
                                    String replyId = reply.data()[ReplyTable.replyId];
                                    String replyMessage = reply.data()[ReplyTable.reply];
                                    List<String> likedProfileIds = reply.data()[ReplyTable.likedProfileIds].cast<String>();
                                    List<String> replyReplyProfileIds = reply.data()[ReplyTable.replyReplyProfileIds].cast<String>();
                                    int replyReplyCount = reply.data()[ReplyTable.replyReplyCount];
                                    String replyProfileId = reply.data()[ReplyTable.id];
                                    String postId = reply.data()[ReplyTable.postId];
                                    bool isMyself = firebaseUser.email == reply.data()[ReplyTable.id];
                                    ExpandableController _tempExpandableController = ExpandableController();

                                    final replyWidget = GestureDetector(
                                      onTap: replyReplyCount <= 0 ? null : () => _tempExpandableController.toggle(),
                                      child: Material(
                                        color: appBgColor,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                TagNameDisplay(
                                                  key: UniqueKey(),
                                                  text: utils.getUserTemplate(username),
                                                  showImage: true,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Icon(
                                                  MdiIcons.clockOutline,
                                                  color: mainColor,
                                                  size: _iconSize,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  createdAtString,
                                                  key: Key(createdAtString),
                                                  style: smallTextStyle,
                                                ),
                                                Flexible(
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: PopupMenu(
                                                      profile: UserProfile(
                                                        username: username,
                                                        id: replyProfileId,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Flexible(
                                                  child: TagNameDisplay(
                                                    text: replyMessage,
                                                    key: Key(replyMessage),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                IconButton(
                                                  onPressed: () {
                                                    _expandedKey.currentState.setReplyReplyId(replyId, username);
                                                    _collapsedKey.currentState.setReplyReplyId(replyId, username);
                                                  },
                                                  icon: Icon(
                                                    replyReplyProfileIds.contains(myProfile.id) ? MdiIcons.reply : MdiIcons.replyOutline,
                                                    color: utils.getColorByHex(myProfile.color),
                                                    size: _iconSize,
                                                  ),
                                                ),
                                                Text(
                                                  utils.shortStringForLongInt(replyReplyCount),
                                                  style: smallTextStyle,
                                                ),
                                                SizedBox(
                                                  width: 15,
                                                ),
                                                LikeReplyIconRow(
                                                  likedProfileIds: likedProfileIds,
                                                  replyId: replyId,
                                                  post: _thisPost,
                                                  iconSize: _iconSize,
                                                ),
                                              ],
                                            ),
                                            _replySeparator(),
                                          ],
                                        ),
                                      ),
                                    );

                                    Future<List<Reply>> _getReplyReplies() async {
                                      QuerySnapshot results = await firestore.collection(outerRepliesDbName)
                                          .doc(postId)
                                          .collection(innerReplyDbName)
                                          .doc(replyId)
                                          .collection(replyReplyDbName)
                                          .orderBy(ReplyTable.createdAt, descending: true)
                                          .limit(50)
                                          .get();
                                      return Reply.generateReply(results.docs);
                                    }

                                    final replyFinalWidget = ExpandableNotifier(
                                      controller: _tempExpandableController,
                                      child: ScrollOnExpand(
                                        child: ExpandablePanel(
                                          collapsed: replyWidget,
                                          expanded: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              replyWidget,
                                              /// Reply of reply
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal: 12.0),
                                                child: FutureBuilder<List<Reply>>(
                                                  future: _getReplyReplies(),
                                                  builder: (ctx, snapshot) {

                                                    if (!snapshot.hasData) {
                                                      return LoadingMediaWidget();
                                                    }

                                                    return VisibilityDetector(
                                                      key: UniqueKey(),
                                                      onVisibilityChanged: (VisibilityInfo info) {
                                                        /// Auto close the expanded when leave screen
                                                        if (info.visibleFraction < 0.1 && _tempExpandableController.expanded) {
//                                                          _tempExpandableController.toggle();
                                                        }
                                                      },
                                                      child: Container(
                                                        width: deviceWidth,
                                                        height: (deviceHeight - kToolbarHeight * 2) * 0.6,
                                                        child: ListView.separated(
                                                          separatorBuilder: (ctx, index) {
                                                            return Divider(
                                                              color: unfocusedColor,
                                                              height: 0,
                                                            );
                                                          },
                                                          itemCount: snapshot.data.length,
                                                          itemBuilder: (ctx, index) {

                                                            Reply _thisReply = snapshot.data[index];

                                                            return Padding(
                                                              key: Key(_thisReply.replyId),
                                                              padding: EdgeInsets.all(8.0),
                                                              child: Row(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Column(
                                                                    mainAxisSize: MainAxisSize.min,
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                      Flexible(
                                                                        child: TagNameDisplay(
                                                                          text: utils.getUserTemplate(username),
                                                                          showImage: true,
                                                                          style: smallTextStyle,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        height: 10,
                                                                      ),
                                                                      Row(
                                                                        mainAxisSize: MainAxisSize.min,
                                                                        children: [
                                                                          Text(
                                                                            _thisReply.createdAt,
                                                                            key: Key(_thisReply.createdAt),
                                                                            style: smallTextStyle.merge(
                                                                              TextStyle(
                                                                                color: deepUnfocusedColor,
                                                                              )
                                                                            ),
                                                                          ),
                                                                          SizedBox(
                                                                            width: 10,
                                                                          ),
                                                                          GestureDetector(
                                                                            onTap: () {
                                                                              _expandedKey.currentState.setReplyReplyId(replyId, username);
                                                                              _collapsedKey.currentState.setReplyReplyId(replyId, username);
                                                                              _expandedKey.currentState.setTextController(utils.getUserTemplate(_thisReply.username) + " ");
                                                                              _collapsedKey.currentState.setTextController(utils.getUserTemplate(_thisReply.username) + " ");
                                                                            },
                                                                            child: Text(
                                                                              TextData.reply,
                                                                              style: smallTextStyle.merge(
                                                                                TextStyle(
                                                                                  color: deepUnfocusedColor,
                                                                                )
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    width: 15,
                                                                  ),
                                                                  Flexible(
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: 4.0),
                                                                      child: TagNameDisplay(
                                                                        key: Key(_thisReply.replyMessage),
                                                                        text: _thisReply.replyMessage,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                              _replySeparator(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );

                                    if (!repliesWidget.contains(replyFinalWidget)) {
                                      repliesWidget.add(replyFinalWidget);
                                    }
                                  }
                                }
                              }

                              if (!snapshot.hasData) {
                                return Center(
                                  child: Text(
                                    TextData.noReply,
                                    style: commonTextStyle,
                                  ),
                                );
                              }

                              _handleReplies(snapshot.data.docs);

                              return Scrollbar(
                                child: ListView(
                                  reverse: true,
                                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                                  children: repliesWidget,
                                ),
                              );
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: SafeArea(
                            child: ExpandableNotifier(
                              controller: _replyBarExpandableController,
                              child: Expandable(
                                collapsed: ReplyBar(
                                  key: _collapsedKey,
                                  postId: _thisPost.postId,
                                  inputController: _replyController,
                                  expandableController: _replyBarExpandableController,
                                  isExpanded: false,
                                ),
                                expanded: ReplyBar(
                                  key: _expandedKey,
                                  postId: _thisPost.postId,
                                  inputController: _replyController,
                                  expandableController: _replyBarExpandableController,
                                  isExpanded: true,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              body: TinderSwapCard(
                swipeUp: false,
                swipeDown: false,
                orientation: AmassOrientation.TOP,
                totalNum: _posts.length,
                stackNum: 2,
                maxWidth: deviceWidth,
                maxHeight: deviceHeight * 0.9,
                minWidth: deviceWidth - _cardMinWidth,
                minHeight: deviceHeight * 0.75,
                cardBuilder: (context, index) {

                  Post _thisPost = _posts[index];

                  return Padding(
                    key: Key(index.toString()),
                    padding: EdgeInsets.only(bottom: deviceHeight * 0.09),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                      elevation: commonElevation,
                      child: Stack(
                        children: [
                          MediaSwipper(
                            key: UniqueKey(),
                            post: _thisPost,
                            shouldAutoPlay: false,
                          ),
                          Positioned.fill(
                            child: StatefulBuilder(
                              builder: (ctx, customState) {
                                return AnimatedOpacity(
                                  duration: Duration(milliseconds: 100),
                                  opacity: _opacity,
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 12.0, bottom: 24.0, left: 8, right: 8),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          if (_thisPost.longitude.toString().isNullOrEmpty) Container(
                                            width: 0,
                                            height: 0,
                                          ) else OpenContainerWrapper(
                                            openedWidget: MapPage(
                                              latLng: LatLng(_thisPost.latitude, _thisPost.longitude),
                                              location: _thisPost.location,
                                            ),
                                            closedColor: transparent,
                                            closedShape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.zero,
                                            ),
                                            closedElevation: 0,
                                            closedWidget: Padding(
                                              padding: EdgeInsets.only(left: 8.0, bottom: 8.0),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Icon(
                                                    Icons.pin_drop,
                                                    color: utils.getColorByHex(_thisPost.textColor, nullColor: defaultWhiteColor),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Flexible(
                                                    child: Text(
                                                      _thisPost.location,
                                                      maxLines: 2,
                                                      overflow: TextOverflow.ellipsis,
                                                      style: commonTextStyle.merge(
                                                          TextStyle(
                                                            color: utils.getColorByHex(_thisPost.textColor, nullColor: defaultWhiteColor),
                                                            fontWeight: FontWeight.bold,
                                                          )
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Flexible(
                                            child: GestureDetector(
                                              onTap: () {
                                                if (_opacity == 0) {
                                                  _opacity = 1.0;
                                                } else {
                                                  _opacity = 0.0;
                                                }
                                                customState(() {});
                                              },
                                              child: Padding(
                                                padding: EdgeInsets.all(8),
                                                child: TagNameDisplay(
                                                  text: _thisPost.caption,
                                                  maxLines: null,
                                                  isOverflow: false,
                                                  style: commonTextStyle.merge(
                                                      TextStyle(
                                                        color: utils.getColorByHex(_thisPost.textColor, nullColor: defaultWhiteColor),
                                                      )
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                cardController: _cardController = CardController(),
                swipeUpdateCallback: (DragUpdateDetails details, Alignment align) {
                  if (align.x < 0) {
                    debugPrint("here left");
                  } else if (align.x > 0) {
                    debugPrint("here right");
                  }
                },
                swipeCompleteCallback: (CardSwipeOrientation orientation, int index) {
                  if (orientation != CardSwipeOrientation.RECOVER) {
                    if (index + 1 >= _posts.length) {
                      showLoading = true;
                      refreshThisPage();
                      /// Get next batch of posts
                      handleRefresh(onScrollDown: true);
                    }
                    if (widget.postType == PostType.discover) {
                      apiManager.viewPost(_posts[_currentIndexNotifier.value].postId);
                    }
                    _currentIndexNotifier.value = index + 1;
                  }
                },
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}


