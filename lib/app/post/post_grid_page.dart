import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toppr/app/map/google_map.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/firestore_tables.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/view/dialogs/wrong_token_dialog.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/animation/fade_indexed_stack.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/button/like_icon_button.dart';
import 'package:toppr/widgets/button/like_reply_icon_button.dart';
import 'package:toppr/widgets/button/popup_menu.dart';
import 'package:toppr/widgets/display/tag_name_display.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/media/media_swipper.dart';
import 'package:toppr/widgets/no_data/no_data.dart';
import 'package:toppr/widgets/panel/custom_panel.dart';
import 'package:toppr/widgets/textfield/reply_bar.dart';
import 'package:visibility_detector/visibility_detector.dart';

class PostGridPage extends StatefulWidget {

  final List<Post> posts;
  final PostType postType;
  final int skippedCount;
  final String title;
  final bool needAppBar;

  PostGridPage({
    Key key,
    this.posts,
    this.postType = PostType.like,
    this.skippedCount = 0,
    this.title,
    this.needAppBar = true,
  }) : super(key: key);

  @override
  _PostGridPageState createState() => _PostGridPageState();
}

class _PostGridPageState extends State<PostGridPage> with AutomaticKeepAliveClientMixin<PostGridPage> {

  bool showLoading = false;
  List<Post> _posts;
  bool noPosts;
  Future<void> _loading;
  bool _isOtherPosts;
  int _startFrom;
  RefreshController _refreshController;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _refreshController = RefreshController();
    _startFrom = 0;
    noPosts = false;
    _posts = [];
    _isOtherPosts = widget.postType != PostType.discover;
    if (_isOtherPosts) {
      _posts = widget.posts;
      _loading = _posts.isEmpty ? handleRefresh() : _emptyCall();
    } else {
      _loading = handleRefresh();
    }
    super.initState();
  }

  Future<void> _emptyCall() async {
    await Future.delayed(Duration(milliseconds: 500), () {});
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> handleRefresh({bool onScrollDown = false, bool onPullDown = false}) async {
    final Completer<void> completer = Completer<void>();
    _startFrom += onScrollDown ? _posts.length : 0;
    await getPosts(onScrollDown);
    showLoading = false;
    refreshThisPage();
    completer.complete();
    return completer.future.then<void>((_) {
      if (onPullDown) {
        _refreshController.refreshCompleted();
      }
      if (onScrollDown) {
        _refreshController.loadComplete();
      }
    });
  }

  Future<void> getPosts(bool onScrollDown) async {
    await apiManager.getPosts(widget.skippedCount + _startFrom, widget.postType, "").then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfPosts = jsonResponse[DataKey.posts];
          var _newPosts = Post.generatePost(listOfPosts);
          _posts..addAll(List<Post>.from(_newPosts));
          utils.addPostToUserData(_posts);
          noPosts = _posts.isEmpty;
          if (!noPosts) await Post.setLocation(_posts);
          break;
        case StatusCode.wrongToken:
          getWrongTokenDialog();
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  Widget _main() {
    return noPosts ? NoData(
      text: TextData.noPost,
    ) : FutureBuilder(
      future: _loading,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.done || showLoading) {
          return LoadingWidget();
        }

        return SmartRefresher(
          controller: _refreshController,
          enablePullUp: true,
          onRefresh: () => handleRefresh(onPullDown: true),
          onLoading: () => handleRefresh(onScrollDown: true),
          child: GridView.builder(
              padding: EdgeInsets.zero,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 1 / 1,
              ),
              itemCount: _posts.length,
              itemBuilder: (BuildContext ctx, int index) {

                String _key = UniqueKey().toString();

                return ImageHolder(
                  showLoading: false,
                  width: deviceWidth / 3,
                  height: deviceWidth / 3,
                  imageList: _posts[index].images,
                  index: 0,
                  heroKey: _key,
                  onPressed: () {

                    List<Post> _tempSublist = List.from(_posts);
                    _tempSublist = _tempSublist.sublist(index);

                    getRoute.navigateTo(ViewPostPage(
                        posts: _tempSublist,
                        title: widget.title,
                        postType: widget.postType,
                        totalCount: _posts.length - _tempSublist.length
                    ));
                  },
                );
              }
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.needAppBar ? Scaffold(
      appBar: MyAppBar(
        title: widget.title,
      ),
      body: _main(),
    ) : _main();
  }

  @override
  bool get wantKeepAlive => true;
}


