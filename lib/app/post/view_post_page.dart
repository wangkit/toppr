import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/widgets/appbar/appbar.dart';

class ViewPostPage extends StatefulWidget {

  final List<Post> posts;
  final String title;
  final PostType postType;
  final String targetProfileId;
  final int totalCount;

  ViewPostPage({
    Key key,
    @required this.posts,
    @required this.title,
    this.postType = PostType.discover,
    this.targetProfileId,
    this.totalCount,
  }) : super(key: key);

  @override
  _ViewPostPageState createState() => _ViewPostPageState();
}

class _ViewPostPageState extends State<ViewPostPage> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.title.isNullOrEmpty ? null : MyAppBar(
        title: widget.title,
      ),
      backgroundColor: appBgColor,
      body: PostPage(
        posts: widget.posts,
        postType: widget.postType,
        targetProfileId: widget.targetProfileId,
        skippedCount: widget.totalCount,
      ),
    );
  }
}
