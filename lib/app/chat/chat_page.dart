import 'dart:async';
import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/view/dialogs/block_dialog.dart';
import 'package:toppr/widgets/button/popup_menu.dart';
import 'package:toppr/widgets/chat/message_bubble.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/textfield/chat_input_bar.dart';

class ChatPage extends StatefulWidget {
  final UserProfile targetProfile;
  final Function setNoUnread;

  ChatPage({
    Key key,
    @required this.targetProfile,
    this.setNoUnread,
  }) : super(key: key);

  @override
  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> with AutomaticKeepAliveClientMixin<ChatPage> {

  TextEditingController _messageTextController;
  UserProfile _targetProfile;
  String chatId;
  double scrollOffset;
  ScrollController _scrollController;
  String scrollPositionKey;
  ExpandableController _expandableController;
  RefreshController _refreshController;
  DocumentSnapshot _lastDocument;
  Color _commonColor;
  List<String> _selectedMessageId = [];

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    if (widget.setNoUnread != null) {
      widget.setNoUnread();
    }
    _refreshController = RefreshController(
      initialRefresh: false,
    );
    _targetProfile = widget.targetProfile;
    /// Set chat item to no unread first
    firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: myId).where(ChatTable.targetId, isEqualTo: _targetProfile.id).get().then((result) {
      if (result.docs.length > 0) {
        firestore.collection(chatDbName).doc(result.docs.first.id).update({
          ChatTable.hasUnread: false,
        });
      }
    });
    _messageTextController = TextEditingController();
    _expandableController = ExpandableController();
    chatId = utils.getChatId([myId, _targetProfile.id]);
    /// Scroll position got from shared preference or 0.0 if null
    scrollOffset = prefs.getDouble(scrollPositionKey) ?? 0.0;

    /// Set init scroll offset according to the scrollPosition retrieved
    _scrollController = ScrollController(
      initialScrollOffset: scrollOffset,
    );

    /// Listen to the change of scroll offset and set it to scrollPosition
    _scrollController.addListener(() async {
      scrollOffset = _scrollController.offset;
      utils.saveDouble(scrollPositionKey, scrollOffset);
    });
    scrollPositionKey = chatId + "_scrollPosition";
    _commonColor = utils.getCorrectContrastColor(_targetProfile.color);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _messageTextController.dispose();
    super.dispose();
  }

  void messagesStream() async {
    await for (var snapshot in firestore.collection(messageDbName).snapshots()) {
      for (var message in snapshot.docs) {
        print(message.data);
      }
    }
  }

  void scrollToBottom({int waitMilliseconds = 100}) {
    if (_scrollController.hasClients) {
      _scrollController.jumpTo(
        0.0,
      );
      /// A delay is needed for the scroll controller to update its max extent and current position
      Future.delayed(Duration(milliseconds: waitMilliseconds), () {
        if (scrollOffset != 0.0) {
          scrollToBottom();
        }
      });
    }
  }

  void scrollToBottomOnKeyboardUp() {
    /// If the user is at the bottom, scroll the chat list view to the bottom when the user opens the keyboard by clicking the input bar
    /// If the user is NOT at the bottom at the moment when the user clicks the input bar, do nothing
    if (_scrollController.position.atEdge && _scrollController.position.pixels != 0) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        scrollToBottom();
      });
    }
  }

  void refreshThisPage() {
    if (this.mounted){
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => getRoute.pop(),
          icon: Icon(
            Icons.arrow_back,
            color: _commonColor,
          ),
        ),
        actions: <Widget>[
          _selectedMessageId.length > 0 ? IconButton(
            onPressed: () {
              _selectedMessageId.forEach((messageId) {
                utils.clearSingleMessage(messageId);
              });
              _selectedMessageId.clear();
              refreshThisPage();
            },
            icon: Icon(
              Icons.delete,
              color: mainColor
            ),
          ) : Container(),
          PopupMenu(
            profile: _targetProfile,
          ),
        ],
        title: Row(
          children: [
            Thumbnail(
              width: 40,
              height: 40,
              imageUrl: _targetProfile.image,
              color: utils.getColorByHex(_targetProfile.color),
              text: _targetProfile.username,
            ),
            SizedBox(width: 10,),
            Text(
              _targetProfile.username,
              style: commonTextStyle.merge(
                TextStyle(
                  color: _commonColor,
                )
              ),
            ),
          ],
        ),
        backgroundColor: utils.getColorByHex(_targetProfile.color),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: firestore
                  .collection(messageDbName)
                  .where(MessageItem.chatId, isEqualTo: chatId)
                  .orderBy(MessageItem.createdAt, descending: true)
                  .limit(numberOfMessagesPerPage).snapshots(),
              builder: (context, snapshot) {

                List<MessageBubble> messageBubbles = [];

                void _handleMessages(List messages) {
                  if (messages == null) {
                    return;
                  }
                  if (messages.isNotEmpty) {
                    for (var message in messages) {

                      Timestamp createdAt = message.data()[MessageItem.createdAt];
                      String createdAtString = timeUtils.getDateTimeFromTimestamp(createdAt);
                      bool isMyself = myProfile.id == message.data()[MessageItem.senderId];
                      _lastDocument = messages.last;

                      final messageBubble = MessageBubble(
                        messageId: message.data()[MessageItem.messageId],
                        message: message.data()[MessageItem.message],
                        senderId: message.data()[MessageItem.senderId],
                        senderName: isMyself ? myProfile.username : _targetProfile.username,
                        createdAt: createdAtString,
                        senderColor: isMyself ? myProfile.color : _targetProfile.color,
                        receiverId: message.data()[MessageItem.receiverId],
                        isSelected: _selectedMessageId.contains(message.data()[MessageItem.messageId]),
                        receiverName: isMyself ? _targetProfile.username : myProfile.username,
                        selectedMessages: _selectedMessageId,
                        notifyParent: refreshThisPage,
                        isMyself: isMyself,
                      );
                      if (!messageBubbles.contains(messageBubble)) {
                        messageBubbles.add(messageBubble);
                      }
                    }
                  }
                }

                if (!snapshot.hasData) {
                  return Container();
                }
                final messages = snapshot.data.docs;
                _handleMessages(messages);
                return Expanded(
                  child: SmartRefresher(
                    controller: _refreshController,
                    reverse: true,
                    enablePullUp: true,
                    enablePullDown: false,
                    onLoading: () {
                      firestore.collection(messageDbName)
                          .where(MessageItem.chatId, isEqualTo: chatId)
                          .orderBy(MessageItem.createdAt, descending: true)
                          .startAfterDocument(_lastDocument)
                          .limit(numberOfMessagesPerPage)
                          .get()
                          .then((result) {
                        final messages = result.docs;
                        _handleMessages(messages);
                        _refreshController.loadComplete();
                      });
                    },
                    child: ListView(
                      reverse: true,
                      controller: _scrollController,
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      children: messageBubbles,
                    ),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SafeArea(
                child: ExpandableNotifier(
                  controller: _expandableController,
                  child: Expandable(
                    collapsed: ChatInputBar(
                      inputController: _messageTextController,
                      expandableController: _expandableController,
                      isExpanded: false,
                      targetProfile: _targetProfile,
                    ),
                    expanded: ChatInputBar(
                      inputController: _messageTextController,
                      expandableController: _expandableController,
                      isExpanded: true,
                      targetProfile: _targetProfile,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
