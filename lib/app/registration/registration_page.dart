import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toppr/app/login/login_page.dart';
import 'package:toppr/app/main/main_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/network/api_services.dart';
import 'package:toppr/network/uploader.dart';
import 'package:toppr/page_route/get_route.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:toppr/utils/bottom_sheet/image_bottom_sheet.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/time_utils.dart';
import 'package:toppr/utils/utils.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/view/dialogs/loading_dialog.dart';
import 'package:toppr/widgets/button/footer_button.dart';
import 'package:toppr/widgets/button/loading_long_elevated_button.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';
import 'package:toppr/widgets/color_picker/color_picker.dart';
import 'package:toppr/widgets/tag/tag.dart';
import 'package:toppr/widgets/textfield/email_textfield.dart';
import 'package:toppr/widgets/textfield/otp_textfield.dart';
import 'package:toppr/widgets/textfield/password_textfield.dart';
import 'package:toppr/widgets/textfield/username_textfield.dart';

class RegistrationPage extends StatefulWidget {

  RegistrationPage({
    Key key,
  }) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {

  PageController _pageController;
  TextEditingController _emailController;
  TextEditingController _usernameController;
  TextEditingController _otpController;
  TextEditingController _passwordController;
  FocusNode _emailNode;
  FocusNode _otpNode;
  FocusNode _usernameNode;
  FocusNode _passwordNode;
  int _initialPage;
  int _totalLength;
  File _imageFile;
  List<Widget> _pages;
  String currentColorCode;
  final _formKey = GlobalKey<FormState>();
  final _formTwoKey = GlobalKey<FormState>();
  bool _showLocationDialog;

  void nextPage() {
    _pageController.nextPage(duration: kThemeAnimationDuration, curve: Curves.fastLinearToSlowEaseIn);
  }

  void previousPage() {
    _pageController.previousPage(duration: kThemeAnimationDuration, curve: Curves.fastLinearToSlowEaseIn);
  }

  void goToPage(int target) {
    _pageController.animateToPage(target, duration: kThemeAnimationDuration, curve: Curves.fastLinearToSlowEaseIn);
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _showLocationDialog = true;
    currentColorCode = utils.getHexCodeByColor(appIconColor);
    _emailController = TextEditingController();
    _otpController = TextEditingController();
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    _emailNode = FocusNode();
    _otpNode = FocusNode();
    _usernameNode = FocusNode();
    _passwordNode = FocusNode();
    _initialPage = 1;
    _pageController = PageController();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _totalLength = _pages.length;
      refreshThisPage();
    });
  }

  @override
  void dispose() {
    _otpNode.dispose();
    _emailNode.dispose();
    _passwordNode.dispose();
    _usernameNode.dispose();
    _otpController.dispose();
    _emailController.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  List<Widget> _footer() {
    return [FooterButton(
      onPressed: () {
        getRoute.pop();
        getRoute.navigateTo(LoginPage());
      },
      title: TextData.loginInstead,
    ),];
  }

  AppBar _appBar({bool isPopOnPress = false, bool showLeading = true}) {
    return AppBar(
      centerTitle: true,
      automaticallyImplyLeading: false,
      leading: showLeading ? IconButton(
        onPressed: () {
          if (isPopOnPress) {
            getRoute.pop();
          } else {
            previousPage();
          }
        },
        icon: Icon(
          Icons.arrow_back,
          color: mainColor,
        ),
      ) : null,
      title: Text(
        TextData.registration,
        style: bigTextStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    _pages = [
      Scaffold(
        appBar: _appBar(
          isPopOnPress: true
        ),
        persistentFooterButtons: _footer(),
        body: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: 80,
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(200),
                child: Image.asset(
                  "assets/placeholder.png",
                  width: 200,
                  height: 200,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              EmailTextField(
                controller: _emailController,
                focusNode: _emailNode,
                nextFocusNode: _passwordNode,
              ),
              SizedBox(
                height: 20,
              ),
              LoadingLongElevatedButton(
                title: TextData.next,
                onPressed: (startLoading, stopLoading, btnState) async {
                  if (_formKey.currentState.validate()) {
                    if (btnState == ButtonState.Idle) {
                      startLoading();
                      _emailNode.unfocus();
                      bool isAvailable = await apiManager.checkEmail(_emailController.text);
                      if (isAvailable) {
                        _emailNode.unfocus();
                        apiManager.getVerificationCode(_emailController.text);
                        nextPage();
                      } else {
                        utils.toast(TextData.emailTaken, isWarning: true);
                      }
                      stopLoading();
                    }
                  }
                },
              ),
              Expanded(
                child: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 8.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.0),
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: TextData.byContinuing,
                                style: smallTextStyle,
                              ),
                              TextSpan(
                                recognizer: TapGestureRecognizer()..onTap = () {
                                  utils.launchURL(privacyPolicyUrl);
                                },
                                text: TextData.privacyPolicy,
                                style: TextStyle(
                                    color: mainColor,
                                    decoration: TextDecoration.underline,
                                    fontSize: smallFontSize
                                ),
                              ),
                              TextSpan(
                                text: " " + TextData.and + " ",
                                style: smallTextStyle,
                              ),
                              TextSpan(
                                recognizer: TapGestureRecognizer()..onTap = () {
                                  utils.launchURL(eulaLink);
                                },
                                text: TextData.endUserLicenseAgreement,
                                style: TextStyle(
                                  color: mainColor,
                                  decoration: TextDecoration.underline,
                                  fontSize: smallFontSize,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      Scaffold(
        appBar: _appBar(),
        body: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
              child: Text(
                TextData.otpHasSent + _emailController.text + ".",
                style: bigTextStyle,
              ),
            ),
            OtpTextField(
              controller: _otpController,
              focusNode: _otpNode,
              onCompleted: (String result) async {
                _otpNode.unfocus();
                Dialogs.showLoadingDialog(title: TextData.verifying);
                bool isCorrect = await apiManager.checkOtp(_otpController.text);
                getRoute.pop();
                if (isCorrect) {
                  nextPage();
                } else {
                  utils.toast(TextData.incorrectOtp, isWarning: true);
                }
              },
            ),
            LoadingLongElevatedButton(
              title: TextData.resend,
              color: unfocusedColor,
              width: 150,
              onPressed: (startLoading, stopLoading, btnState) async {
                if (btnState == ButtonState.Idle) {
                  startLoading();
                  bool isSent = await apiManager.getVerificationCode(_emailController.text);
                  if (isSent) {
                    utils.toast(TextData.sentSuccessfully, isSuccess: true);
                  } else {
                    utils.toast(TextData.waitAMinute, isWarning: true);
                  }
                  stopLoading();
                }
              },
            ),
            SizedBox(
              height: 20,
            ),
            LoadingLongElevatedButton(
              title: TextData.next,
              onPressed: (startLoading, stopLoading, btnState) async {
                if (btnState == ButtonState.Idle) {
                  startLoading();
                  bool isCorrect = await apiManager.checkOtp(_otpController.text);
                  if (isCorrect) {
                    nextPage();
                  } else {
                    utils.toast(TextData.incorrectOtp, isWarning: true);
                  }
                  stopLoading();
                }
              },
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
      Scaffold(
        appBar: _appBar(
          showLeading: false,
        ),
        body: Form(
          key: _formTwoKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  child: Text(
                    TextData.accountInstruction,
                    style: bigTextStyle
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                UsernameTextField(
                  controller: _usernameController,
                  focusNode: _usernameNode,
                  nextFocusNode: _passwordNode,
                ),
                PasswordTextField(
                  controller: _passwordController,
                  focusNode: _passwordNode,
                  nextFocusNode: _usernameNode,
                  isNewPassword: true,
                ),
                SizedBox(
                  height: 20,
                ),
                ColorPicker(
                  onPressed: (String newColor) {
                    currentColorCode = newColor;
                  },
                  currentColorCode: currentColorCode,
                ),
                SizedBox(
                  height: 20,
                ),
                LoadingLongElevatedButton(
                  title: TextData.next,
                  onPressed: (startLoading, stopLoading, btnState) async {
                    if (_formTwoKey.currentState.validate()) {
                      if (btnState == ButtonState.Idle) {
                        startLoading();
                        _usernameNode.unfocus();
                        _passwordNode.unfocus();
                        _usernameController.text = _usernameController.text.normalizeUsername;
                        bool isAvailable = await apiManager.checkUsername(_usernameController.text);
                        if (isAvailable) {
                          nextPage();
                        } else {
                          utils.toast(TextData.usernameTaken, isWarning: true);
                        }
                        stopLoading();
                      }
                    }
                  },
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
      Scaffold(
        appBar: _appBar(
          showLeading: false,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                    TextData.firstThought,
                    style: bigTextStyle
                ),
              ),
              SizedBox(
                height: 30,
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return GestureDetector(
                    onTap: () async {

                      Future<void> _pickImage(ImageSource source) async {
                        getRoute.pop();
                        final picker = ImagePicker();
                        PickedFile _pickedFile = await picker.getImage(
                          source: source,
                        );
                        if (_pickedFile != null) {
                          _imageFile = File(_pickedFile.path);
                        }
                        customState(() {});
                      }

                      void _remove() {
                        getRoute.pop();
                        _imageFile = null;
                        customState(() {});
                      }

                      getImageBottomSheet(_pickImage, _remove);
                    },
                    child: Center(
                      child: _imageFile != null ? Image.file(
                        _imageFile,
                        width: deviceWidth,
                        fit: BoxFit.cover,
                      ) : Image.asset(
                        "assets/thought.png",
                        width: deviceWidth,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 20,
              ),
              LongElevatedButton(
                title: TextData.create,
                onPressed: () async {

                  Future<void> _create() async {
                    String fcmSecret = utils.generatePassword();
                    Dialogs.showLoadingDialog(title: TextData.creatingProfile);
                    await firebaseClient.createAccountWithEmailAndPassword(_emailController.text, fcmSecret);
                    await firebaseClient.signInToFirebase(_emailController.text, fcmSecret);
                    String _imageLink = await uploader.uploadFileToFirebase(_imageFile, false, ImageType.post);
                    await apiManager.createProfile(
                      _usernameController.text,
                      _passwordController.text,
                      _emailController.text,
                      currentColorCode,
                      _imageLink,
                      fcmSecret,
                      Position(
                        latitude: 0,
                        longitude: 0,
                      ),
                    );
                    getRoute.pop();
                    getRoute.navigateToAndPopAll(MainPage());
                  }

                  if (_imageFile != null) {
                    _create();
                  } else {
                    utils.toast(TextData.selectAnImage, isWarning: true);
                  }
                },
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    ];

    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: _pages,
    );
  }
}


