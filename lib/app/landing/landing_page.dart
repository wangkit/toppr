import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:toppr/app/login/login_page.dart';
import 'package:toppr/app/registration/registration_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/button/footer_button.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';
import 'package:toppr/widgets/tag/tag.dart';

class LandingPage extends StatefulWidget {

  LandingPage({
    Key key,
  }) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            getAppName(),
            SizedBox(
              height: 50,
            ),
            LongElevatedButton(
              title: TextData.createNewAccount,
              onPressed: () {
                getRoute.navigateTo(RegistrationPage());
              },
            ),
            SizedBox(
              height: 20,
            ),
            LongElevatedButton(
              isReverseColor: true,
              title: TextData.login,
              onPressed: () {
                getRoute.navigateTo(LoginPage());
              },
            ),
          ],
        ),
      ),
    );
  }
}


