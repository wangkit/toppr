import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toppr/widgets/appbar/appbar.dart';

class MapPage extends StatefulWidget {

  final LatLng latLng;
  final String location;

  MapPage({
    Key key,
    this.latLng,
    this.location,
  }) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {

  Completer<GoogleMapController> _controller;
  CameraPosition _kTagPlex;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _controller = Completer();
    _kTagPlex= CameraPosition(
      target: widget.latLng,
      zoom: 15.0
    );
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: MyAppBar(
        title: widget.location,
      ),
      body: GoogleMap(
        markers: Set.from([Marker(
          markerId: MarkerId(
            UniqueKey().toString(),
          ),
          position: widget.latLng,
        )]),
        mapType: MapType.normal,
        initialCameraPosition: _kTagPlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }
}


