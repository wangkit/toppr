import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class AboutPage extends StatefulWidget {
  AboutPage({Key key}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: MyAppBar(
        title: TextData.aboutUs,
      ),
      backgroundColor: appBgColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(height: 20,),
            _greyBar(TextData.followUs),
            _ourFacebook(),
            _ourTwitter(),
            _greyBar(TextData.appInformation),
            _buildInfo(versionName + "(" + versionCode.toString() + ")", Icon(Icons.build, color: mainColor,)),
            _buildInfo(appName, Icon(Icons.location_city, color: mainColor,)),
            _buildInfo(TextData.createdBy, Icon(Icons.people_outline, color: mainColor,)),
            _greyBar(TextData.share),
            _share(),
            _buildInfo(TextData.yarner, Icon(Icons.store, color: mainColor), onPressed: () async {
              await utils.launchURL(Platform.isAndroid ? yarnerAndroidLink : yarnerIosLink);
            }),
            _greyBar(TextData.contactUs),
            _email(),
            _greyBar(TextData.tC),
            _buildInfo(
                TextData.endUserLicenseAgreement,
                Icon(
                  MdiIcons.fileDocument,
                  color: mainColor,
                ),
                onPressed: () async {
                  await utils.launchURL(companyWebsiteLink + eulaLink);
                }
            ),
            _buildInfo(
                TextData.privacyPolicy,
                Icon(
                  MdiIcons.emailNewsletter,
                  color: mainColor,
                ),
                onPressed: () async {
                  await utils.launchURL(companyWebsiteLink + privacyPolicyUrl);
                }
            ),
            _greyBar(TextData.about),
            _buildInfo(
                "Third-party Software",
                Icon(
                  MdiIcons.blenderSoftware,
                  color: mainColor,
                ),
                onPressed: () {
                  showLicensePage(context: getRoute.getContext());
                }
            ),
            _buildInfo(
                "Icons provided by Icons8.com",
                Icon(
                  MdiIcons.simpleIcons,
                  color: mainColor,
                ),
                onPressed: () async {
                  await utils.launchURL(icon8Link);
                }
            ),
            _buildInfo(
                TextData.officialPage,
                Icon(
                  Icons.web,
                  color: mainColor,
                ),
                onPressed: () async {
                  await utils.launchURL(companyWebsiteLink);
                }
            ),
            _buildInfo("@2020 toppr of Yarns. All Rights Reserved.", Icon(Icons.copyright, color: mainColor,)),
          ],
        ),
      ),
    );
  }

  Widget _buildInfo(String info, Icon icon, {Function onPressed}) {
    return Center(
      child: ListTile(
        onTap: onPressed,
        leading: icon,
        title: Text(
          info,
          style: smallTextStyle,
        ),
      ),
    );
  }

  Widget _email() {
    return Center(
      child: ListTile(
        leading: Icon(
          Icons.email,
          color: mainColor,
        ),
        title: Text(
          TextData.email,
          style: smallTextStyle
        ),
        onTap: () {
          utils.emailToSpinnerOfYarns();
        },
      ),
    );
  }

  Widget _share() {
    return Center(
      child: ListTile(
        leading: Icon(
          Icons.share,
          color: mainColor,
        ),
        title: Text(
          TextData.share,
          style: smallTextStyle
        ),
        onTap: () {
          Share.share(shareMsg + "\n" + companyWebsiteLink);
        },
      ),
    );
  }

  Widget _ourFacebook() {
    return Center(
      child: ListTile(
        leading: Icon(
          MdiIcons.facebook,
          color: mainColor,
        ),
        title: Text(
          TextData.facebook,
          style: smallTextStyle
        ),
        onTap: () async {
          await utils.openFacebook("ofyarns");
        },
      ),
    );
  }

  Widget _ourTwitter() {
    return Center(
      child: ListTile(
        leading: Icon(
          MdiIcons.twitter,
          color: mainColor,
        ),
        title: Text(
            TextData.twitter,
          style: smallTextStyle
        ),
        onTap: () async {
          await utils.openTwitter("of_yarns");
        },
      ),
    );
  }

  Widget _greyBar(String msg) {
    return ElasticIn(
      child: Container(
          height: 23,
          width: double.infinity,
          decoration: BoxDecoration(
            color: unfocusedColor,
          ),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(left: 20.0),
                child: Text(
                  msg,
                  style: commonTextStyle
                ),
              )
          )
      ),
    );
  }
}
