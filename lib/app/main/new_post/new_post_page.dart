import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:badges/badges.dart';
import 'package:bubble/bubble.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:reorderables/reorderables.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:toppr/utils/bottom_sheet/image_bottom_sheet.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/view/dialogs/loading_dialog.dart';
import 'package:toppr/widgets/button/bottom_sheet_button.dart';
import 'package:toppr/widgets/button/custom_bouncing_widget.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';
import 'package:toppr/widgets/color_picker/color_picker.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/widgets/media/media_swipper.dart';
import 'package:toppr/widgets/tag/tag.dart';

class NewPostPage extends StatefulWidget {

  NewPostPage({Key key,}) : super(key: key);

  @override
  NewPostPageState createState() => NewPostPageState();
}

class NewPostPageState extends State<NewPostPage> {

  ImagePicker _picker;
  List<File> _imageFiles;
  bool _useLocation;
  PageController _pageController;
  String _currentColorCode;
  List<String> _imageLinks;
  TextEditingController _controller;
  FocusNode _focusNode;
  SwiperController _swipeController;
  GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    _imageLinks = [];
    _currentColorCode = myProfile.color;
    _useLocation = true;
    _swipeController = SwiperController();
    _pageController = PageController();
    resetImageFiles();
    _controller = TextEditingController();
    _focusNode = FocusNode();
    _picker = ImagePicker();
    super.initState();
  }

  void resetImageFiles() {
    _imageFiles = List.generate(allowedMediaPerPost, (index) => null);
    _imageLinks = [];
  }

  @override
  void dispose() {
    _swipeController.dispose();
    _pageController.dispose();
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> _setupImageLinks() async {
    for (int i = 0; i < allowedMediaPerPost; i++) {
      var _file = _imageFiles[i];
      if (_file != null) {
        /// Add ' at start and end to conform to backend's need
        _imageLinks.add("'" + await uploader.uploadFileToFirebase(_file, false, ImageType.post) + "'");
      }
    }
  }

  List<File> _validImageFiles() {
    List<File> _result = [];
    for (int i = 0; i < _imageFiles.length; i++) {
      if (_imageFiles[i] != null) {
        _result.add(_imageFiles[i]);
      }
    }
    return _result;
  }

  Future<void> _pickImage(ImageSource source) async {
    getRoute.pop();
    bool _allFull = false;
    if (source == ImageSource.camera) {
      /// If camera, use image picker
      PickedFile _pickedFile = await _picker.getImage(
        source: source,
      );
      if (_pickedFile != null) {
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            /// Replace null if have null
            _allFull = false;
            _imageFiles.removeAt(i);
            _imageFiles.insert(i, File(_pickedFile.path));
            break;
          } else {
            _allFull = true;
          }
        }
        if (_allFull) {
          /// Replace first if all have images
          _imageFiles.removeAt(0);
          _imageFiles.insert(0, File(_pickedFile.path));
        }
      }
    } else {
      List<int> _emptySlot = [];
      /// else use multiple image picker
      FilePickerResult _results = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      );
      if (_results != null) {
        if (_results.files.length > allowedMediaPerPost) {
          /// Make sure picked file number is acceptable
          _results.files.sublist(0, allowedMediaPerPost);
        }
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            _emptySlot.add(i);
          }
        }
        for (int i = 0; i < _results.files.length; i++) {
          if (_emptySlot.length >= i + 1) {
            _imageFiles.removeAt(_emptySlot[i]);
            _imageFiles.insert(_emptySlot[i], File(_results.files[i].path));
          } else {
            _imageFiles.removeAt(_results.files.length - i - 1);
            _imageFiles.insert(_results.files.length - i - 1, File(_results.files[i].path));
          }
        }
      }
    }
    refreshThisPage();
  }

  int _validImageCount() {
    int _count = 0;
    for (int i = 0; i < _imageFiles.length; i++) {
      if (_imageFiles[i] != null) _count++;
    }
    return _count;
  }

  void _onReorder(int oldIndex, int newIndex) {
    File _tempFile = _imageFiles[oldIndex];
    _imageFiles.removeAt(oldIndex);
    _imageFiles.insert(newIndex, _tempFile);
    refreshThisPage();
  }

  @override
  Widget build(BuildContext context) {

    return PageView(
      controller: _pageController,
      scrollDirection: Axis.vertical,
      physics: _validImageCount() > 0 ? null : NeverScrollableScrollPhysics(),
      children: [
        /// Page One
        Scaffold(
          persistentFooterButtons: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: GestureDetector(
                onTap: () {
                  if (_validImageCount() > 0) {
                    _pageController.nextPage(duration: kThemeAnimationDuration, curve: Curves.easeInOut);
                  } else {
                    utils.toast(TextData.insertImageFirst, isWarning: true);
                  }
                },
                child: Text(
                  TextData.next,
                  style: commonTextStyle,
                ),
              ),
            ),
          ],
          body: SingleChildScrollView(
            child:  Column(
              children: [
                ReorderableWrap(
                  runSpacing: 8.0,
                  padding: EdgeInsets.symmetric(vertical: 24.0),
                  children: List.generate(_imageFiles.length, (index) {
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 6.0),
                      child: GestureDetector(
                        onTap: () {
                          getGenericBottomSheet([
                            _imageFiles[index] != null ? Container() : BottomSheetButton(
                              onTap: () {
                                _pickImage(
                                  ImageSource.camera,
                                );
                              },
                              iconData: Icons.add_a_photo_outlined,
                              label: TextData.addFromCamera,
                            ),
                            _imageFiles[index] != null ? Container() : BottomSheetButton(
                              onTap: () {
                                _pickImage(
                                  ImageSource.gallery,
                                );
                              },
                              iconData: Icons.perm_media_outlined,
                              label: TextData.addFromGallery,
                            ),
                            BottomSheetButton(
                              onTap: () {
                                if (_imageFiles[index] != null) {
                                  _imageFiles.removeAt(index);
                                  if (_imageFiles.length != allowedMediaPerPost) {
                                    _imageFiles.add(null);
                                  }
                                  refreshThisPage();
                                }
                              },
                              iconData: Icons.clear_outlined,
                              label: TextData.removeImage,
                            ),
                          ]);
                        },
                        child: Badge(
                          position: BadgePosition(
                            top: 0,
                            end: 5,
                          ),
                          badgeColor: utils.getColorByHex(myProfile.color),
                          badgeContent: Text(
                            (index + 1).toString(),
                            style: smallTextStyle.merge(
                                TextStyle(
                                    fontSize: extraSmallFontSize
                                )
                            ),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(imageRadius),
                              border: Border.all(
                                color: utils.getColorByHex(myProfile.color),
                              ),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(imageRadius),
                              child: _imageFiles[index] != null ? Image.file(
                                _imageFiles[index],
                                fit: BoxFit.cover,
                                width: deviceWidth / 3.4,
                                height: deviceWidth / 2,
                              ) : Container(
                                width: deviceWidth / 3.4,
                                height: deviceWidth / 2,
                                color: lightUnfocusedColor,
                                child: Icon(
                                  MdiIcons.image,
                                  color: mainColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                  onReorder: _onReorder,
                ),
              ],
            ),
          ),
        ),
        /// Page Two
        Scaffold(
          persistentFooterButtons: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: GestureDetector(
                onTap: () async {
                  if (_formKey.currentState.validate()) {
                    if (_validImageCount() <= 0) {
                      utils.toast(TextData.insertImageFirst, isWarning: true);
                      _pageController.previousPage(duration: kThemeAnimationDuration, curve: Curves.easeInOut);
                    } else {
                      var flushbar = utils.getLoadingToast(TextData.posting);
                      flushbar..show(getRoute.getContext());
                      String _caption = _controller.text.trim();
                      _controller.clear();
                      _focusNode.unfocus();
                      _pageController.previousPage(duration: kThemeAnimationDuration, curve: Curves.easeInOut);
                      await _setupImageLinks().then((_) async {
                        double _long = 0;
                        double _lat = 0;
                        Position _position = await utils.getLocation();
                        if (_position != null) {
                          _long = _position?.longitude;
                          _lat = _position?.latitude;
                        }
                        await apiManager.createPost(_caption, _imageLinks, _long, _lat, _currentColorCode);
                        resetImageFiles();
                        refreshThisPage();
                        flushbar..dismiss(getRoute.getContext());
                      });
                    }
                  }
                },
                child: Text(
                  TextData.top,
                  style: commonTextStyle.merge(
                    TextStyle(
                      color: utils.getColorByHex(myProfile.color),
                    )
                  ),
                ),
              ),
            ),
          ],
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(imageRadius),
                            border: Border.all(
                              color: utils.getColorByHex(myProfile.color),
                            )
                          ),
                          child: Swiper(
                            itemCount: _validImageFiles().length,
                            autoplay: false,
                            controller: _swipeController,
                            itemWidth: deviceWidth,
                            itemBuilder: (ctx, newIndex) {

                              return ClipRRect(
                                borderRadius: BorderRadius.circular(imageRadius),
                                child: Image.file(
                                  _validImageFiles()[newIndex],
                                  fit: BoxFit.cover,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                        width: deviceWidth - 100 - 24,
                        padding: EdgeInsets.only(top: 24.0, right: 12.0,),
                        child: Form(
                          key: _formKey,
                          child: TextFormField(
                            controller: _controller,
                            focusNode: _focusNode,
                            maxLines: 3,
                            maxLength: 300,
                            maxLengthEnforced: true,
                            validator: (String text) {
                              if (text.isNullOrEmpty) {
                                return "Caption must not be empty";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: TextData.caption,
                              hintStyle: commonTextStyle.merge(
                                TextStyle(
                                  color: unfocusedColor
                                )
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: deepUnfocusedColor,
                                ),
                                borderRadius: BorderRadius.circular(commonRadius),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: utils.getColorByHex(myProfile.color),
                                ),
                                borderRadius: BorderRadius.circular(commonRadius),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  StatefulBuilder(
                    builder: (ctx, customState) {
                      return SwitchListTile.adaptive(
                        title: Text(
                          TextData.useLocation,
                          style: commonTextStyle,
                        ),
                        secondary: Icon(
                          Icons.pin_drop,
                          color: mainColor,
                        ),
                        onChanged: (bool newValue) {
                          _useLocation = !_useLocation;
                          customState(() {});
                        },
                        value: _useLocation,
                      );
                    }
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ColorPicker(
                    showBlackWhite: true,
                    title: TextData.pickTextColor,
                    onPressed: (String newColor) {
                      _currentColorCode = newColor;
                    },
                    currentColorCode: _currentColorCode,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
