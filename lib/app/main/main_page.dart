import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/main/instant_message/instant_message_page.dart';
import 'package:toppr/app/main/new_post/new_post_page.dart';
import 'package:toppr/app/main/settings/settings_page.dart';
import 'package:toppr/app/post/post_grid_page.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/animation/fade_indexed_stack.dart';
import 'package:toppr/widgets/button/gradient_icon_button.dart';

import 'discover/discover_page.dart';

class MainPage extends StatefulWidget {

  MainPage({
    Key key,
  }) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  int _currentIndex;
  int _homeIndex = 0;
  int _discoverIndex = 1;
  int _newPostIndex = 2;
  int _chatIndex = 3;
  int _myProfileIndex = 4;
  int _settingsIndex = 5;
  List<Widget> _stack;
  GlobalKey<DiscoverPageState> _discoverKey = GlobalKey();
  GlobalKey _followingKey = GlobalKey();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _stack = [
      PostPage(
        key: _followingKey,
      ),
      DiscoverPage(
        key: _discoverKey,
      ),
      NewPostPage(),
      InstantMessagePage(),
      ViewProfilePage(profile: myProfile),
      SettingsPage(),
    ];
    _currentIndex = _homeIndex;
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(deviceWidth, kToolbarHeight),
        child: SafeArea(
          child: Container(
            height: kToolbarHeight,
            child: Material(
              elevation: commonElevation,
              color: defaultWhiteColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: List.generate(_stack.length, (index) {
                  return IconButton(
                    icon: Icon(
                      _currentIndex == index ?
                          /// Selected index
                      index == _homeIndex ? Icons.home : index == _chatIndex ? MdiIcons.chat : index == _discoverIndex ? MdiIcons.earth : index == _newPostIndex ? Icons.add_a_photo_rounded : index == _myProfileIndex ? MdiIcons.accountBox : Icons.settings
                          :
                          /// Non-selected index
                      index == _homeIndex ? Icons.home_outlined : index == _chatIndex ? MdiIcons.chatOutline : index == _discoverIndex ? MdiIcons.earth : index == _newPostIndex ? Icons.add_a_photo_outlined : index == _myProfileIndex ? MdiIcons.accountBoxOutline : Icons.settings_outlined,
                      color: mainColor,
                    ),
                    onPressed: () {
                      if (_currentIndex != index) {
                        _discoverKey.currentState.unfocus();
                        _currentIndex = index;
                        refreshThisPage();
                      }
                    },
                  );
                }),
              ),
            ),
          ),
        ),
      ),
      body: FadeIndexedStack(
        index: _currentIndex,
        children: _stack,
      ),
    );
  }
}


