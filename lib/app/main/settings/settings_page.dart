import 'dart:async';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:toppr/app/about/about_page.dart';
import 'package:toppr/app/landing/landing_page.dart';
import 'package:toppr/app/post/post_grid_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/binary_dialog.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';

class SettingsPage extends StatefulWidget {

  SettingsPage({Key key,}) : super(key: key);

  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    Map<String, Widget> _detailMap = {
      TextData.history: PostGridPage(
        posts: [],
        title: TextData.history,
        postType: PostType.history,
      ),
      TextData.likedPost: PostGridPage(
        posts: [],
        title: TextData.likedPost,
        postType: PostType.like,
      ),
      TextData.aboutUs: AboutPage(),
//    TextData.userExperienceSettings: UserExperienceSettings(),
    };

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.separated(
              shrinkWrap: true,
              itemCount: _detailMap.length,
              separatorBuilder: (ctx, index) {
                return Divider(
                  color: unfocusedColor,
                  height: 1,
                );
              },
              itemBuilder: (ctx, index) {
                return InkWell(
                  onTap: () {
                    getRoute.navigateTo(_detailMap.values.toList()[index]);
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20.0, bottom: 20.0, right: 10.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            _detailMap.keys.toList()[index],
                            style: bigTextStyle,
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.navigate_next,
                            color: mainColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          Flexible(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: LongElevatedButton(
                color: warning,
                title: TextData.logout,
                onPressed: () {
                  getBinaryDialog(TextData.logout, TextData.areYouSure, () {
                    utils.logout();
                  });
                },
              ),
            ),
          ),
          SizedBox(
            height: deviceHeight * 0.05,
          ),
        ],
      ),
    );
  }
}
