import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/chat/chat_page.dart';
import 'package:toppr/app/post/post_grid_page.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/profile/edit_profile_page.dart';
import 'package:toppr/app/relationship/view_relationship_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/physics/avoid_implicit_scroll.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/wrong_token_dialog.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/button/bordered_flat_button.dart';
import 'package:toppr/widgets/button/follow_button.dart';
import 'package:toppr/widgets/button/long_elevated_button.dart';
import 'package:toppr/widgets/button/popup_menu.dart';
import 'package:toppr/widgets/display/tag_name_display.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'package:toppr/widgets/media/image/profile_background.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/no_data/no_data.dart';
import 'package:toppr/widgets/profile/count.dart';
import 'package:toppr/widgets/profile/profile_holder.dart';
import 'package:toppr/widgets/textfield/search_textfield.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class DiscoverPage extends StatefulWidget {

  DiscoverPage({
    Key key,
  }) : super(key: key);

  @override
  DiscoverPageState createState() => DiscoverPageState();
}

class DiscoverPageState extends State<DiscoverPage> {

  TextEditingController _controller;
  FocusNode _node;
  List<UserProfile> _profiles;
  bool _noProfiles;
  bool showLoading;
  bool _isDiscoverPost;

  @override
  void initState() {
    _isDiscoverPost = true;
    showLoading = false;
    _noProfiles = false;
    _profiles = [];
    _controller = TextEditingController();
    _node = FocusNode();
    super.initState();
  }

  Future<void> handleRefresh() async {
    _profiles.clear();
    _isDiscoverPost = false;
    showLoading = true;
    refreshThisPage();
    await getProfiles();
    showLoading = false;
    refreshThisPage();
  }

  Future<void> getProfiles() async {
    await apiManager.searchUser(_controller.text).then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      switch (jsonStatus) {
        case StatusCode.ok:
          _profiles.clear();
          List<dynamic> listOfProfiles = jsonResponse[DataKey.profiles];
          var _newProfiles = UserProfile.generateUserProfile(listOfProfiles);
          _profiles..addAll(List<UserProfile>.from(UserProfile.filterUserList(_newProfiles, _newProfiles)));
          _noProfiles = _profiles.isEmpty;
          break;
        case StatusCode.wrongToken:
          getWrongTokenDialog();
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  @override
  void dispose() {
    _node.dispose();
    _controller.dispose();
    super.dispose();
  }

  void unfocus() {
    _node.unfocus();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: appBgColor,
      body: NestedScrollView(
        physics: NoImplicitScrollPhysics(parent: ScrollPhysics()),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          // These are the slivers that show up in the "outer" scroll view.
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverSafeArea(
                top: false,
                bottom: false,
                sliver: SliverAppBar(
                  backgroundColor: appBgColor,
                  centerTitle: false,
                  flexibleSpace: FlexibleSpaceBar(
                    background: SafeArea(
                      child: Center(
                        child: SearchTextField(
                          title: TextData.searchUser,
                          leadingTag: userTag,
                          onChangedRefresh: (String query) {
                            handleRefresh();
                          },
                          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                          controller: _controller,
                          focusNode: _node,
                          suffix: clearTextFieldButton(_controller, onPressed: () {
                            if (!_isDiscoverPost) {
                              _node.unfocus();
                              _isDiscoverPost = true;
                              refreshThisPage();
                            }
                          }),
                        ),
                      ),
                    ),
                  ),
                  expandedHeight: 100,
                  floating: true,
                  pinned: false,
                  snap: false,
                  primary: true,
                  automaticallyImplyLeading: false,
                  forceElevated: false,
                ),
              ),
            ),
          ];
        },
        body: AnimatedSwitcher(
          duration: kThemeAnimationDuration,
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(child: child, scale: animation);
          },
          child: _isDiscoverPost ? PostGridPage(
            key: UniqueKey(),
            postType: PostType.discover,
            title: TextData.explore,
            needAppBar: false,
          ) : showLoading ? LoadingWidget() : _noProfiles ? NoData(
            text: TextData.noProfile,
          ) : ListView.builder(
            addAutomaticKeepAlives: true,
            scrollDirection: Axis.vertical,
            itemCount: _profiles.length,
            itemBuilder: (context, index) {
              return ProfileHolder(profile: _profiles[index]);
            },
          )
        ),
      ),
    );
  }
}
