import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:toppr/app/main/main_page.dart';
import 'package:toppr/app/registration/registration_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/view.dart';
import 'package:toppr/widgets/button/footer_button.dart';
import 'package:toppr/widgets/button/loading_long_elevated_button.dart';
import 'package:toppr/widgets/textfield/email_textfield.dart';
import 'package:toppr/widgets/textfield/password_textfield.dart';
import 'package:toppr/widgets/textfield/username_textfield.dart';

class LoginPage extends StatefulWidget {

  LoginPage({
    Key key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController _emailController;
  TextEditingController _passwordController;
  FocusNode _emailNode;
  FocusNode _passwordNode;
  GlobalKey<FormState> _formKey = GlobalKey();

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _emailNode = FocusNode();
    _passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _passwordNode.dispose();
    _emailNode.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      persistentFooterButtons: [
        FooterButton(
          title: TextData.signUpInstead,
          onPressed: () {
            getRoute.pop();
            getRoute.navigateTo(RegistrationPage());
          },
        ),
      ],
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: deviceHeight * 0.25,
              ),
              getAppName(),
              SizedBox(
                height: 20,
              ),
              EmailTextField(
                controller: _emailController,
                focusNode: _emailNode,
                nextFocusNode: _passwordNode,
              ),
              PasswordTextField(
                controller: _passwordController,
                focusNode: _passwordNode,
                nextFocusNode: _emailNode,
              ),
              SizedBox(
                height: 20,
              ),
              LoadingLongElevatedButton(
                title: TextData.login,
                onPressed: (startLoading, stopLoading, btnState) async {
                  if (_formKey.currentState.validate()) {
                    _emailNode.unfocus();
                    _passwordNode.unfocus();
                    if (btnState == ButtonState.Idle) {
                      startLoading();
                      bool isSuccess = await apiManager.login(_emailController.text, _passwordController.text);
                      if (isSuccess) {
                        await apiManager.getMyProfile();
                        _emailController.clear();
                        _passwordController.clear();
                        getRoute.navigateToAndPopAll(MainPage());
                      } else {
                        utils.toast(TextData.incorrectCredential, isWarning: true);
                      }
                      stopLoading();
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}


