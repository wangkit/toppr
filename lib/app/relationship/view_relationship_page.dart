import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/physics/avoid_implicit_scroll.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/wrong_token_dialog.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/profile/profile_holder.dart';
import 'package:toppr/widgets/textfield/search_textfield.dart';

class ViewRelationshipPage extends StatefulWidget {

  final UserProfile profile;
  final bool isFollower;

  ViewRelationshipPage({
    Key key,
    @required this.profile,
    @required this.isFollower,
  }) : super(key: key);

  @override
  _ViewRelationshipPageState createState() => _ViewRelationshipPageState();
}

class _ViewRelationshipPageState extends State<ViewRelationshipPage> {

  bool showLoading;
  List<UserProfile> _profiles;
  bool _noProfiles;
  Future<void> _loading;
  RefreshController _refreshController;
  TextEditingController _searchController;
  FocusNode _searchNode;

  @override
  void dispose() {
    _refreshController.dispose();
    _searchController.dispose();
    _searchNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _searchController = TextEditingController();
    _searchNode = FocusNode();
    _refreshController = RefreshController(
      initialRefresh: false
    );
    showLoading = false;
    _noProfiles = false;
    _profiles = [];
    _loading = handleRefresh();
    super.initState();
  }

  Future<void> handleRefresh({bool onPullDown = false, bool onScrollDown = false, bool onSearching = false}) async {
    final Completer<void> completer = Completer<void>();
    if (onSearching) {
      _profiles.clear();
      _noProfiles = false;
      showLoading = true;
      refreshThisPage();
    }
    int startFrom = onScrollDown ? _profiles.length : 0;
    await getProfiles(onScrollDown, startFrom);
    refreshThisPage();
    showLoading = false;
    completer.complete();
    return completer.future.then<void>((_) {
      if (onPullDown) {
        _refreshController.refreshCompleted();
      }
      if (onScrollDown) {
        _refreshController.loadComplete();
      }
    });
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> getProfiles(bool onScrollDown, int startFrom) async {
    await apiManager.getFollow(widget.profile.id, widget.isFollower, startFrom, _searchController.text).then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      if (!onScrollDown) _profiles.clear();
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfProfiles = jsonResponse[DataKey.profiles];
          var _newProfiles = UserProfile.generateUserProfile(listOfProfiles);
          _profiles..addAll(List<UserProfile>.from(_newProfiles));
          _noProfiles = _profiles.isEmpty;
          break;
        case StatusCode.wrongToken:
          getWrongTokenDialog();
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: widget.isFollower ? "${widget.profile.username} followers" : "${widget.profile.username} following",
      ),
      backgroundColor: appBgColor,
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SearchTextField(
            controller: _searchController,
            focusNode: _searchNode,
            leadingTag: userTag,
            title: TextData.username.toLowerCase(),
            onChangedRefresh: (String text) {
              handleRefresh(onSearching: true);
            },
          ),
          Flexible(
            child: FutureBuilder(
              future: _loading,
              builder: (context, projectSnap) {
                switch (projectSnap.connectionState) {
                  default:
                    return projectSnap.connectionState != ConnectionState.done || showLoading ? LoadingWidget() :
                    _noProfiles ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            MdiIcons.dog,
                            color: Colors.amber,
                            size: kToolbarHeight * 2,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            TextData.emptyAsSpace,
                            style: commonTextStyle,
                          ),
                        ],
                      ),
                    ) : SmartRefresher(
                      controller: _refreshController,
                      enablePullUp: true,
                      onRefresh: () => handleRefresh(onPullDown: true),
                      onLoading: () => handleRefresh(onScrollDown: true),
                      child: ListView.builder(
                        addAutomaticKeepAlives: true,
                        scrollDirection: Axis.vertical,
                        itemCount: _profiles.length,
                        itemBuilder: (context, index) {
                          return ProfileHolder(profile: _profiles[index]);
                        },
                      ),
                    );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
