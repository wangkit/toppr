import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';

class Dialogs {
  static Future<void> showLoadingDialog({String title}) async {
    return showModal<void>(
        context: getRoute.getContext(),
        configuration: FadeScaleTransitionConfiguration(
          barrierDismissible: false,
        ),
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  backgroundColor: appBgColor,
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: [
                          LoadingWidget(),
                          Container(height: 20,),
                          Text(
                            title ?? TextData.pleaseWait,
                            style: commonTextStyle,
                          ),
                      ]
                    ),
                  )
                ]
              )
          );
        }
      );
  }
}