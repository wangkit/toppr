import 'package:animations/animations.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/view/dialogs/custom_dialog.dart';
import 'package:toppr/widgets/button/toggle_button/custom_toggle_buttons.dart';
import 'package:toppr/widgets/dialog/negative_button.dart';
import 'package:toppr/widgets/dialog/positive_button.dart';
import 'package:toppr/widgets/tag/tag.dart';

getBlockDialog(String targetId, String name, String chatId, Function onBlock) {

  List<bool> _tempList = List.generate(BlockReason.blockMap.length, (index) => index == BlockReason.noReason ? true : false);

  int _findReason() {
    for (int i = 0; i < _tempList.length; i++) {
      if (_tempList[i]) {
        return i;
      }
    }
    return -1;
  }

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          "$name will never be able to interact with you again. This chat and all messages will be deleted. This process is irreversible. Are you sure?",
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: StatefulBuilder(
            builder: (ctx, customState) {
              return CustomToggleButtons(
                  renderBorder: false,
                  isSelected: _tempList,
                  children: List.generate(BlockReason.blockMap.length, (index) => Tag(
                    key: Key(BlockReason.blockMap.values.toList()[index] + _tempList[index].toString()),
                    onPressed: () {
                      for (int i = 0; i < _tempList.length; i++) {
                        _tempList[i] = false;
                      }
                      customState(() {
                        _tempList[index] = true;
                      });
                    },
                    colorCode: _tempList[index] ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                    text: BlockReason.blockMap.values.toList()[index],
                    textColor: mainColor,
                  ),
                ),
              );
            },
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.cancel, color: unfocusedColor,),
          PositiveButton(onPressed: () {
            int _reason = _findReason();
            if (_reason >= 0) {
              getRoute.pop();
              if (onBlock != null) {
                onBlock();
              }
//              utils.deleteChat(false, targetId);
//              utils.clearMessageInChat(chatId, targetId);
              apiManager.block(targetId, _findReason());
            } else {
              utils.toast(TextData.missingReason, isWarning: true);
            }
          }, color: errorColor, text: TextData.block, isHalfWidth: true, autoPop: false,),
        ],
      );
    },
  );
}