import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/dialog/negative_button.dart';
import 'package:toppr/widgets/dialog/positive_button.dart';

import 'custom_dialog.dart';

getBinaryDialog(String title, String content, Function onPressed,
    {Color negativeColor = warning, Color positiveColor = success, String positiveText = TextData.yes, String negativeText = TextData.no,
      Function negativeOnPressed, bool barrierDismissible = true, Function onDismiss, bool needNegativeButton = true}) {
  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: barrierDismissible,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: Text(
            content,
            style: DialogConfig.dialogMessageStyle,
          ),
        ),
        actions: <Widget>[
          needNegativeButton ? NegativeButton(text: negativeText, color: negativeColor, onPressed: negativeOnPressed,) : Container(),
          PositiveButton(onPressed: onPressed, color: positiveColor, text: positiveText, isHalfWidth: needNegativeButton,),
        ],
      );
    },
  ).then((_) {
    if (onDismiss != null) {
      onDismiss();
    }
  });
}