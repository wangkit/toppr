import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/dialog/negative_button.dart';

import 'custom_dialog.dart';

getWrongTokenDialog() {

  void onPressed() async {
    getRoute.pop();
    utils.logout();
  }

  return showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(isBarrierDismissible: false),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: ListTile(
          leading: Icon(
            MdiIcons.alert,
            color: warning,
          ),
          title: Text(
            TextData.expiredSession,
            textAlign: TextAlign.center,
            style: commonTextStyle,
          ),
        ),
        backgroundColor: Colors.white,
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: Text(
            TextData.noSession,
            style: commonTextStyle,
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.close, onPressed: onPressed, isHalfWidth: false),
        ],
      );
    },
  );
}