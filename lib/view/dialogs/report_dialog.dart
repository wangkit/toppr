import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/view/dialogs/custom_dialog.dart';
import 'package:toppr/widgets/dialog/negative_button.dart';
import 'package:toppr/widgets/dialog/positive_button.dart';

getReportDialog(String targetId, String name, Function onReport) {

  TextEditingController _controller = TextEditingController();
  GlobalKey<FormState> _fieldKey = GlobalKey<FormState>();
  int _maximum = 500;

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          "Report $name",
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: Form(
            key: _fieldKey,
            child: TextFormField(
              controller: _controller,
              maxLines: null,
              maxLength: _maximum,
              style: commonTextStyle,
              validator: (value) {
                if (value.isNullOrEmpty) {
                  return TextData.missingReason;
                }
                if (value.length > _maximum) {
                  return TextData.textTooLong;
                }
                return null;
              },
              decoration: InputDecoration(
                hintStyle: commonTextStyle.merge(
                    TextStyle(
                      color: unfocusedColor,
                    )
                ),
                hintText: TextData.reason,
              ),
            ),
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.cancel, color: unfocusedColor,),
          PositiveButton(onPressed: () async {
            if (_fieldKey.currentState.validate()) {
              getRoute.pop();
              if (onReport != null) {
                onReport();
              }
              await apiManager.reportUser(targetId, _controller.text).then((status) {
                if (status == StatusCode.ok) {
                  utils.toast(TextData.reportSuccessfully, isSuccess: true);
                }
              });
            }
          }, color: errorColor, text: TextData.report, isHalfWidth: true, autoPop: false,),
        ],
      );
    },
  );
}