import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

Widget bottomSheetPullIndicator({double padding = 8.0}) {
  return Padding(
    padding: EdgeInsets.all(padding),
    child: Divider(color: unfocusedColor, thickness: 4, indent: deviceWidth * 0.43, endIndent: deviceWidth * 0.43,),
  );
}

IconButton clearTextFieldButton(TextEditingController controller, {Function onPressed}) {
  return IconButton(
    icon: Icon(
      Icons.clear,
      color: unfocusedColor,
      size: 20,
    ),
    onPressed: () {
      if (onPressed != null) {
        onPressed();
      }
      WidgetsBinding.instance.addPostFrameCallback((_) => controller?.clear());
    },
  );
}

Widget getAppName() {
  return Text(
    appName,
    style: bigTextStyle.merge(
        TextStyle(
          fontSize: 38,
          fontFamily: 'crazyHeader',
          fontWeight: FontWeight.bold,
        )
    ),
  );
}