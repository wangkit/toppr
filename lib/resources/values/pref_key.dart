class PrefKey {
  static String myId = "myIdKey";
  static String accountColor = "accountColorKey";
  static String recentEmoji = "emojiRecentKey";
  static String uniqueId = "uniqueIdKey";
  static String secret = "secret";
  static String hasCalledFCMRegistration = "hasCalledFCMRegistrationKey";
}