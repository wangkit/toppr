class ReplyTable {
  static String replyId = "replyId";
  static String postId = "postId";
  static String id = "id";
  static String username = "username";
  static String reply = "reply";
  static String createdAt = "createdAt";
  static String likedProfileIds = "likedProfileIds";
  static String replyReplyProfileIds = "replyReplyProfileIds";
  static String replyReplyCount = "replyReplyCount";
}