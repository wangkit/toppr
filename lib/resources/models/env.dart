import 'package:meta/meta.dart';

enum BuildFlavor { production, development, staging }

BuildEnvironment get env => _env;
BuildEnvironment _env;

class BuildEnvironment {
  /// Backend url
  final String baseUrl;

  /// Environment as stated in enum in line 3
  final BuildFlavor flavor;

  BuildEnvironment._init({this.flavor, this.baseUrl});

  /// Init only once
  static void init({@required flavor, @required baseUrl,}) =>
      _env ??= BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl,);
}