import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geocoding/geocoding.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

import 'firestore_tables.dart';

class UserProfile {

  String id;
  String username;
  String color;
  String email;
  String bio;
  String image;
  int status;
  bool isVerified;
  String updatedAt;
  String createdAt;
  String backgroundImage;
  String lastActive;
  String fcmSecret;
  int followerCount;
  int followingCount;
  int postCount;
  bool isFollowingMe;
  bool amIFollowing;
  bool blockedMe;

  UserProfile({
    this.id,
    this.email,
    this.username,
    this.color,
    this.fcmSecret,
    this.bio,
    this.backgroundImage,
    this.amIFollowing,
    this.isFollowingMe,
    this.image,
    this.status,
    this.isVerified,
    this.updatedAt,
    this.followerCount,
    this.followingCount,
    this.postCount,
    this.createdAt,
    this.lastActive,
    this.blockedMe,
  });

  /// Filtering duplicate simpler user profile
  static List<UserProfile> filterUserList(List<UserProfile> currentUsers, List<UserProfile> newUsers) {
    List<String> _currentForumIdList = [];
    List<UserProfile> _trueNewUsersList = [];
    currentUsers.forEach((user) {
      _currentForumIdList.add(user.id);
    });
    newUsers.forEach((user) {
      if (!_currentForumIdList.contains(user.id)) {
        _trueNewUsersList.add(user);
      }
    });
    return _trueNewUsersList;
  }

  static List<UserProfile> generateUserProfile(List targetList) {
    return List<UserProfile>.generate(targetList.length, (index) {

      var _thisProfile = targetList[index];

      return UserProfile(
        id: _thisProfile[0],
        email: _thisProfile[1],
        username: _thisProfile[2],
        image: _thisProfile[3],
        bio: _thisProfile[4],
        color: _thisProfile[5],
        backgroundImage: _thisProfile[6] ?? backgroundProfilePlaceholderLink,
        isVerified: _thisProfile[7],
        lastActive: _thisProfile[8],
        updatedAt: _thisProfile[9],
        createdAt: _thisProfile[10],
        fcmSecret: _thisProfile[11],
        followerCount: _thisProfile[12],
        followingCount: _thisProfile[13],
        postCount: _thisProfile[14],
        amIFollowing: _thisProfile[15],
        isFollowingMe: _thisProfile[16],
        blockedMe: _thisProfile[17],
      );
    });
  }
}

class Reply {

  String postId;
  String replyId;
  String createdAt;
  String username;
  String replyMessage;
  List<String> likedProfileIds;
  List<String> replyReplyProfileIds;
  int replyReplyCount;
  String id;

  Reply({
    this.id,
    this.postId,
    this.username,
    this.replyReplyCount,
    this.replyId,
    this.replyReplyProfileIds,
    this.replyMessage,
    this.likedProfileIds,
    this.createdAt,
  });

  static List<Reply> generateReply(List<DocumentSnapshot> targetList) {
    try {
      return List<Reply>.generate(targetList.length, (index) {

        var _thisReply = targetList[index].data();

        return Reply(
          createdAt: timeUtils.getTimeDiffString(timeUtils.getDateTimeFromTimestamp(_thisReply[ReplyTable.createdAt])),
          id: _thisReply[ReplyTable.id],
          username: _thisReply[ReplyTable.username],
          replyMessage: _thisReply[ReplyTable.reply],
          replyId: _thisReply[ReplyTable.replyId],
          replyReplyProfileIds: _thisReply[ReplyTable.replyReplyProfileIds].cast<String>(),
          replyReplyCount: _thisReply[ReplyTable.replyReplyCount],
          postId: _thisReply[ReplyTable.postId],
          likedProfileIds: _thisReply[ReplyTable.likedProfileIds].cast<String>(),
        );
      });
    } on Exception catch (e) {
      print("Generate reply error: $e");
      return [];
    }
  }
}

class Post {

  String postId;
  String id;
  String profileUsername;
  String profileColor;
  String caption;
  bool liked;
  String profileImage;
  List<String> images;
  int likeCount;
  List<String> tags;
  double longitude;
  double latitude;
  String location;
  String createdAt;
  String video;
  String videoThumbnail;
  String textColor;

  Post({
    this.id,
    this.postId,
    this.profileUsername,
    this.profileColor,
    this.caption,
    this.profileImage,
    this.images,
    this.likeCount,
    this.video,
    this.liked,
    this.videoThumbnail,
    this.location,
    this.tags,
    this.longitude,
    this.latitude,
    this.createdAt,
    this.textColor,
  });

  static Future<void> setLocation(List<Post> postList) async {
    for (int i = 0; i < postList.length; i++) {
      postList[i].location = await utils.getAddress(postList[i]);
    }
  }

  static List<Post> generatePost(List targetList) {
    return List<Post>.generate(targetList.length, (index) {

      var _thisPost = targetList[index];
      var _creatorDetail = _thisPost[2];

      return Post(
        postId: _thisPost[0],
        id: _thisPost[1],
        profileColor: _creatorDetail[0],
        profileUsername: _creatorDetail[1],
        profileImage: _creatorDetail[2],
        caption: _thisPost[3],
        images: _thisPost[4].cast<String>(),
        tags: _thisPost[5].cast<String>(),
        longitude: _thisPost[6],
        latitude: _thisPost[7],
        likeCount: _thisPost[8],
        createdAt: _thisPost[9],
        video: _thisPost[10],
        videoThumbnail: _thisPost[11],
        liked: _thisPost[12],
        textColor: _thisPost[13],
      );
    });
  }
}

class BlockReason {
  static const int verbalAbuse = 0;
  static const int sexualHarassment = 1;
  static const int annoying = 2;
  static const int unreasonable = 3;
  static const int noReason = 4;

  static const Map<int, String> blockMap = {
    verbalAbuse: "Verbal Abuse",
    sexualHarassment: "Sexual Harassment",
    annoying: "Annoying",
    unreasonable: "Unreasonable",
    noReason: "No Reason",
  };
}

class MessageItem {
  static String messageId = 'messageId';
  static String message = 'message';
  static String createdAt = 'createdAt';
  static String senderName = 'senderName';
  static String receiverName = 'receiverName';
  static String senderId = 'senderId';
  static String chatId = 'chatId';
  static String receiverId = 'receiverId';
}

class ChatTable {
  static String tableName = 'chat';
  static String senderId = 'senderId';
  static String updatedAt = 'updatedAt';
  static String lastMessage = 'lastMessage';
  static String targetId = 'targetId';
  static String targetUsername = 'targetUsername';
  static String createdAt = 'createdAt';
  static String isLastMessageMyself = 'isLastMessageMyself';
  static String hasUnread = 'hasUnread';
  static String targetImage = 'targetImage';
  static String targetColor = 'targetColor';
}

class ChatItem {
  String senderId;
  String lastMessage;
  String updatedAt;
  String targetId;
  String targetUsername;
  String createdAt;
  bool hasUnread;
  bool isLastMessageMyself;
  String targetImage;
  String targetColor;

  ChatItem({
    this.senderId,
    this.targetId,
    this.updatedAt,
    this.targetColor,
    this.targetImage,
    this.isLastMessageMyself,
    this.targetUsername,
    this.createdAt,
    this.hasUnread,
    this.lastMessage,
  });
}
