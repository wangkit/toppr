import 'package:flutter/material.dart';

const btnYellow = const Color(0xFFe5c454);
const grandGrey = const Color(0xffbfc8d7);
const greenSheen = const Color(0xFF72BDA3);
const colorPrimary = const Color(0xFF008577);
const colorPrimaryDark = const Color(0xFF00574B);
const colorAccent = const Color(0xFFD81B60);
const textColor = const Color(0xFFFFF5EE);
const white = const Color(0xFFFBFBFB);
const mild_purpler = const Color(0xFF94849B);
const mild_green = const Color(0xFF7E9181);
Color lightUnfocusedColor = Colors.grey[300];
Color unfocusedColor = Colors.grey[400];
Color deepUnfocusedColor = Colors.grey[600];
const mild_yellow = const Color(0xFFD1BE9C);
const splitComplementRed = const Color(0xffff0800);
const splitComplementPurple = const Color(0xffff00f8);
const splitComplementBlue = const Color(0xff8700ff);
const splitComplementBrown = const Color(0xffff8700);

const green_blue = const Color(0xFF009FB7);
const pageBackgroundColor = const Color(0xFFfff);
const golden = const Color(0xFFFFDF00);
const grey_black = const Color(0xFF221E1D);
const black = const Color(0xFF000);
const asdRed = const Color(0xffbc2424);
const tomato = const Color(0xFFFF6347);
const tiffanyBlue = const Color(0xff81d8d0);
const candyGreen = const Color(0xffd3f8e2);
const candyPurple = const Color(0xffe4c1f9);
const candyPink = const Color(0xfff694c1);
const webPink = const Color(0xffffbad2);
const candyYellow = const Color(0xffede7b1);
const light_sky_blue = const Color(0xFF87CEFA);
const titleGrey = const Color(0xffB6B6B6);
const textBlue = const Color(0xff666679);
const light_purple_blue = const Color(0xffefefff);
const light_tomato = const Color(0xFFffa695);
const black_overlay_80 = const Color(0xFF80000000);
const thistle = const Color(0xFFD8BFD8);
const black_overlay_66 = const Color(0xFF66000000);
const alice_blue = const Color(0xFFF0F8FF);
const light_pink = const Color(0xFFFF9494);
const corn_silk = const Color(0xFFFFF8DC);
const errorColor = const Color(0xFFB00020);
const facebook = const Color(0xFF3b5998);
const blue = const Color(0xFF0044FF);
const linkColor = Colors.blueAccent;
const brown = const Color(0xff472300);
const navy_blue = const Color(0xFF000080);
const darkslateblue = const Color(0xFF483D8B);
const mediumslateblue = const Color(0xFF7b68ee);
const navy_blue_again = const Color (0xff002447);
const navy_light_blue = const Color (0xff53d3d1);
const pale_skin = const Color (0xfffbeceb);
const little_yellow = const Color (0xfffeb249);
const ink_green = const Color(0xff02820a);
const indigo = const Color(0xFF4B0082);
const maroon = const Color(0xFF800000);
const orange_red = const Color(0xFFFF4500);
const yellow = const Color(0xFFFFFF00);
const success = const Color(0xFF6cfca4);
const light_grey = const Color(0xFFD8D8D8);
const grey = const Color(0xFF585858);
const dark_blue = const Color(0xFF0B0B61);
const old_lace = const Color(0xFFFDF5E6);
const light_yellow_purple = const Color(0xFFb792f2);
const light_light_light_blue = const Color(0xFF0080FF);
const dark_red = const Color(0xFFB40404);
const almond = const Color(0xFFFFEBCD);
const dark_green = const Color(0xFF0B6138);
const light_green = const Color(0xFF088A4B);
const nice_blue = const Color(0xFF659DBD);
const silver = const Color(0xFFC0C0C0);
const dark_grey = const Color(0xFFA9A9A9);
const nice_green_blue = const Color(0xFF41B3A3);
const mint = const Color(0xFFC3DBC5);
const grey_blue = const Color(0xFF557a95);
const light_grey_blue = const Color(0xFF7395ae);
const sharp_pink = const Color(0xFFFF308E);
const sharp_orange = const Color(0xFFFA9301);
const sharp_yellow = const Color(0xFFF8D411);
const sharp_blue = const Color(0xFF20CBF3);
const shit = const Color(0xFF989572);
const light_blue = const Color(0xFFCFFCFF);
const sharp_purple = const Color(0xFF6F26FA);
const sharp_green = const Color(0xFF49CC5C);
const skin_color = const Color(0xFFFC9F5B);
const light_purple = const Color(0xFFF7F0F5);
const pale_yellow = const Color(0xFFE2D686);
const sharp_sharp_green = const Color(0xFF5EFC8D);
const story_bg = const Color(0xFFEDC9AF);
const yellow_green = const Color(0xFFC7CB85);
const pale_purple = const Color(0xFFB0A3D4);
const snow = const Color(0xFFFFFAFA);
const honeydew = const Color(0xFFF0FFF0);
const linen = const Color(0xFFFAF0E6);
const beige = const Color(0xFFF5F5DC);
const light_skin = const Color(0xFFF7DBA7);
const ghostwhite = const Color(0xFFF8F8FF);
const lavenderblush = const Color(0xFFFFF0F5);
const tan = const Color(0xFFD0CD94);
const slightGreen = const Color(0xfff0fffa);
const azure = const Color(0xFFF0FFFF);
const seaShell = const Color(0xFFFFF5EE);
const mintCream = const Color(0xFFF5FFFA);
const antiqueWhite = const Color(0xFFFAEBD7);
const teelDeer = const Color(0xFF94E8B4);
const positiveColor = const Color(0xff29c64d);
const focusedPositiveColor = const Color(0xff80e598);
const negativeColor = const Color(0xffFF4B4B);
const focusedNegativeColor = const Color(0xffff9999);
const transparent = const Color(0x00000000);
const blackDown = const Color(0xffebebeb);
const expandableBlackColor = const Color(0xff252525);
const splashBackgroundColor = const Color(0xff141414);
const whiteDown = const Color(0xfff9f9fa);
const girlGreen = const Color(0xfff0fffa);
const girlBlue = const Color(0xfff0f5ff);
const ruby = const Color(0xff8b1a1a);
const warning = const Color(0xffffe375);
const infoBlue = const Color(0xff3b5998);
const suspicionColor = const Color(0xffb37c3d);
const brownBlueComColor = const Color(0xff00ff98);
const blueComComColor = const Color(0xff00e7ff);
const brownAnaComColor = const Color(0xff794854);
const purpleBrown = const Color(0xff79486c);
const likeIconGlow = const Color(0xff00ff98);
var dimLikeIconGlow = Color(0xff1de292);
const likeIconSplash = const Color(0xff9dffd7);
const bookmarkIconSplash = const Color(0xfff2e1a9);
const dislikeIconGlow = const Color(0xffff0067);
const dislikeIconSplash = const Color(0xffffb1d0);
const flushBarDarkColor = const Color(0xff303030);
const flushBarLightColor = const Color(0xffbdbdbd);
const appIconColor = const Color(0xff00ff87);
const systemGreen = const Color(0xff8fd158);
const colorPickerColor = const Color(0xff443a49);
const facebookColor = const Color(0xff4267B2);
const twitterColor = const Color(0xff00acee);
const cottonSkin = const Color(0xffffcbcb);
const cottonRed = const Color(0xffffa7a7);
const cottonSkyBlue = const Color(0xffc9fdff);
const cottonLightBlue = const Color(0xfff4ffff);
const cottonLightSkin = const Color(0xfffffafa);
const cottonLittlePink = const Color(0xfffff4fa);
const grandRed = const Color(0xffe2d2d2);
const grandYellow = const Color(0xffe3e2b4);
const grandGreen = const Color(0xffa2b59f);
const locoGrey = const Color(0xffedeef0);
const locoPink = const Color(0xffede1e3);
const locoBlue = const Color(0xffd1dfe8);
const locoDark = const Color(0xff909fa6);
const makeupWhite = const Color(0xffefeff1);
const makeupRed = const Color(0xffecd4d4);
const makeupBlue = const Color(0xffccdbe2);
const makeupPurple = const Color(0xffc9cbe0);
const littleGrey = const Color(0xfff0f0f0);


Map<int, Color> blackColor =
{
  50: Color(0xFF888888),
  100: Color(0xFF707070),
  200: Color(0xFF585858),
  300: Color(0xFF404040),
  400: Color(0xFF282828),
  500: Color(0xFF111111),
  600: Color(0xFF0f0f0f),
  700: Color(0xFF0d0d0d),
  800: Color(0xFF0b0b0b),
  900: Color(0xFF0a0a0a),
};

MaterialColor CustomBlack = MaterialColor(0xFF111111, blackColor);
const blackThemeFABColor = const Color(0xff383838);


Map<int, Color> whiteColor =
{
  50: Color(0xFFe1e1e1),
  100: Color(0xFFe5e5e5),
  200: Color(0xFFe7e7e7),
  300: Color(0xFFeaeaea),
  400: Color(0xFFededed),
  500: Color(0xFFeeeeee),
  600: Color(0xFFf5f5f5),
  700: Color(0xFFf7f7f7),
  800: Color(0xFFf9f9f9),
  900: Color(0xFFffffff),
};
const whiteThemeFABColor = const Color(0xffe0e0e0);
MaterialColor CustomWhite = MaterialColor(0xFFf2f2f2, whiteColor);

Map<int, Color> greenBlue =
{
  50: Color(0xFF93decd),
  100: Color(0xFF7ed8c3),
  200: Color(0xFF68d1b9),
  300: Color(0xFF52cbaf),
  400: Color(0xFF3dc4a5),
  500: Color(0xFF28be9b),
  600: Color(0xFF24ab8b),
  700: Color(0xFF20987c),
  800: Color(0xFF1c856c),
  900: Color(0xFF18725d),
};
MaterialColor GreenBlue = MaterialColor(0xFF28be9b, greenBlue);

Map<int, Color> fluroGreen =
{
  50: Color(0xFF62ffb5),
  100: Color(0xFF4effac),
  200: Color(0xFF3bffa3),
  300: Color(0xFF27ff99),
  400: Color(0xFF14ff90),
  500: Color(0xFF00ff87),
  600: Color(0xFF00ff87),
  700: Color(0xFF00eb7d),
  800: Color(0xFF00db72),
  900: Color(0xFF00c468),
};
MaterialColor FluroGreen = MaterialColor(0xFF00ff87b, fluroGreen);