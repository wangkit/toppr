import 'package:toppr/resources/values/data_key.dart';

enum ImageType {post, reply, profile}

enum PostType {discover, history, like, following, profile}

extension PostTypeExtension on PostType {

  static const names = {
    PostType.discover: DataKey.discover,
    PostType.history: DataKey.history,
    PostType.like: DataKey.like,
    PostType.following: DataKey.following,
    PostType.profile: DataKey.profile,
  };

  String get name => names[this];
}