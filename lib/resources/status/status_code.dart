class StatusCode {
  static const ok = 200;
  static const exception = 405;
  static const wrongToken = 409;
}