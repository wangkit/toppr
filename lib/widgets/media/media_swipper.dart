import 'dart:async';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/init_dev.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/widgets/media/image/image_holder.dart';
import 'image/video_url_thumbnail.dart';

class MediaSwipper extends StatefulWidget {
  final Post post;
  final bool shouldAutoPlay;

  MediaSwipper({
    Key key,
    @required this.post,
    this.shouldAutoPlay = false,
  }) : super(key: key);

  @override
  MediaSwipperState createState() => MediaSwipperState();
}

class MediaSwipperState extends State<MediaSwipper> with AutomaticKeepAliveClientMixin<MediaSwipper> {

  SwiperController _swipeController;
  int _realMediaLength;

  @override
  void initState() {
    _swipeController = SwiperController();
    _realMediaLength = widget.post.images.length + (widget.post.video.isNullOrEmpty ? 0 : 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Center(
      child: Container(
        width: deviceWidth,
        child: Swiper(
          key: UniqueKey(),
          itemCount: _realMediaLength == 0 ? 1 : _realMediaLength,
          autoplay: _realMediaLength > 0 && widget.shouldAutoPlay,
          autoplayDisableOnInteraction: true,
          /// if number of images are smaller than or equal to 1, then no need scroll
          physics: _realMediaLength <= 1 ? NeverScrollableScrollPhysics() : null,
          autoplayDelay: 10000,
          pagination: SwiperPagination(
            builder: DotSwiperPaginationBuilder(
              activeColor: utils.getVisibleColor(widget.post.profileColor),
            )
          ),
          controller: _swipeController,
          itemWidth: deviceWidth,
          itemBuilder: (ctx, newIndex) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 0),
              child: ClipRRect(
                key: UniqueKey(),
                borderRadius: BorderRadius.circular(imageRadius),
                child: !widget.post.video.isNullOrEmpty && newIndex == 0 ? VideoUrlThumbnail(
                  thumbnail: widget.post.videoThumbnail,
                  videoUrl: widget.post.video,
                ) : ImageHolder(
                  key: UniqueKey(),
                  index: widget.post.video.isNullOrEmpty ? newIndex : newIndex - 1,
                  width: deviceWidth,
                  name: widget.post.profileUsername,
                  userColorCode: widget.post.profileColor,
                  imageList: widget.post.images.isEmpty && widget.post.video.isNullOrEmpty ? [placeholderLink] : widget.post.images,
                  heroKey: UniqueKey().toString(),
                  nextPage: () {
                    if (_realMediaLength > 1) {
                      _swipeController.next();
                    }
                  },
                  previousPage: () {
                    if (_realMediaLength > 1) {
                      _swipeController.previous();
                    }
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
