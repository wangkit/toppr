import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';
import 'package:validators/validators.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

/// Video player does not work on IOS SIMULATOR
class VideoViewer extends StatefulWidget {
  final String videoUrl;
  final String videoHeroKey;
  final bool showAppBar;
  final File videoFile;

  VideoViewer({Key key, this.showAppBar = true, this.videoUrl, this.videoHeroKey, this.videoFile,}) : super(key: key);

  @override
  _VideoViewerState createState() => _VideoViewerState();
}

class _VideoViewerState extends State<VideoViewer> with AutomaticKeepAliveClientMixin{

  get wantKeepAlive => true;

  String videoUrl;
  bool showAppBar;
  String videoHeroKey;
  VideoPlayerController _controller;
  ChewieController _chewieController;
  bool isUrl;
  WebViewController _webViewController;
  bool useWebView;
  double aspectRatio;
  File videoFile;

  @override
  void dispose() {
    _controller?.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  void setWebView() {
    useWebView = true;
    refreshThisPage();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    useWebView = false;
    videoUrl = widget.videoUrl;
    videoFile = widget.videoFile;
    showAppBar = widget.showAppBar;
    videoHeroKey = widget.videoHeroKey;
    isUrl = videoUrl.isUrl;
    super.initState();
    if (!useWebView) {
      /// Handle file uri in case the video has already been downloaded
      _controller = isUrl ? VideoPlayerController.network(videoUrl) : VideoPlayerController.file(videoFile);
      _controller..setVolume(0.0);
      _controller.addListener(() {
        if (_controller.value.hasError) {
          setWebView();
        }
      });
      _controller..initialize().then((_) {
        aspectRatio = _controller.value.aspectRatio;
        _chewieController = ChewieController(
          aspectRatio: aspectRatio,
          videoPlayerController: _controller,
          autoPlay: true, // if set to false will error
          looping: false,
        );
        refreshThisPage();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      decoration: BoxDecoration(
        color: appBgColor,
      ),
      child: Scaffold(
        appBar: showAppBar ? MyAppBar(title: "") : null,
        backgroundColor: transparent,
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: BottomAppBar(
            color: appBgColor,
            child: Row(
              children: <Widget>[

              ],
            ),
          ),
        ),
        /// Force ios to use chewie because ios does not have video play error
        body: useWebView && Platform.isAndroid ? WebView(
          onWebViewCreated: (controller) {
            _webViewController = controller;
            /// Seems we do not have to dispose the video controller here
            /// because this video viewer is not built to display multiple videos at the same time, which could lead to an exception
            /// Meaning the controllers will be disposed eventually before another video is displayed
          },
          initialUrl: widget.videoUrl,
        ) : _controller.value.initialized ? VisibilityDetector(
          key: UniqueKey(),
          onVisibilityChanged: (VisibilityInfo info) {
            if (info.visibleFraction > 0.7) {
              _chewieController.play();
            } else if (info.visibleFraction <= 0.0) {
              setWebView();
            } else {
              _chewieController.pause();
            }
          },
          child: Hero(
            tag: videoHeroKey,
            child: Container(
              width: deviceWidth,
              height: deviceHeight,
              child: Chewie(
                controller: _chewieController,
              ),
            ),
          ),
        ) : LoadingMediaWidget(),
      ),
    );
  }
}