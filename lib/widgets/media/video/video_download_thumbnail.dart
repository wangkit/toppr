import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';
import 'package:toppr/widgets/media/image/video_url_thumbnail.dart';
import 'package:toppr/widgets/media/video/video_viewer.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

/// Video player does not work on IOS SIMULATOR
/// THIS IS USED WHEN WE DO NOT HAVE THE URL OF THE VIDEO THUMBNAIL
class VideoDownloadThumbnail extends StatefulWidget {
  final String videoUrl;
  final File videoFile;

  VideoDownloadThumbnail({
    Key key,
    this.videoUrl,
    this.videoFile,
  }) : super(key: key);

  @override
  _VideoDownloadThumbnailState createState() => _VideoDownloadThumbnailState();
}

class _VideoDownloadThumbnailState extends State<VideoDownloadThumbnail> with AutomaticKeepAliveClientMixin {

  get wantKeepAlive => true;
  bool isUrl;
  String videoUrl;
  VideoPlayerController _controller;
  double aspectRatio;
  bool usePlaceholder;
  Uint8List thumbnail;
  File videoFile;
  File thumbnailFile;

  Future<Uint8List> getThumbnail() async {
    if (isUrl) {
      thumbnail = await VideoThumbnail.thumbnailData(
        video: videoUrl,
        imageFormat: ImageFormat.JPEG,
        quality: 80,
      );
    } else {
      thumbnail = await VideoThumbnail.thumbnailData(
        video: videoFile.path,
        imageFormat: ImageFormat.JPEG,
        quality: 80,
      );
    }
    await _controller.initialize();
    thumbnailFile = await utils.uintToFile(thumbnail);
    aspectRatio = _controller.value.aspectRatio;
    return thumbnail;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void setPlaceholder() {
    usePlaceholder = true;
    refreshThisPage();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    /// Video player does not work on IOS SIMULATOR
    videoUrl = widget.videoUrl;
    videoFile = widget.videoFile;
    isUrl = videoUrl.isUrl;
    _controller = isUrl ? VideoPlayerController.network(videoUrl) : VideoPlayerController.file(videoFile);
    usePlaceholder = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      future: getThumbnail(),
      builder: (ctx, snapshot) {
        return ConnectionState.done != snapshot.connectionState ? LoadingMediaWidget() : InkWell(
          onTap: () {
            getRoute.navigateTo(VideoViewer(
              showAppBar: false,
              videoUrl: videoUrl,
              videoFile: videoFile,
              videoHeroKey: UniqueKey().toString(),
            ));
          },
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: deviceWidth,
                child: Image.memory(
                  thumbnail,
                  fit: BoxFit.cover,
                  cacheHeight: (deviceWidth * 2).toInt(),
                  cacheWidth: (deviceWidth * 2).toInt(),
                ),
              ),
              Image.asset(
                "assets/icon/play_video.png",
                cacheHeight: (deviceWidth).toInt(),
                cacheWidth: (deviceWidth).toInt(),
              ),
            ],
          ),
        );
      },
    );
  }
}