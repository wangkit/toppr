import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/image_viewer.dart';

/// Holds an image and acts as an access point to image viewer
class ImageHolder extends StatefulWidget {
  final String heroKey;
  final bool disableOnTap;
  final double width;
  final double height;
  final List<String> imageList;
  final FilterQuality quality;
  final String userColorCode;
  final int index;
  final Color color;
  final String name;
  final Function nextPage;
  final Function previousPage;
  final Function onPressed;
  final bool showLoading;

  ImageHolder({
    Key key,
    @required this.index,
    this.userColorCode,
    this.color,
    this.width,
    this.showLoading = true,
    this.name = "",
    this.nextPage,
    this.previousPage,
    this.onPressed,
    @required this.imageList,
    this.quality = FilterQuality.low,
    this.height,
    this.disableOnTap = false,
    @required this.heroKey,
  }) : super(key: key);
  @override
  _ImageHolderState createState() => _ImageHolderState();
}

class _ImageHolderState extends State<ImageHolder> with AutomaticKeepAliveClientMixin<ImageHolder> {

  get wantKeepAlive => true;
  double width;
  double height;
  String heroKey;
  String userColorCode;
  Color color;
  bool isImageLoaded;
  bool disableOnTap;
  FilterQuality quality;
  List<String> imageList;
  int index;
  String name;
  bool isError;

  @override
  void initState() {
    isError = false;
    name = widget.name;
    color = widget.color;
    index = widget.index;
    imageList = widget.imageList;
    userColorCode = widget.userColorCode;
    quality = widget.quality;
    isImageLoaded = false;
    heroKey = widget.heroKey;
    disableOnTap = widget.disableOnTap;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _navigateToViewer() {
    if (!isError) {
      getRoute.navigateTo(ImageViewer(
        imageUrls: imageList,
        heroKey: heroKey,
        color: userColorCode,
        text: name,
        initialIndex: index,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: color ?? null,
      width: width,
      child: GestureDetector(
        onDoubleTap: disableOnTap ? null : () => _navigateToViewer(),
        onLongPress: disableOnTap ? null : () => _navigateToViewer(),
        onTap: widget.onPressed == null ? null : () {
          if (!isError) {
            widget.onPressed();
          }
        },
        onTapUp: widget.onPressed != null ? null : disableOnTap ? null : (TapUpDetails details) {
          if (details.globalPosition.dx > (deviceWidth / 2)) {
            /// Right half
            widget.nextPage();
          } else {
            /// Left half
            widget.previousPage();
          }
        },
        child: Hero(
          tag: heroKey,
          child: CachedNetworkImage(
            filterQuality: quality,
            fit: BoxFit.cover,
            imageUrl: imageList.length >= index + 1 ? imageList[index] : placeholderLink,
            errorWidget: (context, url, error) {
              isError = true;
              return Icon(Icons.error, color: errorColor);
            },
            placeholder: (BuildContext context, String url) {
              return widget.showLoading ? Container(
                width: width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    LoadingMediaWidget(),
                  ],
                ),
              ) : Container();
            },
          ),
        ),
      ),
    );
  }
}