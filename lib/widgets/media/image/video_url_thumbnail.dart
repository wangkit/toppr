import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';
import 'package:toppr/widgets/media/video/video_viewer.dart';

/// Holds an image and acts as an access point to image viewer
/// THIS IS USED WHEN WE ALREADY HAVE THE THUMBNAIL URL OF THIS VIDEO
class VideoUrlThumbnail extends StatefulWidget {
  final String videoUrl;
  final String thumbnail;

  VideoUrlThumbnail({
    Key key,
    @required this.videoUrl,
    @required this.thumbnail,
  }) : super(key: key);
  @override
  _VideoUrlThumbnailState createState() => _VideoUrlThumbnailState();
}

class _VideoUrlThumbnailState extends State<VideoUrlThumbnail> with AutomaticKeepAliveClientMixin<VideoUrlThumbnail> {

  get wantKeepAlive => true;
  String videoUrl;
  String thumbnail;

  @override
  void initState() {
    videoUrl = widget.videoUrl;
    thumbnail = widget.thumbnail;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () {
        getRoute.navigateTo(VideoViewer(
          showAppBar: false,
          videoUrl: videoUrl,
          videoHeroKey: UniqueKey().toString(),
        ));
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: deviceWidth,
            child: CachedNetworkImage(
              imageUrl: thumbnail,
              fit: BoxFit.cover,
            ),
          ),
          Image.asset(
            "assets/icon/play_video.png",
            cacheHeight: (deviceWidth).toInt(),
            cacheWidth: (deviceWidth).toInt(),
          ),
        ],
      ),
    );
  }
}