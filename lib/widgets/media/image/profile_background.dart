import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/widgets/media/image/image_viewer.dart';

class ProfileBackground extends StatefulWidget {

  final UserProfile profile;
  final File image;
  final bool disableOnPress;

  ProfileBackground({
    Key key,
    @required this.profile,
    this.image,
    this.disableOnPress = false,
  }) : super(key: key);

  @override
  ProfileBackgroundState createState() => ProfileBackgroundState();
}

class ProfileBackgroundState extends State<ProfileBackground> {

  @override
  void initState() {
    super.initState();
  }

  String _hero = UniqueKey().toString();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: deviceWidth,
      height: deviceHeight * 0.25,
      child: GestureDetector(
        onTap: widget.disableOnPress ? null : () {
          getRoute.navigateTo(ImageViewer(imageUrls: [widget.profile.backgroundImage], text: widget.profile.username, color: widget.profile.color, heroKey: _hero));
        },
        child: Hero(
          tag: _hero,
          child: widget.image != null ? Image.file(
            widget.image,
            fit: BoxFit.cover,
          ) : CachedNetworkImage(
            imageUrl: widget.profile.backgroundImage,
            fit: BoxFit.cover,
          )
        ),
      ),
    );
  }
}
