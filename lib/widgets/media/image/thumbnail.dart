import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/widgets/media/image/image_viewer.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

class Thumbnail extends StatefulWidget {
  final String imageUrl;
  final File imageFile;
  final double width;
  final double height;
  final double radius;
  final BlendMode blendMode;
  final Color color;
  final String text;
  final bool showBorder;
  final List<String> imageUrls;
  final Function onPressed;
  final bool disableOnTap;

  Thumbnail({
    Key key,
    @required this.imageUrl,
    @required this.text,
    this.imageUrls,
    this.onPressed,
    this.imageFile,
    this.width = 70.0,
    this.height = 70.0,
    this.radius = 8.0,
    this.showBorder = false,
    this.disableOnTap = false,
    this.blendMode,
    @required this.color,
  }) : super(key: key);

  @override
  ThumbnailState createState() => ThumbnailState();
}

class ThumbnailState extends State<Thumbnail> {

  String _hero;

  @override
  void initState() {
    _hero = UniqueKey().toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.disableOnTap ? null : widget.onPressed != null ? widget.onPressed : widget.imageUrl.isNullOrEmpty ? null : () {
        getRoute.navigateTo(ImageViewer(
          text: widget.text,
          imageUrls: widget.imageUrls != null ? widget.imageUrls : [widget.imageUrl],
          color: utils.getHexCodeByColor(widget.color),
          heroKey: _hero,
        ));
      },
      child: Hero(
        tag: _hero,
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius),
            border: widget.showBorder ? Border.all(
              color: utils.getColorByHex(myProfile.color),
            ) : null,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: widget.imageFile == null ? CachedNetworkImageProvider(
                widget.imageUrl ?? "",
              ) : FileImage(
                widget.imageFile,
              ),
            ),
          ),
        ),
      ),
    );
  }
}