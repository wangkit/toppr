import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/loading/loading_media_widget.dart';

/// User can rotate , resize, download, url image here
class ImageViewer extends StatefulWidget {
  final List<String> imageUrls;
  final String heroKey;
  final int initialIndex;
  final String text;
  final String color;

  ImageViewer({
    Key key,
    @required this.imageUrls,
    @required this.text,
    @required this.color,
    this.initialIndex = 0,
    @required this.heroKey,
  }) : super(key: key);

  @override
  _ImageViewerState createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {

  PhotoViewScaleStateController scaleStateController;
  PageController _pageController;
  String heroKey;
  int initialIndex;
  int currentIndex;
  bool _showTopBottomBars;
  String _text;
  String _color;
  double _opacity;

  @override
  void initState() {
    super.initState();
    _color = widget.color;
    initialIndex = widget.initialIndex;
    currentIndex = initialIndex;
    _text = widget.text;
    heroKey = widget.heroKey;
    _pageController = PageController(initialPage: initialIndex);
    _showTopBottomBars = true;
    if (_showTopBottomBars) {
      _opacity = 1.0;
    } else {
      _opacity = 0.0;
    }
    scaleStateController = PhotoViewScaleStateController();
  }

  void showHideBars() {
    _showTopBottomBars = !_showTopBottomBars;
    if (_showTopBottomBars) {
      _opacity = 1.0;
    } else {
      _opacity = 0.0;
    }
    refreshThisPage();
  }

  Widget imageLoadWidget(ImageChunkEvent loadingProgress) {
    return Center(
      child: Container(
        width: 40,
        height: 40,
        child:  CircularProgressIndicator(
          backgroundColor: mainColor,
          value: loadingProgress?.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes : null,
        ),
      ),
    );
  }

  @override
  void dispose() {
    scaleStateController.dispose();
    _pageController.dispose();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void goBack(){
    scaleStateController.scaleState = PhotoViewScaleState.initial;
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () => showHideBars(),
      child: Scaffold(
        backgroundColor: transparent,
        appBar: MyAppBar(
          title: "",
          actions: widget.imageUrls.length > 1 ? [
            IconButton(
              onPressed: () {
                _pageController.animateToPage(_pageController.page.floor() - 1, duration: kThemeAnimationDuration, curve: Curves.fastOutSlowIn);
              },
              icon: Icon(
                  Icons.navigate_before,
                  color: mainColor
              ),
            ),
            IconButton(
              onPressed: () {
                _pageController.nextPage(duration: kThemeAnimationDuration, curve: Curves.fastOutSlowIn);
              },
              icon: Icon(
                Icons.navigate_next,
                color: mainColor
              ),
            ),
          ] : [],
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: mainColor,
            ),
          ),
        ),
        body: Container(
          width: deviceWidth,
          height: deviceHeight,
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              PhotoViewGallery.builder(
                itemCount: widget.imageUrls.length,
                enableRotation: false,
                pageController: _pageController,
                onPageChanged: (int newIndex) {
                  currentIndex = newIndex;
                  refreshThisPage();
                },
                builder: (ctx, index) {
                  return PhotoViewGalleryPageOptions(
                    heroAttributes: widget.imageUrls.length == 1 ? PhotoViewHeroAttributes(tag: heroKey) : null,
                    scaleStateController: scaleStateController,
                    imageProvider: CachedNetworkImageProvider(widget.imageUrls[index]),
                  );
                },
                backgroundDecoration: BoxDecoration(
                  color: appBgColor
                ),
                loadFailedChild: LoadingMediaWidget(),
                loadingBuilder: (context, loadingProgress) {
                  return imageLoadWidget(loadingProgress);
                },
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 100),
                  opacity: _opacity,
                  child: SafeArea(
                    minimum: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: Container(
                      color: Colors.grey.withOpacity(0.1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 12.0, top: 8.0, bottom: 8.0),
                                  child: Text(
                                    _text,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: commonTextStyle.merge(
                                        TextStyle(
                                          color: utils.getColorByHex(_color),
                                        )
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Text(
                                "${currentIndex + 1}/${widget.imageUrls.length}",
                                style: commonTextStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}