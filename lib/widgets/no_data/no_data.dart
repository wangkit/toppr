import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';

class NoData extends StatefulWidget {
  final String text;

  NoData({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  _NoDataState createState() => _NoDataState();
}

class _NoDataState extends State<NoData> {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(24.0),
            child: Text(
              widget.text,
              style: commonTextStyle,
            ),
          ),
        ],
      ),
    );
  }
}
