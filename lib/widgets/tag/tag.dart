
import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

class Tag extends StatefulWidget {
  final String text;
  final String colorCode;
  final Function onPressed;
  final Icon icon;
  final Icon leadingIcon;
  final double fontSize;
  final Color textColor;
  final double margin;
  final BubbleEdges padding;
  final double elevation;
  final double radius;
  final Widget child;

  Tag({
    Key key,
    this.text,
    this.colorCode,
    this.onPressed,
    this.leadingIcon,
    this.radius = 0,
    this.fontSize,
    this.child,
    this.elevation = 1,
    this.textColor,
    this.margin = 0,
    this.padding = const BubbleEdges.symmetric(vertical: 4, horizontal: 8),
    this.icon,
  }) : super(key: key);

  @override
  TagState createState() => TagState();
}

class TagState extends State<Tag> {

  String text;
  String colorCode;
  double fontSize;
  double margin;
  double radius;
  double elevation;
  Icon icon;
  Icon leadingIcon;

  @override
  void initState() {
    leadingIcon = widget.leadingIcon;
    radius = widget.radius;
    elevation = widget.elevation;
    margin = widget.margin;
    fontSize = widget.fontSize;
    text = widget.text;
    icon = widget.icon;
    colorCode = widget.colorCode;
    if (radius == 0) radius = commonRadius;
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Bubble(
        margin: BubbleEdges.all(margin),
        padding: widget.padding,
        radius: Radius.circular(radius),
        elevation: elevation,
        color: widget.colorCode != null ? utils.getColorByHex(widget.colorCode) : appIconColor,
        child: widget.child != null ? widget.child : Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            leadingIcon == null ? Container(
              width: 0,
              height: 0,
            ) : Padding(
              padding: EdgeInsets.only(right: 2.0,),
              child: leadingIcon,
            ),
            text != null ? Text(
              text,
              style: fontSize != null || widget.textColor != null ? commonTextStyle.merge(
                  TextStyle(
                    fontSize: fontSize,
                    color: widget.textColor,
                  )
              ) : commonTextStyle,
            ) : Container(),
            icon == null ? Container(
              width: 0,
              height: 0,
            ) : Padding(
              padding: EdgeInsets.only(left: 2.0),
              child: icon,
            ),
          ],
        ),
      ),
    );
  }
}