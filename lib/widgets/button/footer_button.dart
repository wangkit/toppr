
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';

class FooterButton extends StatefulWidget {

  final String title;
  final Function onPressed;

  FooterButton({
    this.title,
    this.onPressed,
    Key key,
  }) : super(key: key);

  @override
  FooterButtonState createState() => FooterButtonState();
}

class FooterButtonState extends State<FooterButton> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onPressed(),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          widget.title,
          style: smallTextStyle,
        ),
      ),
    );
  }
}