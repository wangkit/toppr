import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'custom_icon_button.dart';

class BottomSheetButton extends StatefulWidget {
  final Function onTap;
  final String label;
  final IconData iconData;
  final Widget leading;
  final TextStyle labelStyle;

  BottomSheetButton({
    Key key, 
    @required this.onTap,
    @required this.label,
    this.labelStyle,
    this.iconData,
    this.leading,
  }) : assert(iconData == null ? leading != null : leading == null), super(key: key);

  @override
  BottomSheetButtonState createState() => BottomSheetButtonState();
}

class BottomSheetButtonState extends State<BottomSheetButton> {
  
  String label;
  IconData iconData;
  Widget leading;
  TextStyle labelStyle;
  
  @override
  void initState() {
    label = widget.label;
    labelStyle = widget.labelStyle;
    iconData = widget.iconData;
    leading = widget.leading;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Flexible(
      child: ListTile(
        onTap: () {
          getRoute.pop();
          widget.onTap();
        },
        leading: iconData != null ? CustomIconButton(
          tooltip: label,
          minWidth: 36.0,
          minHeight: 24.0,
          iconSize: 20.0,
          icon: Icon(iconData, color: mainColor,),
          onPressed: null,
        ) : leading,
        title: Text(
          label,
          overflow: TextOverflow.ellipsis,
          style: labelStyle ?? TextStyle(color: mainColor),),
      ),
    );
  }
}