
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';

class GradientIconButton extends StatefulWidget {

  final double size;
  final Icon icon;
  final Gradient gradient;
  final Function onPressed;

  GradientIconButton({
    @required this.gradient,
    @required this.onPressed,
    this.size = 24,
    this.icon,
    Key key,
  }) : super(key: key);

  @override
  GradientIconButtonState createState() => GradientIconButtonState();
}

class GradientIconButtonState extends State<GradientIconButton> {

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      child: IconButton(
        onPressed: widget.onPressed,
        icon: widget.icon,
      ),
      shaderCallback: (Rect bounds) {
        final Rect rect = Rect.fromLTRB(0, 0, widget.size, widget.size);
        return widget.gradient.createShader(rect);
      },
    );
  }
}