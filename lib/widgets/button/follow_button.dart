import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/text.dart';

import 'bordered_flat_button.dart';

class FollowButton extends StatefulWidget {

  final UserProfile profile;
  final double width;
  final Function onChange;

  FollowButton({
    Key key,
    @required this.profile,
    this.width,
    this.onChange,
  }) : super(key: key);

  @override
  _FollowButtonState createState() => _FollowButtonState();
}

class _FollowButtonState extends State<FollowButton> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      child: BorderedFlatButton(
        width: widget.width != null ? widget.width : deviceWidth,
        height: 45,
        edgeInsets: null,
        text: widget.profile.amIFollowing ? TextData.unfollow : TextData.follow,
        textColor: utils.getCorrectContrastColor(widget.profile.amIFollowing ? utils.getHexCodeByColor(appIconColor) : widget.profile.color),
        flatButtonPadding: EdgeInsets.all(0.0),
        bgColor: widget.profile.amIFollowing ? appBgColor : utils.getColorByHex(widget.profile.color),
        borderColorCode: widget.profile.color,
        onPressed: () {
          apiManager.follow(widget.profile.id, widget.profile.amIFollowing);
          widget.profile.amIFollowing = !widget.profile.amIFollowing;
          if (widget.onChange != null) {
            widget.onChange(widget.profile.amIFollowing);
          }
          refreshThisPage();
        },
      ),
    );
  }
}


