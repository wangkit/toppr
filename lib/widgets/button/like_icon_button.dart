import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/models/models.dart';

class LikeIconRow extends StatefulWidget {

  final Post post;

  LikeIconRow({
    Key key,
    @required this.post,
  }) : super(key: key);

  @override
  _LikeIconRowState createState() => _LikeIconRowState();
}

class _LikeIconRowState extends State<LikeIconRow> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          onPressed: () {
            apiManager.likePost(widget.post.postId);
            widget.post.liked = !widget.post.liked;
            refreshThisPage();
          },
          splashRadius: kToolbarHeight / 3,
          icon: Icon(
            widget.post.liked ? MdiIcons.heart : MdiIcons.heartOutline,
            color: utils.getColorByHex(widget.post.profileColor),
          ),
        ),
        Text(
          utils.shortStringForLongInt(widget.post.likeCount + (widget.post.liked ? 1 : 0)),
          style: commonTextStyle,
        ),
      ],
    );
  }
}


