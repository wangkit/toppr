
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/view/splash_factory/custom_splash_factory.dart';

import 'custom_bouncing_widget.dart';
class LongElevatedButton extends StatefulWidget {
  final bool isReverseColor;
  final Function onPressed;
  final String title;
  final double height;
  final double elevation;
  final double width;
  final Color textColor;
  final Color color;
  final double borderRadius;
  final Color splashColor;
  final Widget child;
  final TextStyle textStyle;
  final EdgeInsets padding;
  final bool isEnabled;
  final Widget icon;
  final bool isBackgroundDarkColor;

  LongElevatedButton({
    Key key,
    this.padding,
    this.textStyle,
    this.icon,
    this.child,
    this.isEnabled = true,
    this.splashColor,
    this.borderRadius,
    this.textColor,
    this.height,
    this.color,
    this.isBackgroundDarkColor,
    this.width,
    this.elevation,
    this.isReverseColor = false,
    @required this.onPressed,
    this.title,
  }) : super(key: key);

  @override
  LongElevatedButtonState createState() => LongElevatedButtonState();
}

class LongElevatedButtonState extends State<LongElevatedButton> {

  bool isReverseColor;
  Function onPressed;
  String title;
  double height;
  double elevation;
  double width;
  Color textColor;
  double borderRadius;
  Color splashColor;
  Color color;
  TextStyle textStyle;
  BorderRadius radius = BorderRadius.circular(36.0);
  EdgeInsets edgeInsets;
  Color bgColor;
  bool disabled;
  bool isBackgroundDarkColor;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }
  
  void disableButton() {
    disabled = true;
    refreshThisPage();
  }

  void enableButton() {
    disabled = false;
    refreshThisPage();
  }

  @override
  void initState() {
    isBackgroundDarkColor = widget.isBackgroundDarkColor;
    disabled = !widget.isEnabled;
    edgeInsets = widget.padding;
    textStyle = widget.textStyle;
    splashColor = widget.splashColor;
    borderRadius = widget.borderRadius;
    if (borderRadius != null) {
      radius = BorderRadius.circular(borderRadius);
    }
    textColor = widget.textColor;
    height = widget.height;
    color = widget.color;
    width = widget.width;
    isReverseColor = widget.isReverseColor;
    onPressed = widget.onPressed;
    title = widget.title;
    elevation = widget.elevation;
    bgColor = appIconColor;
    bgColor = isReverseColor ? utils.getVisibleColor(utils.getHexCodeByColor(appTheme.withOpacity(0.2))) : color != null ? color : appIconColor;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: disabled,
      child: Padding(
        padding: edgeInsets ?? EdgeInsets.all(0.0),
        child: Container(
          width: width != null ? width : deviceWidth * 0.9,
          height: height != null ? height : 50,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: radius,
            border: Border.all(
              color: color ?? appIconColor,
            ),
          ),
          child: Material(
            elevation: elevation ?? 0.0,
            color: Colors.transparent,
            child: InkWell(
              borderRadius: radius,
              onTap: onPressed,
              splashFactory: CustomSplashFactory(),
              splashColor: bgColor?.withOpacity(0.5),
              child: widget.child != null ? widget.child : Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.icon ?? Container(),
                    Text(
                      title,
                      style: textStyle != null ? textStyle : longButtonTextStyle.merge(
                        TextStyle(
                          color: textColor == null ? mainColor : textColor,
                        )
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}