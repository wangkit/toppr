import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'long_elevated_button.dart';

class LoadingLongElevatedButton extends StatefulWidget {
  final bool isReverseColor;
  final Function onPressed;
  final String title;
  final double height;
  final double elevation;
  final double width;
  final Color textColor;
  final Color color;
  final double borderRadius;
  final Widget child;
  final TextStyle textStyle;
  final EdgeInsets padding;
  final bool isEnabled;
  final Widget icon;
  final bool isBackgroundDarkColor;

  LoadingLongElevatedButton({
    Key key,
    this.padding,
    this.textStyle,
    this.icon,
    this.child,
    this.isEnabled = true,
    this.borderRadius,
    this.textColor,
    this.height,
    this.color,
    this.isBackgroundDarkColor,
    this.width,
    this.elevation = 0,
    this.isReverseColor = false,
    @required this.onPressed,
    this.title,
  }) : super(key: key);

  @override
  LoadingLongElevatedButtonState createState() => LoadingLongElevatedButtonState();
}

class LoadingLongElevatedButtonState extends State<LoadingLongElevatedButton> {

  bool isReverseColor;
  Function onPressed;
  String title;
  double height;
  double elevation;
  double width;
  Color textColor;
  double borderRadius;
  Color color;
  TextStyle textStyle;
  EdgeInsets edgeInsets;
  Color bgColor;
  bool disabled;
  bool isBackgroundDarkColor;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void disableButton() {
    disabled = true;
    refreshThisPage();
  }

  void enableButton() {
    disabled = false;
    refreshThisPage();
  }

  @override
  void initState() {
    isBackgroundDarkColor = widget.isBackgroundDarkColor;
    disabled = !widget.isEnabled;
    edgeInsets = widget.padding;
    textStyle = widget.textStyle;
    borderRadius = widget.borderRadius;
    textColor = widget.textColor;
    height = widget.height;
    color = widget.color;
    width = widget.width;
    isReverseColor = widget.isReverseColor;
    onPressed = widget.onPressed;
    title = widget.title;
    elevation = widget.elevation;
    bgColor = appIconColor;
    bgColor = isReverseColor ? utils.getVisibleColor(utils.getHexCodeByColor(appTheme.withOpacity(0.2))) : color != null ? color : appIconColor;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ArgonButton(
      height: height != null ? height: 50,
      width: width != null ? width : deviceWidth * 0.9,
      borderRadius: widget.borderRadius != null ? widget.borderRadius : 36,
      color: bgColor,
      elevation: elevation,
      highlightElevation: 0,
      roundLoadingShape: true,
      splashColor: bgColor?.withOpacity(0.5),
      borderSide: BorderSide(
        color: bgColor,
      ),
      child: Text(
        title,
        style: textStyle != null ? textStyle : longButtonTextStyle.merge(
          TextStyle(
            color: textColor == null ? mainColor : textColor,
          )
        ),
      ),
      loader: Container(
        padding: EdgeInsets.all(10),
        child: Center(
          child: SpinKitRing(
            color: defaultWhiteColor,
            lineWidth: 5,
          ),
        ),
      ),
      onTap: widget.onPressed,
    );
  }
}