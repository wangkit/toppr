//Credit : @Eugene (https://stackoverflow.com/questions/56340682/flutter-equvalent-android-toggle-switch)

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

typedef OnToggle = void Function(int index);

// ignore: must_be_immutable
class CustomToggleSwitch extends StatefulWidget {
  /// Active background color
  final Color activeBgColor;

  /// Active foreground color
  final Color activeFgColor;

  /// Inactive background color
  final Color inactiveBgColor;

  /// Inactive foreground color
  final Color inactiveFgColor;

  /// List of labels
  final int length;

  /// List of icons
  final List<IconData> icons;

  /// List of active foreground colors
  final List<Color> activeBgColors;

  final List<Widget> children;

  /// Minimum switch width
  final double minWidth;

  /// Minimum switch height
  final double minHeight;

  /// Widget's corner radius
  final double cornerRadius;

  /// Font size
  final double fontSize;

  /// Icon size
  final double iconSize;

  /// OnToggle function
  final OnToggle onToggle;

  // Change selection on tap
  final bool changeOnTap;

  /// Initial label index
  int initialLabelIndex;

  CustomToggleSwitch({
    Key key,
    @required this.length,
    this.activeBgColor,
    this.activeFgColor,
    this.inactiveBgColor,
    this.inactiveFgColor,
    this.children,
    this.onToggle,
    this.cornerRadius = 8.0,
    this.initialLabelIndex = 0,
    this.minWidth = 72.0,
    this.minHeight = 40.0,
    this.changeOnTap = true,
    this.icons,
    this.activeBgColors,
    this.fontSize = 14.0,
    this.iconSize = 17.0,
  }) : super(key: key);

  @override
  _CustomToggleSwitchState createState() => _CustomToggleSwitchState();
}

class _CustomToggleSwitchState extends State<CustomToggleSwitch>
    with AutomaticKeepAliveClientMixin<CustomToggleSwitch> {
  /// Active background color
  Color activeBgColor;

  /// Active foreground color
  Color activeFgColor;

  /// Inactive background color
  Color inactiveBgColor;

  /// Inctive foreground color
  Color inactiveFgColor;

  /// Maintain selection state.
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    /// Assigns active background color to default primary theme color if it's null/not provided.
    activeBgColor = widget.activeBgColor == null
        ? Theme.of(context).primaryColor
        : widget.activeBgColor;

    /// Assigns active foreground color to default accent text theme color if it's null/not provided.
    activeFgColor = widget.activeFgColor == null
        ? Theme.of(context).accentTextTheme.bodyText1.color
        : widget.activeFgColor;

    /// Assigns inactive background color to default disabled theme color if it's null/not provided.
    inactiveBgColor = widget.inactiveBgColor == null
        ? Theme.of(context).disabledColor
        : widget.inactiveBgColor;

    /// Assigns inactive foreground color to default text theme color if it's null/not provided.
    inactiveFgColor = widget.inactiveFgColor == null
        ? Theme.of(context).textTheme.bodyText1.color
        : widget.inactiveFgColor;

    double width = deviceWidth - 20;

    return ClipRRect(
      borderRadius: BorderRadius.circular(widget.cornerRadius),
      child: Container(
        width: width,
        decoration: BoxDecoration(
          color: inactiveBgColor,
          border: Border.all(
            color: unfocusedColor,
          ),
          borderRadius: BorderRadius.circular(widget.cornerRadius),
        ),
        height: widget.minHeight,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: List.generate(widget.length, (index) {
            /// Active if index matches current
            final active = index == widget.initialLabelIndex;

            /// Assigns foreground color based on active status.
            ///
            /// Sets active foreground color if current index is active.
            /// Sets inactive foreground color if current index is inactive.
            final fgColor = active ? activeFgColor : inactiveFgColor;

            /// Default background color
            var bgColor = Colors.transparent;

            /// Changes background color if current index is active.
            ///
            /// Sets same active background color for all items if active background colors list is empty.
            /// Sets different active background color for current item by matching index if active background colors list is not empty
            if (active) {
              bgColor = widget.activeBgColors == null
                  ? activeBgColor
                  : widget.activeBgColors[index];
            }

            return Flexible(
              child: GestureDetector(
                onTap: () => _handleOnTap(index),
                child: ClipRRect(
                  borderRadius: index == 0 ? BorderRadius.only(
                    topLeft: Radius.circular(widget.cornerRadius),
                    bottomLeft: Radius.circular(widget.cornerRadius),
                  ) : index == 1 ? BorderRadius.zero : BorderRadius.only(
                    topRight: Radius.circular(widget.cornerRadius),
                    bottomRight: Radius.circular(widget.cornerRadius),
                  ),
                  child: Container(
                    width: width / widget.length,
                    decoration: BoxDecoration(
                      gradient: active ? LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          GreenBlue,
                          GreenBlue,
                        ],
                      ) : null,
                      color: active ? null : bgColor,
                      border: index == widget.length - 1 ? Border.all(
                        color: appBgColor,
                        width: 0
                      ) : Border(
                        right: BorderSide(
                          color: unfocusedColor,
                        ),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: widget.children[index],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  /// Handles selection
  void _handleOnTap(int index) async {
    if (widget.changeOnTap) {
      setState(() => widget.initialLabelIndex = index);
    }
    if (widget.onToggle != null) {
      widget.onToggle(index);
    }
  }

  /// Calculates width to prevent overflow by taking screen width into account.
  double _calculateWidth(double minWidth) {
    /// Total number of labels/switches
    int totalLabels = widget.length;

    /// Extra width to prevent overflow and add padding
    double extraWidth = 0.10 * totalLabels;

    /// Max screen width
    double screenWidth = MediaQuery.of(context).size.width;

    /// Returns width per label
    ///
    /// Returns passed minWidth per label if total requested width plus extra width is less than max screen width.
    /// Returns calculated width to fit within the max screen width if total requested width plus extra width is more than max screen width.
    return (totalLabels + extraWidth) * widget.minWidth < screenWidth
        ? widget.minWidth
        : screenWidth / (totalLabels + extraWidth);
  }
}
