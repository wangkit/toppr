import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/models/firestore_tables.dart';
import 'package:toppr/resources/models/models.dart';

class LikeReplyIconRow extends StatefulWidget {

  final Post post;
  final String replyId;
  final double iconSize;
  final List<String> likedProfileIds;

  LikeReplyIconRow({
    Key key,
    @required this.post,
    @required this.replyId,
    @required this.likedProfileIds,
    this.iconSize,

  }) : super(key: key);

  @override
  _LikeReplyIconRowState createState() => _LikeReplyIconRowState();
}

class _LikeReplyIconRowState extends State<LikeReplyIconRow> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          onPressed: () async {

            /// Use array union for adding
            /// Use array remove for removing

            firestore.collection(outerRepliesDbName)
                .doc(widget.post.postId)
                .collection(innerReplyDbName)
                .doc(widget.replyId)
                .update({
              ReplyTable.likedProfileIds: widget.likedProfileIds.contains(myProfile.id) ?
              FieldValue.arrayRemove([myProfile.id]) :
              FieldValue.arrayUnion([myProfile.id]),
            });
          },
          icon: Icon(
            widget.likedProfileIds.contains(myProfile.id) ? MdiIcons.heart : MdiIcons.heartOutline,
            color: utils.getColorByHex(myProfile.color),
            size: widget.iconSize,
          ),
        ),
        Text(
          utils.shortStringForLongInt(widget.likedProfileIds.length),
          style: smallTextStyle,
        ),
      ],
    );
  }
}


