import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';

class BorderedFlatButton extends StatefulWidget {
  final String borderColorCode;
  final Function onPressed;
  final String text;
  final Color bgColor;
  final Color textColor;
  final EdgeInsets edgeInsets;
  final double radius;
  final double width;
  final double height;
  final Widget child;
  final EdgeInsets flatButtonPadding;

  BorderedFlatButton({
    Key key,
    this.textColor,
    this.flatButtonPadding = const EdgeInsets.all(8.0),
    this.radius = commonRadius,
    this.text,
    @required this.onPressed,
    @required this.borderColorCode,
    this.bgColor,
    this.edgeInsets,
    this.width,
    this.height,
    this.child,
  }) : assert(text == null ? child !=null : true), super(key: key);

  @override
  BorderedFlatButtonState createState() => BorderedFlatButtonState();
}

class BorderedFlatButtonState extends State<BorderedFlatButton> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.all(12.0),
      child: Container(
        padding: widget.flatButtonPadding,
        width: widget.width,
        height: widget.height,
        child: FlatButton(
          padding: widget.flatButtonPadding,
          color: widget.bgColor ?? null,
          onPressed: widget.onPressed,
          shape: RoundedRectangleBorder(
            side: BorderSide(
                color: utils.getColorByHex(widget.borderColorCode),
                style: BorderStyle.solid
            ),
            borderRadius: BorderRadius.circular(widget.radius),
          ),
          child: widget.child ?? Padding(
            padding: EdgeInsets.all(2.0),
            child: Text(
              widget.text,
              style: smallTextStyle.merge(
                TextStyle(
                  color: widget.textColor ?? mainColor,
                ),
              )
            ),
          ),
        ),
      ),
    );
  }
}