import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/block_dialog.dart';
import 'package:toppr/view/dialogs/report_dialog.dart';

enum Popup {block, report,}

class PopupMenu extends StatefulWidget {

  final UserProfile profile;

  PopupMenu({
    Key key,
    @required this.profile,
  }) : super(key: key);

  @override
  _PopupMenuState createState() => _PopupMenuState();
}

class _PopupMenuState extends State<PopupMenu> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return PopupMenuButton<Popup>(
      onSelected: (Popup result) {
        switch (result) {
          case Popup.block:
            if (widget.profile.id != myProfile.id) {
              getBlockDialog(widget.profile.id, widget.profile.username, utils.getChatId([myProfile.id, widget.profile.id]), null);
            } else {
              utils.toast(TextData.noBlockSelf, isWarning: true);
            }
            break;
          case Popup.report:
            getReportDialog(widget.profile.id, widget.profile.username, null);
            break;
        }
      },
      icon: Icon(
        Icons.more_vert,
        color: mainColor,
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<Popup>>[
        PopupMenuItem<Popup>(
          value: Popup.report,
          child: Text(
            TextData.report,
            style: commonTextStyle,
          ),
        ),
        PopupMenuItem<Popup>(
          value: Popup.block,
          child: Text(
            TextData.block,
            style: commonTextStyle,
          ),
        ),
      ],
    );
  }
}


