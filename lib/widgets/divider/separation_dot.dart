import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';

class SeparationDot extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.only(top: 4.0,),
      child: Icon(
        MdiIcons.circle,
        color: mainColor,
        size: 6.0,
      ),
    );
  }
}
