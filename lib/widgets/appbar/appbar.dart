import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final Widget titleWidget;
  final List<Widget> actions;
  final bool automaticallyImplyLeading;
  final bool centerTitle;
  final double elevation;
  final Color bgColor;
  final Widget leading;

  MyAppBar({
    Key key,
    this.titleWidget,
    this.title,
    this.leading,
    this.actions,
    this.automaticallyImplyLeading = true,
    this.centerTitle = true,
    this.elevation = 4.0,
    this.bgColor = cottonLightSkin,
  }) : assert(titleWidget != null ? title.isNullOrEmpty : true), super(key: key);

  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize => new Size(deviceWidth, kToolbarHeight);
}

class _MyAppBarState extends State<MyAppBar> {

  @override
  Widget build(BuildContext context) {

    return AppBar(
      title: widget.titleWidget ?? Text(
        widget.title,
        style: TextStyle(
          fontSize: 21.0,
          color: mainColor,
        ),
      ),
      backgroundColor: appIconColor,
      leading: widget.leading,
      centerTitle: widget.centerTitle,
      elevation: widget.elevation,
      actions: widget.actions,
      automaticallyImplyLeading: widget.automaticallyImplyLeading,
    );
  }
}
