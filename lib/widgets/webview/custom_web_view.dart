import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/button/custom_icon_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebView extends StatefulWidget {
  final String link;

  CustomWebView({Key key, this.link}) : super(key: key);

  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {

  WebViewController _webViewController;
  String link;

  String _getAppbarTitle() {
    var appbarTitle = widget.link.split("://").last;
    if (appbarTitle.contains("www.")) {
      appbarTitle = appbarTitle.replaceFirst("www.", "");
      if (appbarTitle.contains(".")) {
        appbarTitle = appbarTitle.split(".").first;
      }
    }
    return appbarTitle;
  }

  JavascriptMode _getJsMode() {
    if (_getAppbarTitle() == "latimes") {
      return JavascriptMode.disabled;
    } else {
      return JavascriptMode.unrestricted;
    }
  }

  @override initState() {
    link = widget.link;
    if (link.startsWith("www.")) link = "https://" + link;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
          title: link,
          actions: [
            IconButton(
              onPressed: () {
                _webViewController.reload();
              },
              icon: Icon(
                Icons.refresh,
                color: mainColor,
              ),
            ),
            IconButton(
              onPressed: () {
                Clipboard.setData(ClipboardData(text: link));
                utils.toast(TextData.copiedSuccessfully, bgColor: success, iconData: successIcon);
              },
              icon: Icon(Icons.content_copy, color: mainColor,),
            ),
            IconButton(
              onPressed: () async {
                await utils.launchURL(link);
              },
              icon: Icon(Icons.open_in_browser, color: mainColor,),
            ),
          ]
      ),
      backgroundColor: transparent,
      body: WebView(
        onWebViewCreated: (controller) => _webViewController = controller,
        initialUrl: link,
        javascriptMode: _getJsMode(),
      ),
    );
  }
}