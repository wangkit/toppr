import 'dart:async';
import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/smart_text/smart_text.dart';
import 'package:toppr/widgets/webview/custom_web_view.dart';

/// To display text in post title, reply content with the tagged user name colored and clickable
class TagNameDisplay extends StatefulWidget {
  final String text;
  final bool isBold;
  final TextStyle style;
  final int maxLines;
  final bool isOverflow;
  final bool showImage;

  TagNameDisplay({
    Key key, 
    this.isOverflow = false, 
    @required this.text, 
    this.isBold = false,
    this.showImage = false,
    this.maxLines, 
    this.style,
  }) : super(key: key);

  @override
  _TagNameDisplayState createState() => _TagNameDisplayState();
}

class _TagNameDisplayState extends State<TagNameDisplay> with AutomaticKeepAliveClientMixin<TagNameDisplay> {
  String text;
  List<String> splitForUser;
  List<String> splitForCorner;
  List<String> hashTags;
  List<String> doubleStrokes;
  List<UserProfile> _userData = [];
  Future<void> _loading;
  bool isBold;
  int maxLines;
  TextStyle style;
  bool isOverflow;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    isOverflow = widget.isOverflow;
    style = widget.style;
    isBold = widget.isBold;
    text = widget.text;
    maxLines = widget.maxLines;
    _loading = handleUserData();
    super.initState();
  }
  
  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> handleUserData() async {
    final Completer<void> completer = Completer<void>();
    splitForUser = text.split(RegExp(userTag));
    hashTags = splitForUser.getRange(1, splitForUser.length).fold([], (t, e) {
      var texts = e.split(" ");
      if (texts.length > 1) {
        return List.from(t)..addAll(["$userTag${texts.first}", "${e.substring(texts.first.length)}"]);
      }
      return List.from(t)..add("$userTag${texts.first}");
    });
    if (hashTags.isNotEmpty && _checkIfNeedCallUserData(hashTags)) {
      await apiManager.getUserProfileByUsername(hashTags).then((jsonResponse) {
        List<dynamic> listOfData = jsonResponse[DataKey.profiles];
        var _newData = UserProfile.generateUserProfile(listOfData);
        _userData..addAll(List<UserProfile>.from(_newData));
        _userData.forEach((eachUserData) {
          utils.addToUserDataMap(eachUserData);
        });
        refreshThisPage();
        completer.complete();
      }).catchError((onError) {
        print("error: $onError");
      });
    }
  }

  bool _checkIfNeedCallUserData(List<String> usernames) {
    List<String> tempList = [];
    List<bool> tempBoolList = [];
    usernames.forEach((eachUsername) {
      if (eachUsername.contains(userTag)) {
        tempList.add(utils.getTextAfterUserTag(eachUsername));
      }
    });
    tempList.forEach((eachUsername) {
      if (usernameVsProfileMap.containsKey(eachUsername)) {
        tempBoolList.add(false);
      }
    });
    return tempList.length != tempBoolList.length;
  }

  Widget _smartText() {
    return SmartText(
        isOverflow: isOverflow,
        isBold: isBold,
        style: style ?? utils.getNameTextStyle(mainColor, isBold),
        text: text,
        maxLines: maxLines,
        onUrlClick: (url) {
          getRoute.navigateTo(
              CustomWebView(link: url,)
          );
        },
        onNormalTagClick: (tag) {

        },
        onUserTagClick: (tag) {
          if (usernameVsProfileMap.containsKey(utils.getTextAfterUserTag(tag))) {
            getRoute.navigateTo(ViewProfilePage(profile: usernameVsProfileMap[utils.getTextAfterUserTag(tag)]));
          }
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      future: _loading,
      builder: (BuildContext context, snapshot) {

        String _targetUsername = utils.getTextAfterUserTag(text);

        if (widget.showImage && usernameVsProfileMap.containsKey(_targetUsername)) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.showImage ? Thumbnail(
                width: 30,
                height: 30,
                imageUrl: snapshot.connectionState == ConnectionState.done || usernameVsProfileMap.containsKey(_targetUsername) ? usernameVsProfileMap[_targetUsername].image : placeholderLink,
                text: _targetUsername,
                color: mainColor,
                disableOnTap: snapshot.connectionState != ConnectionState.done,
              ) : Container(
                width: 0,
                height: 0,
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: _smartText(),
              ),
            ],
          );
        }
        return _smartText();
      },
    );
  }
}