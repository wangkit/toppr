import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:toppr/config/app_config.dart';

import 'custom_emoji_picker.dart';

class Emojier extends StatefulWidget {
  final Function onEmojiClicked;
  final int rowNumber;
  final int columnNumber;

  Emojier({Key key, this.onEmojiClicked, this.rowNumber = 4, this.columnNumber = 9}) : super(key: key);

  @override
  _EmojierState createState() => _EmojierState();
}

class _EmojierState extends State<Emojier> {

  @override
  Widget build(BuildContext context) {

    return CustomEmojiPicker(
      bgColor: appBgColor,
      rows: widget.rowNumber,
      columns: widget.columnNumber,
      selectedCategory: Category.RECENT,
      onEmojiSelected: (emoji, category) {
        widget.onEmojiClicked(emoji, category);
      },
      noRecentsStyle: TextStyle(
        color: mainColor,
      ),
      buttonMode: Platform.isIOS ? ButtonMode.CUPERTINO : ButtonMode.MATERIAL,
    );
  }
}
