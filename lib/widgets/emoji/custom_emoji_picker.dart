library emoji_picker;

import 'dart:io';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'dart:math';
import 'custom_emoji_lists.dart' as emojiList;

/// All the possible categories that [Emoji] can be put into
///
/// All [Category] are shown in the keyboard bottombar with the exception of [Category.RECOMMENDED]
/// which only displays when keywords are given
enum Category {
  RECOMMENDED,
  RECENT,
  SMILEYS,
  ANIMALS,
  FOODS,
  TRAVEL,
  ACTIVITIES,
  OBJECTS,
  SYMBOLS,
  FLAGS
}

/// Enum to alter the keyboard button style
enum ButtonMode {
  /// Android button style - gives the button a app.splash color with ripple effect
  MATERIAL,

  /// iOS button style - gives the button a fade out effect when pressed
  CUPERTINO
}

/// Callback function for when emoji is selected
///
/// The function returns the selected [Emoji] as well as the [Category] from which it originated
typedef void OnEmojiSelected(Emoji emoji, Category category);

/// The Emoji Keyboard widget
///
/// This widget displays a grid of [Emoji] sorted by [Category] which the user can horizontally scroll through.
///
/// There is also a bottombar which displays all the possible [Category] and allow the user to quickly switch to that [Category]
class CustomEmojiPicker extends StatefulWidget {
  @override
  _CustomEmojiPickerState createState() => new _CustomEmojiPickerState();

  /// Number of columns in keyboard grid
  int columns;

  /// Number of rows in keyboard grid
  int rows;

  /// The currently selected [Category]
  ///
  /// This [Category] will have its button in the bottombar darkened
  Category selectedCategory;

  /// The function called when the emoji is selected
  OnEmojiSelected onEmojiSelected;

  /// The background color of the keyboard
  Color bgColor;

  /// The color of the keyboard page indicator
  Color indicatorColor;

  Color _defaultBgColor = Color.fromRGBO(242, 242, 242, 1);

  /// A list of keywords that are used to provide the user with recommended emojis in [Category.RECOMMENDED]
  List<String> recommendKeywords;

  /// The maximum number of emojis to be recommended
  int numRecommended;

  /// The string to be displayed if no recommendations found
  String noRecommendationsText;

  /// The text style for the [noRecommendationsText]
  TextStyle noRecommendationsStyle;

  /// The string to be displayed if no recent emojis to display
  String noRecentsText;

  /// The text style for the [noRecentsText]
  TextStyle noRecentsStyle;

  /// Determines the icon to display for each [Category]
  CategoryIcons categoryIcons;

  /// Determines the style given to the keyboard keys
  ButtonMode buttonMode;

  CustomEmojiPicker({
    Key key,
    @required this.onEmojiSelected,
    this.columns = 7,
    this.rows = 3,
    this.selectedCategory,
    this.bgColor,
    this.indicatorColor = Colors.blue,
    this.recommendKeywords,
    this.numRecommended = 10,
    this.noRecommendationsText = "No Recommendations",
    this.noRecommendationsStyle,
    this.noRecentsText = "No Recents",
    this.noRecentsStyle,
    this.categoryIcons,
    this.buttonMode = ButtonMode.MATERIAL,
    //this.unavailableEmojiIcon,
  }) : super(key: key) {
    if (selectedCategory == null) {
      if (recommendKeywords == null) {
        selectedCategory = Category.SMILEYS;
      } else {
        selectedCategory = Category.RECOMMENDED;
      }
    } else if (recommendKeywords == null &&
        selectedCategory == Category.RECOMMENDED) {
      selectedCategory = Category.SMILEYS;
    }

    if (this.noRecommendationsStyle == null) {
      noRecommendationsStyle = TextStyle(fontSize: 20, color: Colors.black26);
    }

    if (this.noRecentsStyle == null) {
      noRecentsStyle = TextStyle(fontSize: 20, color: Colors.black26);
    }

    if (this.bgColor == null) {
      bgColor = _defaultBgColor;
    }

    if (categoryIcons == null) {
      categoryIcons = CategoryIcons();
    }
  }
}

class _Recommended {
  final String name;
  final String emoji;
  final int tier;
  final int numSplitEqualKeyword;
  final int numSplitPartialKeyword;

  _Recommended(
      {this.name,
        this.emoji,
        this.tier,
        this.numSplitEqualKeyword = 0,
        this.numSplitPartialKeyword = 0});
}

/// Class that defines the icon representing a [Category]
class CategoryIcon {
  /// The icon to represent the category
  IconData icon;

  /// The default color of the icon
  Color color;

  /// The color of the icon once the category is selected
  Color selectedColor;

  CategoryIcon({@required this.icon, this.color, this.selectedColor}) {
    if (this.color == null) {
      this.color = Color.fromRGBO(211, 211, 211, 1);
    }
    if (this.selectedColor == null) {
      this.selectedColor = utils.getVisibleColor(myProfile.color);
    }
  }
}

/// Class used to define all the [CategoryIcon] shown for each [Category]
///
/// This allows the keyboard to be personalized by changing icons shown.
/// If a [CategoryIcon] is set as null or not defined during initialization, the default icons will be used instead
class CategoryIcons {
  /// Icon for [Category.RECOMMENDED]
  CategoryIcon recommendationIcon;

  /// Icon for [Category.RECENT]
  CategoryIcon recentIcon;

  /// Icon for [Category.SMILEYS]
  CategoryIcon smileyIcon;

  /// Icon for [Category.ANIMALS]
  CategoryIcon animalIcon;

  /// Icon for [Category.FOODS]
  CategoryIcon foodIcon;

  /// Icon for [Category.TRAVEL]
  CategoryIcon travelIcon;

  /// Icon for [Category.ACTIVITIES]
  CategoryIcon activityIcon;

  /// Icon for [Category.OBJECTS]
  CategoryIcon objectIcon;

  /// Icon for [Category.SYMBOLS]
  CategoryIcon symbolIcon;

  /// Icon for [Category.FLAGS]
  CategoryIcon flagIcon;

  CategoryIcons(
      {this.recommendationIcon,
        this.recentIcon,
        this.smileyIcon,
        this.animalIcon,
        this.foodIcon,
        this.travelIcon,
        this.activityIcon,
        this.objectIcon,
        this.symbolIcon,
        this.flagIcon}) {
    if (recommendationIcon == null) {
      recommendationIcon = CategoryIcon(icon: Icons.search);
    }
    if (recentIcon == null) {
      recentIcon = CategoryIcon(icon: Icons.access_time);
    }
    if (smileyIcon == null) {
      smileyIcon = CategoryIcon(icon: Icons.tag_faces);
    }
    if (animalIcon == null) {
      animalIcon = CategoryIcon(icon: Icons.pets);
    }
    if (foodIcon == null) {
      foodIcon = CategoryIcon(icon: Icons.fastfood);
    }
    if (travelIcon == null) {
      travelIcon = CategoryIcon(icon: Icons.location_city);
    }
    if (activityIcon == null) {
      activityIcon = CategoryIcon(icon: Icons.directions_run);
    }
    if (objectIcon == null) {
      objectIcon = CategoryIcon(icon: Icons.lightbulb_outline);
    }
    if (symbolIcon == null) {
      symbolIcon = CategoryIcon(icon: Icons.euro_symbol);
    }
    if (flagIcon == null) {
      flagIcon = CategoryIcon(icon: Icons.flag);
    }
  }
}

/// A class to store data for each individual emoji
class Emoji {
  /// The name or description for this emoji
  final String name;

  /// The unicode string for this emoji
  ///
  /// This is the string that should be displayed to view the emoji
  final String emoji;

  Emoji({@required this.name, @required this.emoji});

  @override
  String toString() {
    return "Name: " + name + ", Emoji: " + emoji;
  }
}

class _CustomEmojiPickerState extends State<CustomEmojiPicker> {

  List<Widget> pages = new List();
  int recommendedPagesNum;
  final int recentPagesNum = 0;
  final int smileyPagesNum = 1;
  final int animalPagesNum = 2;
  final int foodPagesNum = 3;
  final int travelPagesNum = 4;
  final int activityPagesNum = 5;
  final int objectPagesNum = 6;
  final int symbolPagesNum = 7;
  final int flagPagesNum = 8;
  List<String> allNames = new List();
  List<String> allEmojis = new List();
  PageController pageController;
  bool loaded = true;
  double emojiFontSize = 28;

  @override
  void initState() {
    updateEmojis();
    pageController = PageController(
        initialPage: recentPagesNum
    );
    pageController.addListener(() {
      /// This refresh is to update the darkened icon of the selected category bar
      refreshThisPage();
    });
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      /// Somehow the pageview will jump to last index even though the page controller is inited with recentPageNum
      /// So add this line to force it to show first index
      pageController.jumpToPage(recentPagesNum);
    });
  }

  Future<List<String>> getRecentEmojis() async {
    /// This is merely used to allow update of the recent page after clicking an emoji
    return recentEmojis;
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void addRecentEmoji(Emoji emoji) {
    while (recentEmojis.contains(emoji.name)) {
      recentEmojis.remove(emoji.name);
    }
    setState(() {
      recentEmojis.insert(0, emoji.name);
      prefs.setStringList(PrefKey.recentEmoji, recentEmojis);
    });
  }

  void updateEmojis() {
    allNames.addAll(smileyMap.keys);
    allNames.addAll(animalMap.keys);
    allNames.addAll(foodMap.keys);
    allNames.addAll(travelMap.keys);
    allNames.addAll(activityEmojiMap.keys);
    allNames.addAll(objectMap.keys);
    allNames.addAll(symbolMap.keys);
    allNames.addAll(flagMap.keys);

    allEmojis.addAll(smileyMap.values);
    allEmojis.addAll(animalMap.values);
    allEmojis.addAll(foodMap.values);
    allEmojis.addAll(travelMap.values);
    allEmojis.addAll(activityEmojiMap.values);
    allEmojis.addAll(objectMap.values);
    allEmojis.addAll(symbolMap.values);
    allEmojis.addAll(flagMap.values);

    recommendedPagesNum = 0;
    List<_Recommended> recommendedEmojis = new List();
    List<Widget> recommendedPages = new List();

    if (widget.recommendKeywords != null) {
      allNames.forEach((name) {
        int numSplitEqualKeyword = 0;
        int numSplitPartialKeyword = 0;

        widget.recommendKeywords.forEach((keyword) {
          if (name.toLowerCase() == keyword.toLowerCase()) {
            recommendedEmojis.add(_Recommended(
                name: name, emoji: allEmojis[allNames.indexOf(name)], tier: 1));
          } else {
            List<String> splitName = name.split(" ");

            splitName.forEach((splitName) {
              if (splitName.replaceAll(":", "").toLowerCase() ==
                  keyword.toLowerCase()) {
                numSplitEqualKeyword += 1;
              } else if (splitName
                  .replaceAll(":", "")
                  .toLowerCase()
                  .contains(keyword.toLowerCase())) {
                numSplitPartialKeyword += 1;
              }
            });
          }
        });

        if (numSplitEqualKeyword > 0) {
          if (numSplitEqualKeyword == name.split(" ").length) {
            recommendedEmojis.add(_Recommended(
                name: name, emoji: allEmojis[allNames.indexOf(name)], tier: 1));
          } else {
            recommendedEmojis.add(_Recommended(
                name: name,
                emoji: allEmojis[allNames.indexOf(name)],
                tier: 2,
                numSplitEqualKeyword: numSplitEqualKeyword,
                numSplitPartialKeyword: numSplitPartialKeyword));
          }
        } else if (numSplitPartialKeyword > 0) {
          recommendedEmojis.add(_Recommended(
              name: name,
              emoji: allEmojis[allNames.indexOf(name)],
              tier: 3,
              numSplitPartialKeyword: numSplitPartialKeyword));
        }
      });

      recommendedEmojis.sort((a, b) {
        if (a.tier < b.tier) {
          return -1;
        } else if (a.tier > b.tier) {
          return 1;
        } else {
          if (a.tier == 1) {
            if (a.name.split(" ").length > b.name.split(" ").length) {
              return -1;
            } else if (a.name.split(" ").length < b.name.split(" ").length) {
              return 1;
            } else {
              return 0;
            }
          } else if (a.tier == 2) {
            if (a.numSplitEqualKeyword > b.numSplitEqualKeyword) {
              return -1;
            } else if (a.numSplitEqualKeyword < b.numSplitEqualKeyword) {
              return 1;
            } else {
              if (a.numSplitPartialKeyword > b.numSplitPartialKeyword) {
                return -1;
              } else if (a.numSplitPartialKeyword < b.numSplitPartialKeyword) {
                return 1;
              } else {
                if (a.name.split(" ").length < b.name.split(" ").length) {
                  return -1;
                } else if (a.name.split(" ").length >
                    b.name.split(" ").length) {
                  return 1;
                } else {
                  return 0;
                }
              }
            }
          } else if (a.tier == 3) {
            if (a.numSplitPartialKeyword > b.numSplitPartialKeyword) {
              return -1;
            } else if (a.numSplitPartialKeyword < b.numSplitPartialKeyword) {
              return 1;
            } else {
              return 0;
            }
          }
        }

        return 0;
      });

      if (recommendedEmojis.length > widget.numRecommended) {
        recommendedEmojis =
            recommendedEmojis.getRange(0, widget.numRecommended).toList();
      }

      if (recommendedEmojis.length != 0) {

        for (var i = 0; i < recommendedPagesNum; i++) {
          recommendedPages.add(Container(
            color: widget.bgColor,
            child: GridView.count(
              shrinkWrap: false,
              primary: true,
              crossAxisCount: widget.columns,
              children: List.generate(widget.rows * widget.columns, (index) {
                if (index + (widget.columns * widget.rows * i) <
                    recommendedEmojis.length) {
                  switch (widget.buttonMode) {
                    case ButtonMode.MATERIAL:
                      return Center(
                          child: FlatButton(
                            padding: EdgeInsets.all(0),
                            child: Center(
                              child: Text(
                                recommendedEmojis[
                                index + (widget.columns * widget.rows * i)]
                                    .emoji,
                                style: TextStyle(fontSize: emojiFontSize),
                              ),
                            ),
                            onPressed: () {
                              _Recommended recommended = recommendedEmojis[index + (widget.columns * widget.rows * i)];
                              widget.onEmojiSelected(
                                  Emoji(name: recommended.name, emoji: recommended.emoji),
                                  widget.selectedCategory,
                              );
                              addRecentEmoji(
                                Emoji(
                                  name: recommended.name,
                                  emoji: recommended.emoji,
                                ),
                              );
                            },
                          ));
                      break;
                    case ButtonMode.CUPERTINO:
                      return Center(
                          child: CupertinoButton(
                            pressedOpacity: 0.4,
                            padding: EdgeInsets.all(0),
                            child: Center(
                              child: Text(
                                recommendedEmojis[
                                index + (widget.columns * widget.rows * i)]
                                    .emoji,
                                style: TextStyle(fontSize: emojiFontSize),
                              ),
                            ),
                            onPressed: () {
                              _Recommended recommended = recommendedEmojis[
                              index + (widget.columns * widget.rows * i)];
                              widget.onEmojiSelected(
                                  Emoji(
                                      name: recommended.name,
                                      emoji: recommended.emoji),
                                  widget.selectedCategory);
                              addRecentEmoji(Emoji(
                                  name: recommended.name,
                                  emoji: recommended.emoji));
                            },
                          ));

                      break;
                    default:
                      return Container();
                      break;
                  }
                } else {
                  return Container();
                }
              }),
            ),
          ));
        }
      } else {
        recommendedPagesNum = 1;

        recommendedPages.add(Container(
            color: widget.bgColor,
            child: Center(
                child: Text(
                  widget.noRecommendationsText,
                  style: widget.noRecommendationsStyle,
                ))));
      }
    }

    List<Widget> recentPages = new List();
    recentPages.add(recentPage());

    List<Widget> smileyPages = new List();

    /// Edited to make the emoji page scroll vertically for more and horizontally for another category
    smileyPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(smileyMap.length, (index) {
          String emojiTxt = smileyMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: smileyMap.keys.toList()[index],
                emoji: emojiTxt,
            );
            widget.onEmojiSelected(
                emoji,
                widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
          }
        }),
      ),
    ));

    List<Widget> animalPages = new List();

    animalPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(animalMap.length, (index) {
          String emojiTxt = animalMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: animalMap.keys.toList()[index],
                emoji: animalMap.values.toList()[index]
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                          emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> foodPages = new List();

    foodPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(foodMap.length, (index) {
          String emojiTxt = foodMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: foodMap.keys.toList()[index],
                emoji: foodMap.values.toList()[index]
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> travelPages = new List();

    travelPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(travelMap.length, (index) {

          String emojiTxt = travelMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: travelMap.keys.toList()[index],
                emoji: emojiTxt,
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> activityPages = new List();

    activityPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(activityEmojiMap.length, (index) {

          String emojiTxt = activityEmojiMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: activityEmojiMap.keys.toList()[index],
                emoji: emojiTxt,
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> objectPages = new List();

    objectPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(objectMap.length, (index) {

          String emojiTxt = objectMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: objectMap.keys.toList()[index],
                emoji: emojiTxt,
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> symbolPages = new List();

    symbolPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(symbolMap.length, (index) {

          String emojiTxt = symbolMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
                name: symbolMap.keys.toList()[index],
                emoji: emojiTxt,
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    List<Widget> flagPages = new List();

    flagPages.add(Container(
      color: widget.bgColor,
      child: GridView.count(
        shrinkWrap: false,
        primary: true,
        crossAxisCount: widget.columns,
        children: List.generate(flagMap.length, (index) {

          String emojiTxt = flagMap.values.toList()[index];

          void onPressed() {
            var emoji = Emoji(
              name: flagMap.keys.toList()[index],
              emoji: emojiTxt,
            );
            widget.onEmojiSelected(
              emoji,
              widget.selectedCategory,
            );
            addRecentEmoji(emoji);
          }

          switch (widget.buttonMode) {
            case ButtonMode.MATERIAL:
              return Center(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            case ButtonMode.CUPERTINO:
              return Center(
                  child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        emojiTxt,
                        style: TextStyle(fontSize: emojiFontSize),
                      ),
                    ),
                    onPressed: onPressed,
                  ));
              break;
            default:
              return Container();
              break;
          }
        }),
      ),
    ));

    pages.addAll(recommendedPages);
    pages.addAll(recentPages);
    pages.addAll(smileyPages);
    pages.addAll(animalPages);
    pages.addAll(foodPages);
    pages.addAll(travelPages);
    pages.addAll(activityPages);
    pages.addAll(objectPages);
    pages.addAll(symbolPages);
    pages.addAll(flagPages);

    getRecentEmojis().then((_) {
      pages.removeAt(recommendedPagesNum);
      pages.insert(recommendedPagesNum, recentPage());
      if (this.mounted) {
        setState(() {});
      }
    });
  }

  Widget recentPage() {
    return Container(
      color: widget.bgColor,
      child: FutureBuilder(
        future: getRecentEmojis(),
        builder: (BuildContext ctx, snapshot) {
          return GridView.count(
            shrinkWrap: false,
            primary: true,
            crossAxisCount: widget.columns,
            children: List.generate(recentEmojis.length, (index) {

              var _tapPosition;
              String emojiName = recentEmojis[index];

//              void _onRecentLongPressed() {
//                getGenericMenu(context, _tapPosition, <PopupMenuEntry>[
//                  PopupMenuItem(
//                    height: 0,
//                    /// Set enabled to false to avoid weird ink response from drawing on tap
//                    enabled: false,
//                    child: Bubble(
//                      color: utils.getBgColor(isPureColor: true),
//                      alignment: Alignment.topLeft,
//                      nip: BubbleNip.leftTop,
//                      child: InkWell(
//                        /// This on tap will trigger on its own regardless of the enabled setting above
//                        onTap: () {
//                          getRoute.pop();
//                          recentEmojis.remove(emojiName);
//                          prefs.setStringList(emojiRecentKey, recentEmojis);
//                          if (this.mounted) {
//                            setState(() {});
//                          }
//                        },
//                        child: Text(
//                          "Remove",
//                          style: TextStyle(
//                            color: mainColor,
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                ]);
//              }

              void _onRecentPressed() {
                widget.onEmojiSelected(
                  Emoji(
                      name: emojiName,
                      emoji: allEmojis[allNames.indexOf(emojiName)],
                  ),
                  widget.selectedCategory,
                );
              }

              void _storePosition(TapDownDetails details) {
                _tapPosition = details.globalPosition;
              }

              if (index < recentEmojis.length) {
                switch (widget.buttonMode) {
                  case ButtonMode.MATERIAL:
                    return Center(
                      child: GestureDetector(
                        onTapDown: _storePosition,
                        child: FlatButton(
                          padding: EdgeInsets.all(0),
                          child: Center(
                            child: Text(
                              allEmojis[allNames.indexOf(emojiName)],
                              style: TextStyle(fontSize: emojiFontSize),
                            ),
                          ),
//                            onLongPress: () => _onRecentLongPressed(),
                          onPressed: () => _onRecentPressed(),
                        ),
                      ),
                    );
                    break;
                  case ButtonMode.CUPERTINO:
                    return Center(
                        child: CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          child: Center(
                            child: Text(
                              allEmojis[allNames.indexOf(emojiName)],
                              style: TextStyle(fontSize: emojiFontSize),
                            ),
                          ),
//                          onLongPressed: () => _onRecentLongPressed(),
                          onPressed: () => _onRecentPressed(),
                        ));

                    break;
                  default:
                    return Container();
                    break;
                }
              } else {
                return Container();
              }
            }),
          );
        },
      ),
    );
  }

  Widget defaultButton(CategoryIcon categoryIcon) {
    return SizedBox(
      width: deviceWidth /
          (widget.recommendKeywords == null ? 9 : 10),
      height: deviceWidth /
          (widget.recommendKeywords == null ? 9 : 10),
      child: Container(
        color: widget.bgColor,
        child: Center(
          child: Icon(
            categoryIcon.icon,
            size: 22,
            color: categoryIcon.color,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (loaded) {
      pages.removeAt(recommendedPagesNum);
      pages.insert(recommendedPagesNum, recentPage());

      return Column(
        children: <Widget>[
          Container(
            height: 50,
            color: widget.bgColor,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: deviceWidth / 9,
                    height: deviceWidth / 9,
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.RECENT
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.recentIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.RECENT
                              ? widget.categoryIcons.recentIcon
                              .selectedColor
                              : widget.categoryIcons.recentIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.RECENT) {
                          return;
                        }
                        pageController.jumpToPage(recentPagesNum);
                      },
                    ) : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.RECENT
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.recentIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.RECENT
                              ? widget.categoryIcons.recentIcon
                              .selectedColor
                              : widget.categoryIcons.recentIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.RECENT) {
                          return;
                        }
                        pageController.jumpToPage(recentPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.SMILEYS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.smileyIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.SMILEYS
                              ? widget.categoryIcons.smileyIcon
                              .selectedColor
                              : widget.categoryIcons.smileyIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.SMILEYS) {
                          return;
                        }

                        pageController.jumpToPage(smileyPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.SMILEYS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.smileyIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.SMILEYS
                              ? widget.categoryIcons.smileyIcon
                              .selectedColor
                              : widget.categoryIcons.smileyIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.SMILEYS) {
                          return;
                        }
                        pageController.jumpToPage(smileyPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.ANIMALS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.animalIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.ANIMALS
                              ? widget.categoryIcons.animalIcon
                              .selectedColor
                              : widget.categoryIcons.animalIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.ANIMALS) {
                          return;
                        }
                        pageController.jumpToPage(animalPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.ANIMALS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.animalIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.ANIMALS
                              ? widget.categoryIcons.animalIcon
                              .selectedColor
                              : widget.categoryIcons.animalIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.ANIMALS) {
                          return;
                        }
                        pageController.jumpToPage(animalPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.FOODS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.foodIcon.icon,
                          size: 22,
                          color: widget.selectedCategory == Category.FOODS
                              ? widget
                              .categoryIcons.foodIcon.selectedColor
                              : widget.categoryIcons.foodIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.FOODS) {
                          return;
                        }
                        pageController.jumpToPage(foodPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.FOODS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.foodIcon.icon,
                          size: 22,
                          color: widget.selectedCategory == Category.FOODS
                              ? widget
                              .categoryIcons.foodIcon.selectedColor
                              : widget.categoryIcons.foodIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.FOODS) {
                          return;
                        }
                        pageController.jumpToPage(foodPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.TRAVEL
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.travelIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.TRAVEL
                              ? widget.categoryIcons.travelIcon
                              .selectedColor
                              : widget.categoryIcons.travelIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.TRAVEL) {
                          return;
                        }
                        pageController.jumpToPage(travelPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.TRAVEL
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.travelIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.TRAVEL
                              ? widget.categoryIcons.travelIcon
                              .selectedColor
                              : widget.categoryIcons.travelIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.TRAVEL) {
                          return;
                        }
                        pageController.jumpToPage(travelPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color:
                      widget.selectedCategory == Category.ACTIVITIES
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.activityIcon.icon,
                          size: 22,
                          color: widget.selectedCategory ==
                              Category.ACTIVITIES
                              ? widget.categoryIcons.activityIcon
                              .selectedColor
                              : widget.categoryIcons.activityIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory ==
                            Category.ACTIVITIES) {
                          return;
                        }
                        pageController.jumpToPage(activityPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color:
                      widget.selectedCategory == Category.ACTIVITIES
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.activityIcon.icon,
                          size: 22,
                          color: widget.selectedCategory ==
                              Category.ACTIVITIES
                              ? widget.categoryIcons.activityIcon
                              .selectedColor
                              : widget.categoryIcons.activityIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory ==
                            Category.ACTIVITIES) {
                          return;
                        }
                        pageController.jumpToPage(activityPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.OBJECTS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.objectIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.OBJECTS
                              ? widget.categoryIcons.objectIcon
                              .selectedColor
                              : widget.categoryIcons.objectIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.OBJECTS) {
                          return;
                        }
                        pageController.jumpToPage(objectPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.OBJECTS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.objectIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.OBJECTS
                              ? widget.categoryIcons.objectIcon
                              .selectedColor
                              : widget.categoryIcons.objectIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.OBJECTS) {
                          return;
                        }
                        pageController.jumpToPage(objectPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.SYMBOLS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.symbolIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.SYMBOLS
                              ? widget.categoryIcons.symbolIcon
                              .selectedColor
                              : widget.categoryIcons.symbolIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.SYMBOLS) {
                          return;
                        }
                        pageController.jumpToPage(symbolPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.SYMBOLS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.symbolIcon.icon,
                          size: 22,
                          color:
                          widget.selectedCategory == Category.SYMBOLS
                              ? widget.categoryIcons.symbolIcon
                              .selectedColor
                              : widget.categoryIcons.symbolIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.SYMBOLS) {
                          return;
                        }
                        pageController.jumpToPage(symbolPagesNum);
                      },
                    ),
                  ),
                  SizedBox(
                    width: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    height: deviceWidth /
                        (widget.recommendKeywords == null ? 9 : 10),
                    child: widget.buttonMode == ButtonMode.MATERIAL
                        ? FlatButton(
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.FLAGS
                          ? Colors.black12
                          : Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0))),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.flagIcon.icon,
                          size: 22,
                          color: widget.selectedCategory == Category.FLAGS
                              ? widget
                              .categoryIcons.flagIcon.selectedColor
                              : widget.categoryIcons.flagIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.FLAGS) {
                          return;
                        }
                        pageController.jumpToPage(flagPagesNum);
                      },
                    )
                        : CupertinoButton(
                      pressedOpacity: 0.4,
                      padding: EdgeInsets.all(0),
                      color: widget.selectedCategory == Category.FLAGS
                          ? Colors.black12
                          : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: Center(
                        child: Icon(
                          widget.categoryIcons.flagIcon.icon,
                          size: 22,
                          color: widget.selectedCategory == Category.FLAGS
                              ? widget
                              .categoryIcons.flagIcon.selectedColor
                              : widget.categoryIcons.flagIcon.color,
                        ),
                      ),
                      onPressed: () {
                        if (widget.selectedCategory == Category.FLAGS) {
                          return;
                        }
                        pageController.jumpToPage(flagPagesNum);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: (deviceWidth / widget.columns) * widget.rows,
            width: deviceWidth,
            child: PageView(
                children: pages,
                controller: pageController,
                onPageChanged: (index) {
                  if (widget.recommendKeywords != null && index == recommendedPagesNum) {
                    widget.selectedCategory = Category.RECOMMENDED;
                  } else if (index == recentPagesNum) {
                    widget.selectedCategory = Category.RECENT;
                  } else if (index == smileyPagesNum) {
                    widget.selectedCategory = Category.SMILEYS;
                  } else if (index == animalPagesNum) {
                    widget.selectedCategory = Category.ANIMALS;
                  } else if (index == foodPagesNum) {
                    widget.selectedCategory = Category.FOODS;
                  } else if (index == travelPagesNum) {
                    widget.selectedCategory = Category.TRAVEL;
                  } else if (index == activityPagesNum) {
                    widget.selectedCategory = Category.ACTIVITIES;
                  } else if (index == objectPagesNum) {
                    widget.selectedCategory = Category.OBJECTS;
                  } else if (index == symbolPagesNum) {
                    widget.selectedCategory = Category.SYMBOLS;
                  } else {
                    widget.selectedCategory = Category.FLAGS;
                  }
                },
            ),
          ),
          /// Removed progress painter because it's useless
        ],
      );
    } else {
      /// While loading widget
      return Column(
        children: <Widget>[
          Container(
            height: 50,
            child: Row(
              children: <Widget>[
                widget.recommendKeywords != null
                    ? defaultButton(widget.categoryIcons.recommendationIcon)
                    : Container(),
                defaultButton(widget.categoryIcons.recentIcon),
                defaultButton(widget.categoryIcons.smileyIcon),
                defaultButton(widget.categoryIcons.animalIcon),
                defaultButton(widget.categoryIcons.foodIcon),
                defaultButton(widget.categoryIcons.travelIcon),
                defaultButton(widget.categoryIcons.activityIcon),
                defaultButton(widget.categoryIcons.objectIcon),
                defaultButton(widget.categoryIcons.symbolIcon),
                defaultButton(widget.categoryIcons.flagIcon),
              ],
            ),
          ),
          SizedBox(
            height: (deviceWidth / widget.columns) * widget.rows,
            width: deviceWidth,
            child: Container(
              color: widget.bgColor,
              child: Center(
                child: LoadingWidget(),
              ),
            ),
          ),
        ],
      );
    }
  }
}

class _ProgressPainter extends CustomPainter {
  final BuildContext context;
  final PageController pageController;
  final Map<Category, int> pages;
  final Category selectedCategory;
  final Color indicatorColor;

  _ProgressPainter(this.context, this.pageController, this.pages,
      this.selectedCategory, this.indicatorColor);

  @override
  void paint(Canvas canvas, Size size) {
    double actualPageWidth = deviceWidth;
    double offsetInPages = 0;
    if (selectedCategory == Category.RECOMMENDED) {
      offsetInPages = pageController.offset / actualPageWidth;
    } else if (selectedCategory == Category.RECENT) {
      offsetInPages = (pageController.offset -
          (pages[Category.RECOMMENDED] * actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.SMILEYS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] + pages[Category.RECENT]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.ANIMALS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.FOODS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.TRAVEL) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS] +
              pages[Category.FOODS]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.ACTIVITIES) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS] +
              pages[Category.FOODS] +
              pages[Category.TRAVEL]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.OBJECTS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS] +
              pages[Category.FOODS] +
              pages[Category.TRAVEL] +
              pages[Category.ACTIVITIES]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.SYMBOLS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS] +
              pages[Category.FOODS] +
              pages[Category.TRAVEL] +
              pages[Category.ACTIVITIES] +
              pages[Category.OBJECTS]) *
              actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.FLAGS) {
      offsetInPages = (pageController.offset -
          ((pages[Category.RECOMMENDED] +
              pages[Category.RECENT] +
              pages[Category.SMILEYS] +
              pages[Category.ANIMALS] +
              pages[Category.FOODS] +
              pages[Category.TRAVEL] +
              pages[Category.ACTIVITIES] +
              pages[Category.OBJECTS] +
              pages[Category.SYMBOLS]) *
              actualPageWidth)) /
          actualPageWidth;
    }
    double indicatorPageWidth = size.width / pages[selectedCategory];

    Rect bgRect = Offset(0, 0) & size;

    Rect indicator = Offset(max(0, offsetInPages * indicatorPageWidth), 0) &
    Size(
        indicatorPageWidth -
            max(
                0,
                (indicatorPageWidth +
                    (offsetInPages * indicatorPageWidth)) -
                    size.width) +
            min(0, offsetInPages * indicatorPageWidth),
        size.height);

    canvas.drawRect(bgRect, Paint()..color = Colors.black12);
    canvas.drawRect(indicator, Paint()..color = indicatorColor);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
