import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/firestore_tables.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/button/custom_icon_button.dart';
import 'package:toppr/widgets/emoji/emoji.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:uuid/uuid.dart';

class ReplyBar extends StatefulWidget {
  final TextEditingController inputController;
  final ExpandableController expandableController;
  final bool isExpanded;
  final String postId;

  ReplyBar({
    Key key,
    @required this.inputController,
    @required this.isExpanded,
    @required this.expandableController,
    @required this.postId,
  }) : super(key: key);

  @override
  ReplyBarState createState() => ReplyBarState();
}

class ReplyBarState extends State<ReplyBar> {

  TextEditingController _inputController;
  FocusNode _inputFocusNode;
  String _replyReplyId = "";
  String _replyReplyTargetUsername = "";
  GlobalKey<FormState> _formKey = GlobalKey();
  double _padding = 8;

  void unfocus() {
    _inputFocusNode.unfocus();
  }

  void setReplyReplyId(String targetReplyId, String targetName) {
    _inputFocusNode.unfocus();
    if (_replyReplyId.isNullOrEmpty) {
      _replyReplyId = targetReplyId;
      _replyReplyTargetUsername = targetName;
    } else {
      _replyReplyId = "";
      _replyReplyTargetUsername = "";
    }
    refreshThisPage();
  }

  void setTextController(String target) {
    _inputController.text = target;
    _inputFocusNode.requestFocus();
  }

  void removeReplyReplyId() {
    _inputFocusNode.unfocus();
    _replyReplyId = "";
    _replyReplyTargetUsername = "";
    refreshThisPage();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _inputController = widget.inputController;
    _inputFocusNode = FocusNode();
    super.initState();
  }
  
  @override
  void dispose() {
    _inputFocusNode.dispose();
    super.dispose();
  }

  void closeEmojiOpenKeyboard() {
    if (widget.isExpanded) {
      Future<void> waitForCloseEmoji() async {
        widget.expandableController.toggle();
      }
      waitForCloseEmoji().then((_) {
        Future.delayed(Duration(milliseconds: 100), () {
          FocusScope.of(context).requestFocus(_inputFocusNode);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(_padding),
          child: Row(
            children: <Widget>[
              Flexible(
                child: AnimatedContainer(
                  duration: kThemeAnimationDuration,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: _replyReplyId.isNullOrEmpty ? myProfile == null ? unfocusedColor : utils.getColorByHex(myProfile.color) : unfocusedColor,
                  ),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(2.0),
                        child: CustomIconButton(
                          minWidth: 16.0,
                          minHeight: 16.0,
                          onPressed: () {
                            if (widget.isExpanded) {
                              closeEmojiOpenKeyboard();
                              utils.openKeyboard();
                            } else {
                              widget.expandableController.toggle();
                              utils.closeKeyboard();
                            }
                          },
                          iconSize: 21.0,
                          icon: Icon(
                            widget.isExpanded ? MdiIcons.keyboard : MdiIcons.emoticon,
                            color: utils.getCorrectContrastColor(myProfile.color),
                          ),
                        ),
                      ),
                      Container(width: 8.0),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Form(
                            key: _formKey,
                            child: TextFormField(
                              onTap: () {
                                /// We assume the user wants to type when he taps on textfield,
                                /// so close the emoji keyboard when onTap is triggered
                                closeEmojiOpenKeyboard();
                              },
                              validator: (String value) {
                                if (value.isNullOrEmpty) {
                                  return "Reply must not be empty";
                                }
                                return null;
                              },
                              focusNode: _inputFocusNode,
                              autofocus: false,
                              maxLines: null,
                              style: TextStyle(
                                color: utils.getCorrectContrastColor(myProfile.color),
                              ),
                              keyboardType: TextInputType.multiline,
                              textInputAction: TextInputAction.newline,
                              controller: _inputController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                suffixIcon: _replyReplyId.isNullOrEmpty ? null : IconButton(
                                  focusColor: dislikeIconSplash,
                                  splashColor: dislikeIconSplash,
                                  onPressed: () {
                                    /// This focus node thing is to avoid opening keyboard when pressing this IconButton
                                    _inputFocusNode.unfocus();
                                    _inputFocusNode.canRequestFocus = false;
                                    removeReplyReplyId();
                                    _inputController.clear();
                                    Future.delayed(Duration(milliseconds: 10), () {
                                      _inputFocusNode.canRequestFocus = true;
                                    });
                                  },
                                  icon: Icon(
                                    MdiIcons.deleteOutline,
                                    color: lightUnfocusedColor,
                                  ),
                                ),
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                counterText: null,
                                hintText: _replyReplyTargetUsername.isNullOrEmpty ? TextData.reply : "Replying ${utils.getUserTemplate(_replyReplyTargetUsername)}",
                                hintStyle: TextStyle(
                                  color: _replyReplyId.isNullOrEmpty ? unfocusedColor : lightUnfocusedColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: FloatingActionButton(
                  backgroundColor: transparent,
                  elevation: 0,
                  focusElevation: 0,
                  highlightElevation: 0,
                  heroTag: UniqueKey(),
                  child: AnimatedContainer(
                    width: kToolbarHeight,
                    height: kToolbarHeight,
                    duration: kThemeAnimationDuration,
                    decoration: BoxDecoration(
                      color: _replyReplyId.isNullOrEmpty ? utils.getColorByHex(myProfile.color) : unfocusedColor,
                      borderRadius: BorderRadius.circular(200)
                    ),
                    child: Icon(
                      Icons.send,
                      color: utils.getCorrectContrastColor(myProfile.color),
                    ),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      String _replyId = Uuid().v4();
                      DateTime _now = DateTime.now();
                      String messageText = _inputController.text;
                      _inputFocusNode.unfocus();
                      _inputController.clear();
                      String _taggedUsernames = utils.extractTaggedUsername(messageText);
                      apiManager.createReply(
                        widget.postId,
                        _replyId,
                        messageText,
                        _taggedUsernames,
                        targetReplyId: _replyReplyId,
                      );
                      if (_replyReplyId.isNullOrEmpty) {
                        /// Normal reply
                        firestore.collection(outerRepliesDbName)
                            .doc(widget.postId)
                            .collection(innerReplyDbName)
                            .doc(_replyId)
                            .set({
                          ReplyTable.replyId: _replyId,
                          ReplyTable.id: myProfile.id,
                          ReplyTable.postId: widget.postId,
                          ReplyTable.username: myProfile.username,
                          ReplyTable.createdAt: _now,
                          ReplyTable.reply: messageText,
                          ReplyTable.replyReplyCount: 0,
                          ReplyTable.likedProfileIds: [],
                          ReplyTable.replyReplyProfileIds: [],
                        },);
                      } else {
                        /// Increase target reply replyReplyList
                        firestore.collection(outerRepliesDbName)
                            .doc(widget.postId)
                            .collection(innerReplyDbName)
                            .doc(_replyReplyId)
                            .update({
                          ReplyTable.replyReplyCount: FieldValue.increment(1),
                          ReplyTable.replyReplyProfileIds: FieldValue.arrayUnion([myProfile.id]),
                        });
                        /// Reply a reply
                        firestore.collection(outerRepliesDbName)
                            .doc(widget.postId)
                            .collection(innerReplyDbName)
                            .doc(_replyReplyId)
                            .collection(replyReplyDbName)
                            .doc(_replyId)
                            .set({
                          ReplyTable.replyId: _replyId,
                          ReplyTable.id: myProfile.id,
                          ReplyTable.postId: widget.postId,
                          ReplyTable.username: myProfile.username,
                          ReplyTable.createdAt: _now,
                          ReplyTable.reply: messageText,
                          ReplyTable.replyReplyCount: 0,
                          ReplyTable.likedProfileIds: [],
                          ReplyTable.replyReplyProfileIds: [],
                        },);
                      }

                    }
                  },
                ),
              ),
            ],
          ),
        ),
        if (widget.isExpanded) Emojier(
          onEmojiClicked: (emoji, category) {
            utils.addEmoji(_inputController, emoji.emoji);
          },
        ) else Container(),
      ],
    );
  }
}