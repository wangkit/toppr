import 'dart:convert';
import 'dart:io';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/button/custom_icon_button.dart';
import 'package:toppr/widgets/emoji/emoji.dart';
import 'package:uuid/uuid.dart';
import 'package:validators/validators.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

class ChatInputBar extends StatefulWidget {
  final TextEditingController inputController;
  final ExpandableController expandableController;
  final UserProfile targetProfile;
  final bool isExpanded;

  ChatInputBar({
    Key key,
    @required this.inputController,
    @required this.isExpanded,
    @required this.targetProfile,
    @required this.expandableController,
  }) : super(key: key);

  @override
  _ChatInputBarState createState() => _ChatInputBarState();
}

class _ChatInputBarState extends State<ChatInputBar> {

  TextEditingController _inputController;
  FocusNode _inputFocusNode;
  UserProfile _targetProfile;
  String chatId;

  @override
  void initState() {
    _targetProfile = widget.targetProfile;
    _inputController = widget.inputController;
    _inputFocusNode = FocusNode();
    chatId = utils.getChatId([_targetProfile.id, myId]);
    super.initState();
  }
  
  @override
  void dispose() {
    _inputFocusNode.dispose();
    super.dispose();
  }

  void closeEmojiOpenKeyboard() {
    if (widget.isExpanded) {
      Future<void> waitForCloseEmoji() async {
        widget.expandableController.toggle();
      }
      waitForCloseEmoji().then((_) {
        Future.delayed(Duration(milliseconds: 100), () {
          FocusScope.of(context).requestFocus(_inputFocusNode);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  color: myProfile == null ? unfocusedColor : utils.getColorByHex(myProfile.color),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(2.0),
                        child: CustomIconButton(
                          minWidth: 16.0,
                          minHeight: 16.0,
                          onPressed: () {
                            if (widget.isExpanded) {
                              closeEmojiOpenKeyboard();
                              utils.openKeyboard();
                            } else {
                              widget.expandableController.toggle();
                              utils.closeKeyboard();
                            }
                          },
                          iconSize: 21.0,
                          icon: Icon(
                            widget.isExpanded ? MdiIcons.keyboard : MdiIcons.emoticon,
                            color: utils.getCorrectContrastColor(myProfile.color),
                          ),
                        ),
                      ),
                      Container(width: 8.0),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: TextField(
                            onTap: () {
                              /// We assume the user wants to type when he taps on textfield,
                              /// so close the emoji keyboard when onTap is triggered
                              closeEmojiOpenKeyboard();
                            },
                            focusNode: _inputFocusNode,
                            maxLines: null,
                            style: TextStyle(
                              color: utils.getCorrectContrastColor(myProfile.color),
                            ),
                            keyboardType: TextInputType.multiline,
                            textInputAction: TextInputAction.newline,
                            controller: _inputController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              counterText: null,
                              hintText: TextData.message,
                              hintStyle: TextStyle(
                                color: unfocusedColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: FloatingActionButton(
                  key: UniqueKey(),
                  heroTag: UniqueKey(),
                  backgroundColor: utils.getColorByHex(myProfile.color),
                  onPressed: () {
                    if (usernameVsProfileMap[_targetProfile.username] != null && usernameVsProfileMap[_targetProfile.username].blockedMe) {
                      utils.toast(TextData.cantSendMessageToThis, isWarning: true);
                    } else if (!_inputController.text.isNullOrEmpty) {
                      DateTime _now = DateTime.now();
                      String messageText = _inputController.text;
                      _inputController.clear();
                      /// Create my chat item if not exists
                      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: myId).where(ChatTable.targetId, isEqualTo: _targetProfile.id).get().then((result) {
                        if (result.size == 0) {
                          firestore.collection(chatDbName).add({
                            ChatTable.senderId: myId,
                            ChatTable.createdAt: _now,
                            ChatTable.targetUsername: _targetProfile.username,
                            ChatTable.hasUnread: false,
                            ChatTable.isLastMessageMyself: true,
                            ChatTable.lastMessage: messageText,
                            ChatTable.targetImage: _targetProfile.image,
                            ChatTable.targetColor: _targetProfile.color,
                            ChatTable.targetId: _targetProfile.id,
                            ChatTable.updatedAt: _now,
                          });
                        } else if (result.docs.isNotEmpty) {
                          /// Update chat table's last message and updated at
                          firestore.collection(chatDbName).doc(result.docs.first.id).update({
                            ChatTable.updatedAt: _now,
                            ChatTable.lastMessage: messageText,
                            ChatTable.isLastMessageMyself: true,
                            ChatTable.targetUsername: _targetProfile.username,
                            ChatTable.targetImage: _targetProfile.image,
                            ChatTable.targetColor: _targetProfile.color,
                          });
                        }
                      });
                      /// Create chat item for target if not exists
                      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: _targetProfile.id).where(ChatTable.targetId, isEqualTo: myId).get().then((result) {
                        if (result.size == 0) {
                          firestore.collection(chatDbName).add(
                              {
                                ChatTable.senderId: _targetProfile.id,
                                ChatTable.createdAt: _now,
                                ChatTable.targetUsername: myProfile.username,
                                ChatTable.hasUnread: true,
                                /// is last message myself here means is last message by target
                                ChatTable.isLastMessageMyself: false,
                                ChatTable.lastMessage: messageText,
                                ChatTable.targetImage: myProfile.image,
                                ChatTable.targetId: myProfile.id,
                                ChatTable.targetColor: myProfile.color,
                                ChatTable.updatedAt: _now,
                              }
                          );
                        } else if (result.docs.isNotEmpty) {
                          /// Update chat table's last message and updated at and hasUnread to notify target that there is a new message
                          firestore.collection(chatDbName).doc(result.docs.first.id).update({
                            ChatTable.updatedAt: _now,
                            ChatTable.lastMessage: messageText,
                            ChatTable.hasUnread: true,
                            ChatTable.isLastMessageMyself: false,
                            ChatTable.targetUsername: myProfile.username,
                            ChatTable.targetImage: myProfile.image,
                            ChatTable.targetColor: myProfile.color,
                          });
                        }
                      });
                      /// Send message
                      firestore.collection(messageDbName).add(
                        {
                          MessageItem.chatId: chatId,
                          MessageItem.messageId: Uuid().v4(),
                          MessageItem.message: messageText,
                          MessageItem.senderId: myId,
                          MessageItem.receiverId: _targetProfile.id,
                          MessageItem.createdAt: _now,
                        },
                      );
                    }
                  },
                  child: Icon(
                    Icons.send,
                    color: utils.getCorrectContrastColor(myProfile.color),
                  ),
                ),
              ),
            ],
          ),
        ),
        widget.isExpanded ? Emojier(
          onEmojiClicked: (emoji, category) {
            utils.addEmoji(_inputController, emoji.emoji);
          },
        ) : Container(),
      ],
    );
  }
}