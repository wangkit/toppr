import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:validators/validators.dart';

class PasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final String label;
  final String helper;
  final bool isNewPassword;
  final EdgeInsets edgeInsets;

  PasswordTextField({
    Key key,
    this.isNewPassword = false,
    @required this.controller,
    this.focusNode,
    this.nextFocusNode,
    this.label,
    this.helper,
    this.edgeInsets,
  }) : super(key: key);

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  String label;
  String helper;
  bool isNewPassword;
  bool isObscureText;
  int minLength;
  bool isValidFormat;
  bool isVisible;
  final regPattern = RegExp(r"^(?=.*[a-zA-Z])(?=.*[0-9])");

  @override
  void initState() {
    isVisible = false;
    isNewPassword = widget.isNewPassword;
    minLength = 8;
    isObscureText = true;
    isValidFormat = true;
    label = widget.label;
    focusNode = widget.focusNode;
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    helper = widget.helper;
    focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  String validate(String value) {
    if (value.isEmpty) {
      return "Password must not be empty";
    } else if (value.length < minLength) {
      return "Password must contain at least eight characters";
    } else if (!regPattern.hasMatch(value)) {
      return "Password must contain at least one letter and one number";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        onFieldSubmitted: (term) {
          if (focusNode != null && nextFocusNode != null) {
            utils.fieldFocusChange(focusNode, nextFocusNode);
          }
        },
        obscureText: isObscureText,
        autofillHints: isNewPassword ? [AutofillHints.newPassword] : [AutofillHints.password],
        focusNode: focusNode,
        style: TextStyle(
          color: mainColor,
        ),
        controller: controller,
        validator: (value) {
          return validate(value);
        },
        maxLength: 25,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
          helperText: helper,
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 1,
          counterText: "",
          errorStyle: TextStyle(
            color: errorColor,
          ),
          hintText: TextData.password,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: IconButton(
              icon: Icon(
                isObscureText ? MdiIcons.eye : MdiIcons.eyeOff,
                color: unfocusedColor,
              ),
              onPressed: () {
                setState(() {
                  isObscureText = !isObscureText;
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}