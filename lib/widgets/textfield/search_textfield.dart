import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/view/view.dart';
import 'package:validators/validators.dart';

class SearchTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final Function onChangedRefresh;
  final String title;
  final String leadingTag;
  final Widget suffix;
  final EdgeInsets padding;

  SearchTextField({
    Key key,
    @required this.leadingTag,
    @required this.title,
    @required this.onChangedRefresh,
    @required this.controller,
    this.focusNode,
    this.padding,
    this.suffix,
  }) : super(key: key);

  @override
  _SearchTextFieldState createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding != null ? widget.padding : EdgeInsets.only(left: 12.0, right: 12.0, top: 24.0, bottom: 12.0),
      child: TextFormField(
        focusNode: widget.focusNode,
        style: TextStyle(
          color: mainColor,
        ),
        controller: widget.controller,
        onChanged: widget.onChangedRefresh,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          filled: true,
          fillColor: defaultWhiteColor,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: utils.getColorByHex(myProfile.color),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: utils.getColorByHex(myProfile.color),
            ),
          ),
          counterText: "",
          helperMaxLines: 1,
          hintMaxLines: 1,
          errorMaxLines: 1,
          helperStyle: TextStyle(
            color: mainColor,
          ),
          prefixIcon: Icon(
            Icons.search,
            color: unfocusedColor,
            size: 20
          ),
          prefix: Text(
            widget.leadingTag,
            style: TextStyle(
              color: unfocusedColor,
            ),
          ),
          hintText: widget.title,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: widget.suffix != null ? widget.suffix : clearTextFieldButton(widget.controller),
        ),
      ),
    );
  }
}