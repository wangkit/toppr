import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:validators/validators.dart';

import 'lib/pin_code_fields.dart';

class OtpTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final Function onCompleted;

  OtpTextField({
    Key key,
    @required this.controller,
    @required this.focusNode,
    @required this.onCompleted,
  }) : super(key: key);

  @override
  _OtpTextFieldState createState() => _OtpTextFieldState();
}

class _OtpTextFieldState extends State<OtpTextField> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(18.0),
      child: CustomPinCodeTextField(
        appContext: getRoute.getContext(),
        length: 6,
        autoFocus: true,
        focusNode: widget.focusNode,
        autoDismissKeyboard: true,
        autoDisposeControllers: false,
        obsecureText: false,
        textInputType: TextInputType.number,
        textInputAction: TextInputAction.done,
        animationType: AnimationType.fade,
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.box,
          borderRadius: BorderRadius.circular(12),
          fieldHeight: 50,
          fieldWidth: 40,
          activeFillColor: Colors.white,
          inactiveFillColor: Colors.white70,
          selectedFillColor: Colors.white,
          activeColor: cottonSkin,
          inactiveColor: warning,
          selectedColor: appIconColor
        ),
        validator: (value) {
          if (value.isNullOrEmpty) {
            return "Password must not be empty";
          }
          if (!isNumeric(value)) {
            return "Only numbers are allowed";
          }
          return null;
        },
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
        ],
        animationDuration: Duration(milliseconds: 300),
        backgroundColor: appBgColor,
        enableActiveFill: true,
        controller: widget.controller,
        onCompleted: widget.onCompleted,
        onChanged: (value) {
          debugPrint("Value: $value");
        },
        beforeTextPaste: (text) {
          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
          //but you can show anything you want here, like your pop up saying wrong paste format or etc
          return false;
        },
      ),
    );
  }
}
