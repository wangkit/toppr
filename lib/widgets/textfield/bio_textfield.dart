import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/view/view.dart';
import 'package:validators/validators.dart';

class BioTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final String helper;
  final bool canEmpty;
  final EdgeInsets edgeInsets;
  final bool autoFocus;

  BioTextField({
    Key key,
    @required this.controller,
    this.focusNode,
    this.nextFocusNode,
    this.helper,
    this.canEmpty = true,
    this.edgeInsets,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  _BioTextFieldState createState() => _BioTextFieldState();
}

class _BioTextFieldState extends State<BioTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  String helper;
  bool canEmpty;
  bool isVisible;
  bool autoFocus;

  @override
  void initState() {
    autoFocus = widget.autoFocus;
    isVisible = false;
    focusNode = widget.focusNode;
    focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    helper = widget.helper;
    canEmpty = widget.canEmpty;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        autofocus: autoFocus,
        focusNode: focusNode,
        style: TextStyle(
          color: mainColor,
        ),
        maxLength: 200,
        maxLines: null,
        maxLengthEnforced: true,
        onFieldSubmitted: (term) {
          if (focusNode != null && nextFocusNode != null) {
            utils.fieldFocusChange(focusNode, nextFocusNode);
          }
        },
        autofillHints: [AutofillHints.email],
        controller: controller,
        validator: !canEmpty ? (value) {
          if (value.isNullOrEmpty) {
            return "Bio must not be empty";
          }
          return null;
        } : null,
        decoration: InputDecoration(
          helperText: helper,
          counterText: "",
          helperMaxLines: 3,
          hintMaxLines: 1,
          errorMaxLines: 1,
          errorStyle: TextStyle(
            color: errorColor,
          ),
          helperStyle: TextStyle(
            color: mainColor,
          ),
          hintText: TextData.bio,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: clearTextFieldButton(controller),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}