import 'package:toppr/resources/values/text.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/view/view.dart';
import 'package:validators/validators.dart';

class EmailTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final String label;
  final String helper;
  final bool canEmpty;
  final EdgeInsets edgeInsets;
  final bool autoFocus;

  EmailTextField({
    Key key,
    @required this.controller,
    this.focusNode,
    this.nextFocusNode,
    this.label,
    this.helper,
    this.canEmpty = false,
    this.edgeInsets,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  String label;
  String helper;
  bool canEmpty;
  bool isVisible;
  bool autoFocus;

  @override
  void initState() {
    autoFocus = widget.autoFocus;
    isVisible = false;
    label = widget.label;
    focusNode = widget.focusNode;
    focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    helper = widget.helper;
    canEmpty = widget.canEmpty;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        autofocus: autoFocus,
        focusNode: focusNode,
        style: TextStyle(
          color: mainColor,
        ),
        onFieldSubmitted: (term) {
          if (focusNode != null && nextFocusNode != null) {
            utils.fieldFocusChange(focusNode, nextFocusNode);
          }
        },
        autofillHints: [AutofillHints.email],
        controller: controller,
        validator: !canEmpty ? (value) {
          if (value.isEmpty) {
            return "Email must not be empty";
          }
          if (value.contains(" ")) {
            return "Email must not contain white space";
          }
          if (!isEmail(value)) {
            return "Invalid email format";
          }
          return null;
        } : null,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          helperText: helper,
          counterText: "",
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 1,
          helperStyle: TextStyle(
            color: mainColor,
          ),
          hintText: TextData.email,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: clearTextFieldButton(controller),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}