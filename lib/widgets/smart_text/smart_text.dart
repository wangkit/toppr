library smart_text_view;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

abstract class SmartTextElement {}

/// Represents an element containing a link
class LinkElement extends SmartTextElement {
  final String url;

  LinkElement(this.url);

  @override
  String toString() {
    return "LinkElement: $url";
  }
}

/// Represents an element containing a hastag
class CornerTagElement extends SmartTextElement {
  final String tag;

  CornerTagElement(this.tag);

  @override
  String toString() {
    return "cornerTagElement: $tag";
  }
}

/// Represents an element containing a normal tag
class NormalTagElement extends SmartTextElement {
  final String tag;

  NormalTagElement(this.tag);

  @override
  String toString() {
    return "normalTagElement: $tag";
  }
}

/// Represents an element containing a user tag
class UserTagElement extends SmartTextElement {
  final String tag;

  UserTagElement(this.tag);

  @override
  String toString() {
    return "userTagElemenet: $tag";
  }
}

/// Represents an element containing text
class TextElement extends SmartTextElement {
  final String text;

  TextElement(this.text);

  @override
  String toString() {
    return "TextElement: $text";
  }
}

final _linkRegex = RegExp(r"(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)", caseSensitive: false);
final _userTagRegex = RegExp(r"\B" + userTag + r".*", caseSensitive: false);
final _normalTagRegex = RegExp(r"\B" + normalTag + r".*", caseSensitive: false);

/// Turns [text] into a list of [SmartTextElement]
List<SmartTextElement> _smartify(String text) {
  final sentences = text.split('\n');
  List<SmartTextElement> span = [];
  sentences.forEach((sentence) {
    final words = sentence.split(' ');
    words.forEach((word) {
      /// isURL will return true when the username is, for example, @aaa.bbb, while it isn't a url
      if (word.isUrl) {
        int firstIndex;
        String originalText = word;
        firstIndex = word.indexOf("http");
        if (firstIndex == -1) {
          firstIndex = word.indexOf("www.");
        }
        if (firstIndex != 0) {
          span.add(TextElement(originalText.substring(0, firstIndex)));
        }
        word = word.substring(firstIndex, word.length);
        span.add(LinkElement(word));
      } else if (_userTagRegex.hasMatch(word)) {
        span.add(UserTagElement(word));
      } else if (_normalTagRegex.hasMatch(word)) {
        span.add(NormalTagElement(word));
      } else {
        span.add(TextElement(word));
      }
      span.add(TextElement(' '));
    });
    if (words.isNotEmpty) {
      span.removeLast();
    }
    span.add(TextElement('\n'));
  });
  if (sentences.isNotEmpty) {
    span.removeLast();
  }
  return span;
}

/// Callback with URL to open
typedef StringCallback(String url);

/// Turns URLs into links
class SmartText extends StatelessWidget {
  /// Text to be linkified
  final String text;

  /// Style for non-link text
  final TextStyle style;

  /// Style of link text
  final TextStyle linkStyle;

  /// Style of Corner Tag text
  final TextStyle tagStyle;

  /// Callback for tapping a link
  final StringCallback onUrlClick;

  /// Callback for tapping a normal tag
  final StringCallback onNormalTagClick;

  /// Callback for tapping a user tag
  final StringCallback onUserTagClick;

  /// Deterine text style bold or not
  final bool isBold;

  final bool isUserTag;
  final bool isNormalTag;
  final int maxLines;
  final bool isOverflow;
  final bool isReplyContent;

  const SmartText({
    Key key,
    this.isReplyContent,
    this.text,
    this.style,
    this.linkStyle,
    this.tagStyle,
    this.onUrlClick,
    this.onUserTagClick,
    this.maxLines,
    this.onNormalTagClick,
    this.isOverflow = false,
    this.isNormalTag = true,
    this.isUserTag = true,
    this.isBold = false,
  }) : super(key: key);

  /// Raw TextSpan builder for more control on the RichText
  TextSpan _buildTextSpan(
      {String text,
        TextStyle style,
        TextStyle linkStyle,
        TextStyle tagStyle,
        TextStyle normalTagStyle,
        StringCallback onOpen,
        StringCallback onNormalTagClick,
        StringCallback onUserTagClick}) {
    
    void _onOpen(String url) {
      if (onOpen != null) {
        onOpen(url);
      }
    }

    void _onUserTagClick(String userTag) {
      if (onUserTagClick != null) {
        onUserTagClick(userTag);
      }
    }

    void _onNormalTagClick(String normalTag) {
      if (onNormalTagClick != null) {
        onNormalTagClick(normalTag);
      }
    }

    final elements = _smartify(text);

    return TextSpan(
      children: elements.map<TextSpan>((element) {
        if (element is TextElement) {
          return TextSpan(
            text: element.text,
            style: style,
          );
        } else if (element is LinkElement) {
          return TextSpan(
            text: element.url,
            style: linkStyle,
            recognizer: TapGestureRecognizer()..onTap = () => _onOpen(element.url),
          );
        } else if (element is UserTagElement) {
          String simplifiedUsername = element.tag.replaceFirst(userTag, "");
          if (isUserTag && usernameVsProfileMap.containsKey(simplifiedUsername)) {
            return TextSpan(
              text: element.tag,
              style: usernameVsProfileMap.containsKey(simplifiedUsername) ? utils.getNameTextStyle(utils.getColorByHex(usernameVsProfileMap[simplifiedUsername].color, nullColor: mainColor), isBold) : style != null ? style : mainColor,
              recognizer: TapGestureRecognizer()..onTap = () => _onUserTagClick(element.tag),
            );
          } else {
            return TextSpan(
              text: element.tag,
              style: style,
            );
          }
        } else if (element is NormalTagElement) {
          if (isNormalTag) {
            return TextSpan(
              text: element.tag,
              style: normalTagStyle,
              recognizer: TapGestureRecognizer()..onTap = () => _onNormalTagClick(element.tag),
            );
          } else {
            return TextSpan(
              text: element.tag,
              style: style,
            );
          }
        } else {
          throw new Exception("Error in smart text");
        }
      }).toList());
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      overflow: isOverflow ? TextOverflow.ellipsis : TextOverflow.clip,
      maxLines: maxLines,
      softWrap: true,
      text: _buildTextSpan(
        text: text,
        style: Theme.of(context).textTheme.body1.merge(style),
        linkStyle: Theme.of(context)
            .textTheme
            .body1
            .merge(style)
            .copyWith(
          color: Colors.blueAccent,
          decoration: TextDecoration.underline,
        ).merge(linkStyle),
        normalTagStyle: Theme.of(context)
            .textTheme
            .body1
            .merge(style)
            .copyWith(
          color: Colors.blueAccent,
        ),
        tagStyle: Theme.of(context)
            .textTheme
            .body1
            .merge(style)
            .copyWith(
          color: Colors.blueAccent,
        ).merge(linkStyle),
        onOpen: onUrlClick,
        onNormalTagClick: onNormalTagClick,
        onUserTagClick: onUserTagClick
      ),
    );
  }
}

class LinkTextSpan extends TextSpan {
  LinkTextSpan({TextStyle style, VoidCallback onPressed, String text})
      : super(
    style: style,
    text: text,
    recognizer: new TapGestureRecognizer()..onTap = onPressed,
  );
}