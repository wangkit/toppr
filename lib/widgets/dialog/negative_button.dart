
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/button/custom_bouncing_widget.dart';

class NegativeButton extends StatefulWidget {
  final Function onPressed;
  final String text;
  final Color color;
  final bool isHalfWidth;

  NegativeButton({
    Key key,
    this.onPressed,
    this.text = TextData.no,
    this.color = warning,
    this.isHalfWidth = true,
  }) : super(key: key);

  @override
  NegativeButtonState createState() => NegativeButtonState();
}

class NegativeButtonState extends State<NegativeButton> {

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.all(4.0),
        child: CustomBouncingWidget(
          child: Container(
            width: widget.isHalfWidth ? deviceWidth * 0.45 : deviceWidth * 0.9,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
                  side: BorderSide(color: widget.color)
              ),
              color: widget.color,
              child: Text(
                widget.text,
                style: DialogConfig.dialogButtonStyle,
              ),
              onPressed: () {
                getRoute.pop();
                if (widget.onPressed != null) {
                  widget.onPressed();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}