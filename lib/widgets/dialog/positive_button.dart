
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/dialog_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/button/custom_bouncing_widget.dart';

class PositiveButton extends StatefulWidget {
  final Function onPressed;
  final String text;
  final Color color;
  final bool isHalfWidth;
  final bool autoPop;

  PositiveButton({
    Key key,
    this.onPressed,
    this.text = TextData.yes,
    this.color = success,
    this.isHalfWidth = true,
    this.autoPop = true,
  }) : super(key: key);

  @override
  PositiveButtonState createState() => PositiveButtonState();
}

class PositiveButtonState extends State<PositiveButton> {

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.all(4.0),
        child: CustomBouncingWidget(
          child: Container(
            width: widget.isHalfWidth ? deviceWidth * 0.45 : deviceWidth * 0.9,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
                  side: BorderSide(color: widget.color)
              ),
              color: widget.color,
              child: Text(
                widget.text,
                style: DialogConfig.dialogButtonStyle,
              ),
              onPressed: () {
                if (widget.autoPop) {
                  getRoute.pop();
                }
                if (widget.onPressed != null) {
                  widget.onPressed();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}