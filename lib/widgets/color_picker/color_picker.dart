import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/view/dialogs/custom_dialog.dart';
import 'package:toppr/widgets/button/custom_bouncing_widget.dart';
import 'package:toppr/widgets/dialog/negative_button.dart';
import 'package:toppr/widgets/dialog/positive_button.dart';
import 'custom_material_picker.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';

class ColorPicker extends StatefulWidget {
  final String currentColorCode;
  final Function onPressed;
  final String title;
  final bool showBlackWhite;

  ColorPicker({
    Key key,
    @required this.onPressed,
    @required this.currentColorCode,
    this.title = TextData.pickColor,
    this.showBlackWhite = false,
  }) : super(key: key);

  @override
  ColorPickerState createState() => ColorPickerState();
}

class ColorPickerState extends State<ColorPicker> {

  Color pickerColor;
  String currentColorCode;

  @override
  void initState() {
    pickerColor = appIconColor;
    currentColorCode = widget.currentColorCode;
    if (!currentColorCode.isNullOrEmpty) {
      pickerColor = utils.getColorByHex(currentColorCode);
    }
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (ctx, customState) {
        return Center(
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: CustomBouncingWidget(
              child: Container(
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(commonRadius),
                  ),
                  padding: EdgeInsets.all(1.0),
                  color: pickerColor,
                  onPressed: () {
                    showModal(
                        context: context,
                        configuration: FadeScaleTransitionConfiguration(),
                        builder: (BuildContext context) {
                          return CustomAlertDialog(
                            title: Text(
                              widget.title,
                              style: TextStyle(
                                color: mainColor,
                              ),
                            ),
                            content: SingleChildScrollView(
                              child: CustomMaterialPicker(
                                pickerColor: pickerColor,
                                onColorChanged: changeColor,
                                showBlackWhite: widget.showBlackWhite,
                              ),
                            ),
                            actions: <Widget>[
                              NegativeButton(text: TextData.cancel,),
                              PositiveButton(text: TextData.confirm, onPressed: () {
                                widget.onPressed(utils.getHexCodeByColor(pickerColor));
                                customState(() {});
                              },),
                            ],
                          );
                        }
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        padding: EdgeInsets.all(2.0),
                        icon: Icon(
                          Icons.colorize,
                          color: utils.getDefaultWhiteOrBlackColor(utils.isDarkColor(utils.getHexCodeByColor(pickerColor))),
                        ),
                        onPressed: null,
                      ),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(top: 4.0, bottom: 4.0, left: 4.0, right: 8.0,),
                          child: Text(
                            widget.title,
                            style: commonTextStyle.merge(
                                TextStyle(
                                  color: utils.getDefaultWhiteOrBlackColor(utils.isDarkColor(utils.getHexCodeByColor(pickerColor))),
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}