import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/models/firestore_tables.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/widgets/animation/open_container_wrapper.dart';

class Count extends StatefulWidget {

  final int count;
  final String text;
  final Widget openedWidget;

  Count({
    Key key,
    @required this.count,
    @required this.text,
    this.openedWidget,
  }) : super(key: key);

  @override
  CountState createState() => CountState();
}

class CountState extends State<Count> {

  ValueNotifier<int> _count;

  @override
  void initState() {
    _count = ValueNotifier(widget.count);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void changeFollowerCount(bool isFollowing) {
    if (isFollowing) {
      if (_count.value == widget.count || _count.value == widget.count - 1) {
        _count.value++;
      }
    } else {
      if (_count.value == widget.count || _count.value == widget.count + 1) {
        _count.value--;
      }
    }
    refreshThisPage();
  }

  @override
  Widget build(BuildContext context) {
    return OpenContainerWrapper(
      openedWidget: widget.openedWidget,
      tappable: widget.openedWidget != null,
      closedElevation: 0,
      closedColor: appBgColor,
      closedWidget: Container(
        width: deviceWidth / 5,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ValueListenableBuilder(
              valueListenable: _count,
              builder: (ctx, value, child) {
                return Text(
                  utils.shortStringForLongInt(value),
                  style: commonTextStyle,
                );
              },
            ),
            Text(
              utils.determineNeedS(widget.count, widget.text, showNumber: false),
              style: commonTextStyle.merge(
                TextStyle(
                    fontSize: extraSmallFontSize
                )
              ),
            ),
          ],
        ),
      ),
    );
  }
}


