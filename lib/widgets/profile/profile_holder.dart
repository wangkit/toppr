import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/app/post/post_page.dart';
import 'package:toppr/app/post/view_post_page.dart';
import 'package:toppr/app/profile/view_profile_page.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/physics/avoid_implicit_scroll.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/text.dart';
import 'package:toppr/widgets/appbar/appbar.dart';
import 'package:toppr/widgets/loading/loading_widget.dart';
import 'package:toppr/widgets/media/image/thumbnail.dart';
import 'package:toppr/widgets/tag/tag.dart';

class ProfileHolder extends StatefulWidget {

  final UserProfile profile;

  ProfileHolder({
    Key key,
    @required this.profile,
  }) : super(key: key);

  @override
  _ProfileHolderState createState() => _ProfileHolderState();
}

class _ProfileHolderState extends State<ProfileHolder> {

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Container(
        width: deviceWidth,
        height: 100,
        child: Card(
          elevation: commonElevation,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(commonRadius),
          ),
          child: InkWell(
            borderRadius: BorderRadius.circular(commonRadius),
            onTap: () {
              getRoute.navigateTo(ViewProfilePage(profile: widget.profile));
            },
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Thumbnail(
                  imageUrl: widget.profile.image,
                  text: widget.profile.username,
                  color: utils.getColorByHex(widget.profile.color),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          utils.getUserTemplate(widget.profile.username),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: smallTextStyle.merge(
                            TextStyle(
                              color: utils.getColorByHex(widget.profile.color),
                            )
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Flexible(
                        child: Text(
                          widget.profile.bio,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: smallTextStyle,
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: widget.profile.id != myId ? FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                      color: widget.profile.amIFollowing ? lightUnfocusedColor : utils.getColorByHex(widget.profile.color),
                      onPressed: () {
                        apiManager.follow(widget.profile.id, widget.profile.amIFollowing);
                        widget.profile.amIFollowing = !widget.profile.amIFollowing;
                        refreshThisPage();
                      },
                      child: Text(
                        widget.profile.amIFollowing ? TextData.unfollow : TextData.follow,
                        style: smallTextStyle.merge(
                          TextStyle(
                            fontSize: extraSmallFontSize
                          )
                        ),
                      ),
                    ) : Tag(
                      padding: BubbleEdges.symmetric(horizontal: 8.0, vertical: 4.0),
                      colorCode: myProfile.color,
                      text: TextData.yourself,
                      fontSize: extraSmallFontSize,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
