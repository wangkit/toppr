import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

class MessageBubble extends StatefulWidget {
  final String senderEmail;
  final String receiverEmail;
  final String message;
  final String createdAt;
  final String messageId;
  final String senderName;
  final String receiverName;
  final String senderColor;
  final String senderId;
  final String receiverId;
  final bool isMyself;
  bool isSelected;
  final Function notifyParent;
  List<String> selectedMessages;
  
  MessageBubble({
    Key key,
    this.senderEmail,
    this.receiverEmail,
    this.message,
    this.messageId,
    this.senderColor,
    this.createdAt,
    this.notifyParent,
    this.senderName,
    this.receiverName,
    this.isSelected,
    this.senderId,
    this.receiverId,
    this.isMyself,
    this.selectedMessages,
  });

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: !widget.isMyself ? null : () {
        widget.isSelected = true;
        widget.selectedMessages.add(widget.messageId);
        widget.notifyParent();
      },
      onTap: !widget.isMyself ? null : () {
        if (widget.isSelected) {
          widget.isSelected = false;
          widget.selectedMessages.remove(widget.messageId);
          widget.notifyParent();
        } else if (widget.selectedMessages.length > 0) {
          widget.isSelected = true;
          widget.selectedMessages.add(widget.messageId);
          widget.notifyParent();
        }
      },
      child: AnimatedContainer(
        duration: kThemeAnimationDuration,
        decoration: BoxDecoration(
          color: widget.isSelected ? unfocusedColor : transparent,
          borderRadius: BorderRadius.circular(commonRadius),
        ),
        child: Bubble(
          margin: BubbleEdges.only(top: 4, bottom: 4, right: 2, left: 2),
          alignment: widget.isMyself ? Alignment.centerRight : Alignment.centerLeft,
          nip: widget.isMyself ? BubbleNip.rightBottom : BubbleNip.leftTop,
          shadowColor: mainColor.withOpacity(0.35),
          elevation: 1 * MediaQuery.of(context).devicePixelRatio,
          radius: Radius.circular(commonRadius),
          color: widget.isMyself ? unfocusedColor : utils.getColorByHex(widget.senderColor),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(width: 4,),
              Flexible(
                child: Text(
                    widget.message,
                    style: commonTextStyle.merge(
                      TextStyle(
                        color: widget.isMyself ? mainColor : utils.isDarkColor(widget.senderColor) ? defaultWhiteColor : defaultBlackColor,
                      ),
                    )
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, top: 10.0),
                child: Text(
                  timeUtils.getHourMinuteFromUTCDateTime(widget.createdAt),
                  style: commonTextStyle.merge(
                    TextStyle(
                      fontSize: extraSmallFontSize,
                      color: widget.isMyself ? mainColor : utils.isDarkColor(widget.senderColor) ? defaultWhiteColor : defaultBlackColor,
                  )
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}