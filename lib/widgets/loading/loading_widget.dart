import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

class LoadingWidget extends StatelessWidget {

  LoadingWidget({this.isMargin = false});

  final bool isMargin;

  @override
  Widget build(BuildContext context) {

    double iconSize = 60;

    return Center(
      child: Container(
        alignment: Alignment.center,
        margin: isMargin ? EdgeInsets.only(top: 50.0, bottom: 50.0) : EdgeInsets.all(0.0),
        child: ClipOval(
          child: SpinPerfect(
            infinite: true,
            child: Stack(
              children: [
                Container(
                  color: myProfile != null ? utils.getColorByHex(myProfile.color) : appIconColor,
                  width: iconSize,
                  height: iconSize,
                ),
                Image.asset(
                  "assets/icon/toppr_without_bg.png",
                  width: iconSize,
                  height: iconSize,
                  cacheWidth: iconSize.toInt() * 2,
                  cacheHeight: iconSize.toInt() * 2,
                ),
              ],
            )
          ),
        ),
      ),
    );
  }
}
