import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:get/route_manager.dart';

class GetRoute {

  bool opaque = false;
  bool popGesture = true;
  bool preventDuplicates = false;
  Transition transition = Transition.cupertino;
  Duration duration = Duration(milliseconds: 200);
  Random rng = Random();

  BuildContext getContext() => Get.context;

  void navigateTo(Widget target) {
    Get.to(
      target,
      opaque: opaque,
      transition: transition,
      duration: duration,
      popGesture: popGesture,
      preventDuplicates: preventDuplicates,
    );
  }

  void navigateToAndPopAll(Widget target) {
    Get.offAll(
      target,
      opaque: opaque,
      transition: transition,
      duration: duration,
      popGesture: popGesture,
    );
  }

  void navigateToAndReplace(Widget target) {
    Get.off(
      target,
      opaque: opaque,
      transition: transition,
      duration: duration,
      popGesture: popGesture,
      preventDuplicates: preventDuplicates,
    );
  }

  /// Using get.back will result in the variables of that widget not being
  /// reinitialized while reentering the same page. So sometimes a true pop
  /// is needed.
  void truePop() {
    Navigator.pop(Get.context);
  }

  void pop() {
    Get.back();
  }
}