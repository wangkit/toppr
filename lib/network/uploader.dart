import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/env.dart';

class Uploader {

  Future<String> uploadFileToFirebase(File file, bool isVideo, ImageType type) async {
    String imageType;
    switch (type) {
      case ImageType.post:
        imageType = "Post";
        break;
      case ImageType.reply:
        imageType = "Reply";
        break;
      case ImageType.profile:
        imageType = "Profile";
        break;
    }
    try {
      String fileName = path.basename(file.path);
      String referenceName;
      String folderName;
      switch (env.flavor) {
        case BuildFlavor.production:
          referenceName = "prod/";
          break;
        case BuildFlavor.development:
          referenceName = "dev/";
          break;
        case BuildFlavor.staging:
          referenceName = "staging/";
          break;
      }
      if (isVideo) {
        folderName = "video";
      } else {
        folderName = "image";
      }
      final StorageReference firebaseStorageRef = FirebaseStorage.instance
          .ref()
          .child("$referenceName/$imageType/$folderName/$fileName");
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(file);
      StorageTaskSnapshot storageSnapshot = await uploadTask.onComplete;
      var downloadUrl = await storageSnapshot.ref.getDownloadURL();
      if (uploadTask.isComplete) {
        var url = downloadUrl.toString();
        return url;
      }
    } on Exception catch (e) {
      debugPrint("Firebase error: $e");
    }
    return null;
  }
}