import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/utils/extensions/string_extensions.dart';
import 'package:http/http.dart' as http;

class FirebaseClient {

  Future<void> registerFcm(String fcm) async {
    final url = apiBaseUrl + "/registerFCMToken";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.fcm: fcm,
      DataKey.platform: Platform.isAndroid ? "Android" : "iOS",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        var jsonResponse = json.decode(response.body);
        int status = jsonResponse[DataKey.status];
        hasCalledFCMRegistration = status == StatusCode.ok;
        utils.saveBoo(PrefKey.hasCalledFCMRegistration, hasCalledFCMRegistration);
      }
    }).catchError((onError) {
      debugPrint("Fcm Error: $onError");
    });
  }

  void addFcm() {
    if (!hasCalledFCMRegistration) {
      firebaseMessaging.getToken().then((String token) {
        if (!token.isNullOrEmpty) {
          registerFcm(token);
        }
      });
    }
  }

  Future<void> signInToFirebase(String email, String password) async {
    try {
      firebaseUser = (await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
    } on Exception catch (e) {
      debugPrint("catch: $e");
    }
  }

  Future<void> createAccountWithEmailAndPassword(String email, String password) async {
    try {
      firebaseUser = (await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
    } on Exception catch (e) {
      debugPrint("catch: $e");
    }
  }
}