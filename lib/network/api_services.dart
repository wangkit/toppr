import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:toppr/config/app_config.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';
import 'package:toppr/resources/enum/enum.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/resources/status/status_code.dart';
import 'package:toppr/resources/values/data_key.dart';
import 'package:toppr/resources/values/pref_key.dart';
import 'package:toppr/resources/values/text.dart';

class ApiManager {

  Future<void> initClientOrRetrieveData() async {
    final url = apiBaseUrl + "/createClient";
    DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
    String deviceName, deviceVersion, deviceId;
    if (Platform.isAndroid) {
      final androidInfo = await deviceInfoPlug.androidInfo;
      deviceName = androidInfo.model;
      deviceVersion = androidInfo.version.release.toString();
      deviceId = androidInfo.androidId;
    } else {
      final iosInfo = await deviceInfoPlug.iosInfo;
      deviceName = iosInfo.name;
      deviceVersion = iosInfo.systemVersion;
      deviceId = iosInfo.identifierForVendor;
    }
    Map<String, String> param = {
      DataKey.deviceId: deviceId,
      DataKey.deviceName: deviceName,
      DataKey.deviceVersion: deviceVersion,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          uniqueId = jsonResponse[DataKey.uniqueId];
          secret = jsonResponse[DataKey.secret];
          utils.saveStr(PrefKey.uniqueId, uniqueId);
          utils.saveStr(PrefKey.secret, secret);
        }
      } else {
        utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<bool> login(String email, String password) {
    final url = apiBaseUrl + "/login";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.password: password,
      DataKey.email: email,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          myId = jsonResponse[DataKey.id];
          utils.saveStr(PrefKey.myId, myId);
        }
        return jsonStatus == StatusCode.ok;
      } else {
        return false;
      }
    }).catchError((onError) {
      debugPrint("Check email error: $onError");
      return false;
    });
  }

  Future<void> createPost(String caption, List<String> imageLinks, double longitude, double latitude, String color) {
    final url = apiBaseUrl + "/createPost";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.caption: caption ?? "",
      DataKey.image: imageLinks.toString() ?? "[]",
      DataKey.longitude: longitude.toString() ?? "0",
      DataKey.latitude: latitude.toString() ?? "0",
      DataKey.listOfTaggedUsers: utils.extractTaggedUsername(caption),
      DataKey.textColor: color ?? myProfile.color,
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Get follow error: $onError");
    });
  }

  Future<dynamic> searchUser(String query) {
    final url = apiBaseUrl + "/searchUser";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.query: query ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Get follow error: $onError");
    });
  }

  Future<dynamic> getFollow(String targetId, bool isFollower, int offset, String query) {
    final url = apiBaseUrl + "/getFollow";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.targetId: targetId,
      DataKey.id: myId,
      DataKey.offset: offset.toString() ?? "0",
      DataKey.isFollower: isFollower.toString() ?? "",
      DataKey.query: query ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Get follow error: $onError");
    });
  }

  Future<dynamic> editProfile(String bio, String color, String username, String image, String backgroundImage) {
    final url = apiBaseUrl + "/editProfile";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.bio: bio ?? "",
      DataKey.color: color ?? "",
      DataKey.username: username ?? "",
      DataKey.image: image ?? "",
      DataKey.backgroundImage: backgroundImage ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Get post error: $onError");
    });
  }

  Future<dynamic> getPostsById(String targetId, int offset) {
    final url = apiBaseUrl + "/getPostsById";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.targetId: targetId ?? "",
      DataKey.offset: offset.toString() ?? "0",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Get post error: $onError");
    });
  }

  Future<dynamic> viewPost(String postId) {
    final url = apiBaseUrl + "/viewPost";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.postId: postId ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> getPosts(int offset, PostType postType, String targetProfileId) {
    final url = apiBaseUrl + "/getPosts";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.offset: offset.toString() ?? "0",
      DataKey.postType: postType.name ?? PostType.discover.name,
      DataKey.targetId: targetProfileId ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Get post error: $onError");
    });
  }

  Future<bool> checkEmail(String email) {
    final url = apiBaseUrl + "/checkEmail";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.email: email,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        return jsonStatus == StatusCode.ok;
      } else {
        return false;
      }
    }).catchError((onError) {
      debugPrint("Check email error: $onError");
      return false;
    });
  }

  Future<bool> checkUsername(String username) {
    final url = apiBaseUrl + "/checkUsername";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.username: username,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        return jsonStatus == StatusCode.ok;
      } else {
        return false;
      }
    }).catchError((onError) {
      debugPrint("check username error: $onError");
      return false;
    });
  }

  Future<void> block(String targetId, int reason) async {
    final url = apiBaseUrl + "/block";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.targetId: targetId,
      DataKey.reason: reason.toString(),
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Block error: $onError");
    });
  }

  Future<dynamic> reportUser(String targetId, String reason) async {
    final url = apiBaseUrl + "/reportUser";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.targetId: targetId,
      DataKey.reason: reason,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Report error: $onError");
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<dynamic> getVerificationCode(String email) async {
    String deviceName;
    DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      final androidInfo = await deviceInfoPlug.androidInfo;
      deviceName = androidInfo.model;
    } else {
      final iosInfo = await deviceInfoPlug.iosInfo;
      deviceName = iosInfo.name;
    }
    final url = apiBaseUrl + "/sendVerificationCode";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.email: email,
      DataKey.deviceName: deviceName,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        var jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        return jsonStatus == StatusCode.ok;
      }
      return false;
    }).catchError((onError) {
      print("get veri code error: $onError");
      return false;
    });
  }

  Future<dynamic> follow(String targetId, bool beforeStatus) {
    final url = apiBaseUrl + "/follow";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.targetId: targetId ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (beforeStatus) {
        myProfile.followingCount--;
      } else {
        myProfile.followingCount++;
      }
      getMyProfile();
    });
  }

  Future<dynamic> likePost(String postId) {
    final url = apiBaseUrl + "/likePost";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.postId: postId ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> createReply(String postId, String replyId, String message, String taggedUsernames, {String targetReplyId = ""}) {
    final url = apiBaseUrl + "/createReply";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.id: myId,
      DataKey.taggedUsernames: taggedUsernames ?? "",
      DataKey.postId: postId ?? "",
      DataKey.replyId: replyId ?? "",
      DataKey.replyReplyId: targetReplyId ?? "",
      DataKey.message: message ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> checkOtp(String otp) async {
    final url = apiBaseUrl + "/checkOneTimePassword";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.otp: otp,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        return jsonStatus == StatusCode.ok;
      } else {
        return false;
      }
    }).catchError((onError) {
      return false;
    });
  }

  Future<dynamic> getProfileById(String targetId) async {
    final url = apiBaseUrl + "/getProfileById";
    Map<String, String> param = {
      DataKey.id: myId ?? "",
      DataKey.uniqueId: uniqueId ?? "",
      DataKey.secret: secret ?? "",
      DataKey.targetId: targetId ?? "",
    };
    return http.post(url, body: param).then((http.Response response) {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      print("Error: $onError");
    });
  }

  Future<void> getMyProfile() async {

    final url = apiBaseUrl + "/getMyProfile";
    Map<String, String> param = {
      DataKey.id: myId ?? "",
      DataKey.uniqueId: uniqueId ?? "",
      DataKey.secret: secret ?? "",
    };

    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          List<dynamic> myProfileList = jsonResponse[DataKey.myProfile];
          debugPrint("myProfileList: $myProfileList");
          if (myProfileList.length > 0) {
            myProfile = UserProfile.generateUserProfile(myProfileList).first;
            utils.addToUserDataMap(myProfile);
            await firebaseClient.signInToFirebase(myProfile.email, myProfile.fcmSecret);
            myId = myProfile.id;
            utils.saveStr(PrefKey.myId, myId);
            firebaseClient.addFcm();
          }
        }
      }
    });
  }

  Future<void> actionLog(String action) async {
    final url = apiBaseUrl + "/actionLog";
    Map<String, String> param = {
      DataKey.id: myId ?? "",
      DataKey.action: action.toString() ?? "",
      DataKey.uniqueId: uniqueId ?? "",
      DataKey.secret: secret ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> getUserProfileByUsername(List<String> usernames) async {
    final url = apiBaseUrl + "/getProfilesByUsernames";
    List<String> requestList = [];
    usernames.forEach((eachPart) {
      if (eachPart.contains(userTag)) {
        eachPart = utils.getTextAfterUserTag(eachPart);
      }
      requestList.add(eachPart);
    });
    if (requestList.isNotEmpty && requestList != null) {
      var userString = requestList.toString();
      userString = userString.substring(1, userString.length - 1).replaceAll(" ", "");
      Map<String, String> param = {
        DataKey.id: myId ?? "",
        DataKey.secret: secret ?? "",
        DataKey.uniqueId: uniqueId ?? "",
        DataKey.usernames: userString,
      };
      return http.post(url, body: param).then((http.Response response) {
        if (response.statusCode == StatusCode.ok) {
          final jsonResponse = json.decode(response.body);
          return jsonResponse;
        }
      }).catchError((onError) {
        print("Error: $onError");
      });
    } else {
      return [];
    }
  }

  Future<void> createProfile(String username, String password, String email, String color, String postImage, String fcmSecret, Position postition) {
    final url = apiBaseUrl + "/createProfile";
    Map<String, String> param = {
      DataKey.uniqueId: uniqueId,
      DataKey.secret: secret,
      DataKey.username: username,
      DataKey.email: email,
      DataKey.password: password,
      DataKey.color: color,
      DataKey.image: postImage,
      DataKey.fcmSecret: fcmSecret,
      DataKey.longitude: postition.longitude.toString(),
      DataKey.latitude: postition.latitude.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        List<dynamic> myProfileList = jsonResponse[DataKey.myProfile];
        if (jsonStatus == StatusCode.ok) {
          debugPrint("myProfileList: $myProfileList");
          if (myProfileList.length > 0) {
            myProfile = UserProfile.generateUserProfile(myProfileList).first;
            myId = myProfile.id;
            utils.saveStr(PrefKey.myId, myId);
            firebaseClient.addFcm();
          }
        }
      }
    }).catchError((onError) {
      debugPrint("create profile error: $onError");
    });
  }
}