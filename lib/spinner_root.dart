import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toppr/resources/color/color.dart';
import 'app/splash/splash_page.dart';
import 'config/app_config.dart';
import 'constants/const.dart';

class TopprRoot extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () {
        utils.closeKeyboard();
        utils.quitFocus(createNewFocusToRemoveOld: true);
      },
      child: RefreshConfiguration(
        headerBuilder: () => WaterDropMaterialHeader(
          color: mainColor,
          backgroundColor: appBgColor,
        ),
        footerBuilder: () => CustomFooter(
          builder: (BuildContext context, LoadStatus status) {
            switch (status) {
              default:
                return Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: CupertinoActivityIndicator(),
                  ),
                );
            }
          },
        ),
        footerTriggerDistance: 80.0,
        headerTriggerDistance: 40.0,
        springDescription: SpringDescription(stiffness: 170, damping: 16, mass: 1.9),
        maxOverScrollExtent : 100,
        maxUnderScrollExtent: 100,
        enableScrollWhenRefreshCompleted: true,
        enableLoadingWhenFailed : true,
        hideFooterWhenNotFull: true,
        enableBallisticLoad: true,
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: myAppName,
          theme: ThemeData(
            appBarTheme: AppBarTheme(
              color: appIconColor,
            ),
            backgroundColor: appBgColor,
            floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: appIconColor,
            ),
//            toggleableActiveColor: cottonLightBlue,
            fontFamily: appFontFamily,
            textSelectionColor: unfocusedColor,
            primarySwatch: GreenBlue,
            textSelectionHandleColor: GreenBlue,
            cursorColor: GreenBlue,
            scaffoldBackgroundColor: appBgColor,
          ),
          home: SplashPage(),
        ),
      ),
    );
  }
}

