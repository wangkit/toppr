import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toppr/spinner_root.dart';
import 'config/app_config.dart';
import 'constants/const.dart';
import 'resources/models/env.dart';

void main() async {
  BuildEnvironment.init(
    flavor: BuildFlavor.development,
    baseUrl: 'https://toppr-dev.herokuapp.com/toppr/modern/V1',
  );
  WidgetsFlutterBinding.ensureInitialized();
  assert(env != null);
  await Firebase.initializeApp();
  prefs = await SharedPreferences.getInstance();
  runApp(TopprRoot());
}