import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toppr/constants/const.dart';
import 'package:toppr/resources/color/color.dart';

/// Theme color
Color appTheme = FluroGreen;

/// Main color
Color mainColor = defaultBlackColor;

/// reverse color
Color reverseColor = defaultWhiteColor;

/// Bg color
Color appBgColor = littleGrey;

final numberOfMessagesPerPage = 100;

final allowedMediaPerPost = 6;

const double commonRadius = 16.0;

double imageRadius = 8.0;

double commonImageRadius = 8.0;

double commonElevation = 6.0;

/// warning icon
IconData warningIcon = MdiIcons.alertBox;

/// success icon
IconData successIcon = Icons.thumb_up;

/// big font size
double bigFontSize = 23.0;

/// font size
double commonFontSize = 20.0;

/// small font size
double smallFontSize = 17.0;

/// extra small font size
double extraSmallFontSize = 14.0;

/// extra big font size
double extraBigFontSize = 26.0;

/// small text style across the app
TextStyle smallTextStyle = TextStyle(
  color: mainColor,
  fontSize: smallFontSize,
);

/// big text style across the app
TextStyle bigTextStyle = TextStyle(
  color: mainColor,
  fontSize: bigFontSize,
);

/// common text style across the app
TextStyle commonTextStyle = TextStyle(
  color: mainColor,
  fontSize: commonFontSize,
);


TextStyle longButtonTextStyle = TextStyle(
  fontWeight: FontWeight.w500,
  fontSize: 19.0,
  color: mainColor,
);

bool hasCalledFCMRegistration = false;

var hasCalledCatKey = false;

/// use black theme
bool useBlackTheme;

/// can push
bool canPushNotifications;