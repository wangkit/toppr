import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'app_config.dart';

class DialogConfig {
  static double dialogCornerRadius = 6.0;
  static RoundedRectangleBorder dialogShape() {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(dialogCornerRadius),
    );
  }
  static double dialogTitleFontSize = 19.0;
  static FontWeight dialogTitleFontWeight = FontWeight.w500;
  static ModalConfiguration getTransition({bool isBarrierDismissible = true}) {
    return FadeScaleTransitionConfiguration(
      barrierDismissible: isBarrierDismissible,
    );
  }
  static TextStyle dialogTitleStyle = TextStyle(
    color: mainColor,
    fontSize: DialogConfig.dialogTitleFontSize,
    fontWeight: DialogConfig.dialogTitleFontWeight,
  );
  static TextStyle dialogMessageStyle = TextStyle(
    color: mainColor,
  );
  static TextStyle dialogButtonStyle = TextStyle(
    color: mainColor,
  );
  static Color dialogBackgroundColor = appBgColor;
}