import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:toppr/network/api_services.dart';
import 'package:toppr/network/firebase.dart';
import 'package:toppr/network/uploader.dart';
import 'package:toppr/page_route/get_route.dart';
import 'package:toppr/resources/models/models.dart';
import 'package:toppr/utils/time_utils.dart';
import 'package:toppr/utils/utils.dart';

/// Firebase user holder
User firebaseUser;

final firestore = FirebaseFirestore.instance;

/// Cloud messaging
FirebaseMessaging firebaseMessaging;

/// Cat key
String catKey;

/// Firebase auth instance
final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

/// Firebase db collection name
final messageDbName = "message";
final chatDbName = "chat";
final outerRepliesDbName = "replies";
final innerReplyDbName = "reply";
final replyReplyDbName = "replyReply";

/// My Id
String myId;

/// My secret
String secret;

/// My unique id
String uniqueId;

/// firebase instance
FirebaseClient firebaseClient;

/// my profile
UserProfile myProfile;

/// Utils instance
Utils utils;

///// iap instance
//IapManager iapManager;

/// Uploader instance
Uploader uploader;

/// Time utils instance
TimeUtils timeUtils;

/// Api manager
ApiManager apiManager;

/// Navigator get
GetRoute getRoute;

String apiBaseUrl;

/// user tag
const userTag = "@";

/// app name
const appName = "Toppr";

/// App font family
const appFontFamily = "proxima";

/// header font familt
const headerFontFamily = "proxima_alt_bold";

/// Shared preference instance
SharedPreferences prefs;

/// Lists for emoji storage
List<String> recentEmojis = new List();
Map<String, String> smileyMap = new Map();
Map<String, String> animalMap = new Map();
Map<String, String> foodMap = new Map();
Map<String, String> travelMap = new Map();
Map<String, String> activityEmojiMap = new Map();
Map<String, String> objectMap = new Map();
Map<String, String> symbolMap = new Map();
Map<String, String> flagMap = new Map();

/// Share message
const shareMsg = "The newest dating platform where you will find your love.";

/// default black color
const defaultBlackColor = Colors.black87;

/// default white color
const defaultWhiteColor = Colors.white;

/// App name
final myAppName = "Spinner";

/// Company contact email
const companyEmail = "admin@yarner.app";

/// Icons 8 link
const icon8Link = "https://icons8.com/";

/// Company website
const companyWebsiteLink = "https://www.yarner.app";

/// Forum EULA link
const eulaLink = companyWebsiteLink + "/terms-of-use";

/// Bucket url
const bucketURl = "https://spinner-version.s3.us-east-2.amazonaws.com";

/// Yarner android link
const yarnerAndroidLink = "https://play.google.com/store/apps/details?id=com.story.yarner";

/// Yarner iOS link
const yarnerIosLink = "https://apps.apple.com/hk/app/yarner/id1512119130";

/// Link to check version
const checkVersionAndGetUrls = bucketURl + "/version.txt";

/// apple link and google link
String googleStoreUrl, appleStoreUrl;

/// Link to privacy policy
var privacyPolicyUrl = companyWebsiteLink + "/privacy-policy";

/// App package information retrieved
String versionName;
int versionCode;

/// App dimension
double deviceHeight, deviceWidth;

/// url prefix
const urlPrefix = "https://www.";

const normalTag = "#";

/// Encrypter
final iv = en.IV.fromLength(16);
final encrypter = en.Encrypter(en.AES(en.Key.fromLength(32)));

/// user placeholder
const placeholder = "assets/placeholder.png";
const placeholderLink = "https://res.cloudinary.com/hyclslbe2/image/upload/v1588150703/profile/placeholder_s9bajp.png";
const backgroundProfilePlaceholderLink = "https://res.cloudinary.com/hyclslbe2/image/upload/v1591627662/corner/corner_bg_placeholder_moxcfv.png";

/// video placeholder
const videoPlaceholder = "assets/video_placeholder.png";

Map<String, UserProfile> usernameVsProfileMap = {};
